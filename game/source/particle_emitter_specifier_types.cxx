#include "particle_emitter_specifier_types.hpp"
#include "entity_particle_emitter.hpp"
#include "random_functions.hpp"

WalkingParticleSpecifier::WalkingParticleSpecifier()
    :ParticleEmitterSpecifier(), color(sf::Color(255,0,255)), velocity(sf::Vector2f(0.f, -100.f))
{
    particleTextureID = GameResources::Textures::DEFAULT_PARTICLE_TEXTURE;
    isGravitySensitive = true;
    numberOfParticlesPerSecond = 5;
    particleLifeTime = sf::seconds(0.5f);
}

WalkingParticleSpecifier::~WalkingParticleSpecifier()
{
}

sf::Vector2f WalkingParticleSpecifier::particleInitialPosition(const EntityParticleEmitter &parentEmitter)
{
    return parentEmitter.getPosition();
}

sf::Vector2f WalkingParticleSpecifier::particleVelocity(sf::Time deltaTime)
{
    return velocity;
}

sf::Color WalkingParticleSpecifier::particleColor(sf::Time deltaTime)
{
    return color;
}

FireworksParticleSpecifier::FireworksParticleSpecifier()
    :ParticleEmitterSpecifier()
{
    particleTextureID = GameResources::Textures::FANCY_PARTICLE_TEXTURE;
    isGravitySensitive = false;
    numberOfParticlesPerSecond = 500;
    particleLifeTime = sf::seconds(2.5f);
}

FireworksParticleSpecifier::~FireworksParticleSpecifier()
{
}

sf::Vector2f FireworksParticleSpecifier::particleInitialPosition(const EntityParticleEmitter &parentEmitter)
{
    return parentEmitter.getPosition();
}

sf::Vector2f FireworksParticleSpecifier::particleVelocity(sf::Time deltaTime)
{
    return sf::Vector2f(cos(lsmRandom::randomFloat(0.f, M_PI)), -sin(lsmRandom::randomFloat(0.f, M_PI))) * 250.f;
}

sf::Color FireworksParticleSpecifier::particleColor(sf::Time deltaTime)
{
    color.r = lsmRandom::randomInt(0,255);
    color.g = lsmRandom::randomInt(0,255);
    color.b = lsmRandom::randomInt(0,255);
    return color;
}

JumpingBackDownParticleSpecifier::JumpingBackDownParticleSpecifier()
    :ParticleEmitterSpecifier()
{
    particleTextureID = GameResources::Textures::DEFAULT_PARTICLE_TEXTURE;
    isGravitySensitive = true;
    numberOfParticlesPerSecond = 5;
    particleLifeTime = sf::seconds(0.5f);
}

JumpingBackDownParticleSpecifier::~JumpingBackDownParticleSpecifier()
{
}

sf::Vector2f JumpingBackDownParticleSpecifier::particleInitialPosition(const EntityParticleEmitter &parentEmitter)
{
    return parentEmitter.getPosition();
}

sf::Vector2f JumpingBackDownParticleSpecifier::particleVelocity(sf::Time deltaTime)
{
    static int yDirectionModulo = 0;
    opositeXVelocity = !opositeXVelocity;
    int yDir = ((yDirectionModulo++ % 4) < 2) ? 1 : 0;
    return sf::Vector2f((float)(1 - 2*(!opositeXVelocity)) * cos(yDir * M_PI / 3.f + (1 - yDir) * M_PI_4),
         -sin(yDir * M_PI / 3.f + (1 - yDir) * M_PI_4)) 
        * (100.f * (float)yDir + 50.f * (float)(1 - yDir));
}

sf::Color JumpingBackDownParticleSpecifier::particleColor(sf::Time deltaTime)
{
    return sf::Color(255,0,255);
}