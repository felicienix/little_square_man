#include "state_stack.hpp"

StateStack::StateStack()
    :stack(), actionRequests(), transitionFadeScreen(sf::Vector2f(WIDTH, HEIGHT)), createChangeStateActionRequest()
{   
    //transparent black
    transitionFadeScreen.setFillColor(sf::Color(0, 0, 0, 0));
}

StateStack::~StateStack()
{
    for(auto* state : stack)
        delete state;
}

void StateStack::handleEvent(const sf::Event &event)
{
    for(auto stateIter = stack.rbegin(); stateIter != stack.rend();++stateIter)
    {
        if(!(*stateIter)->handleEvent(event))
            break;
    }
}

void StateStack::update(sf::Time deltaTime)
{
    for(auto stateIter = stack.rbegin(); stateIter != stack.rend();++stateIter)
    {
        if(!(*stateIter)->update(deltaTime))
            break;
    }

    if(isTransitioning)
    {
        transitionAccumulator += deltaTime;
        float ratio = transitionAccumulator / transitionDelay;
        transitionFadeScreen.setFillColor(sf::Color(0,0,0,255.f * abs(std::sin(ratio * PI))));
        if(!transitioningMiddlePointReached && transitionAccumulator >= transitionDelay * 0.5f) //when it's the middle of the transition, change state
        {
            transitioningMiddlePointReached = true;
            //first delete the top state
            delete stack.back();
            stack.pop_back();
            //and then push the new one
            stack.push_back(createChangeStateActionRequest());
        }
        if(transitionAccumulator >= transitionDelay)
        {
            //stop the  transition
            transitionAccumulator = sf::Time::Zero;
            transitionFadeScreen.setFillColor(sf::Color(0,0,0,0));
            isTransitioning = false;
        }
    }
    else
    {
        applyActions();
    }
}

void StateStack::requestAction(Actions action, const std::function<State *()> &createState)
{
    actionRequests.push(std::make_pair(action, createState));
}

void StateStack::applyActions()
{
    while(!actionRequests.empty())
    {
        auto pairActionRequest = actionRequests.front();
        switch (pairActionRequest.first)
        {
        case Actions::POP_STATE:
            if(!stack.empty())
            {
                delete stack.back();
                stack.pop_back();
                if(!stack.empty())
                    stack.back()->setForeGroundState(true);
            }
            break;
        case Actions::PUSH_STATE:
            {
                if(!stack.empty())
                {
                    stack.back()->setForeGroundState(false);
                }
                stack.push_back(pairActionRequest.second());
            }
            break;
        case Actions::CHANGE_STATE:
            isTransitioning = true;
            transitioningMiddlePointReached = false;
            createChangeStateActionRequest = pairActionRequest.second;
            break;
        case Actions::POP_ALL_STATES:
            while(!stack.empty())
            {
                delete stack.back();
                stack.pop_back();
            }
            break;
        default:
            break;
        }

        actionRequests.pop();
    }
}

void StateStack::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    for(auto stateIter = stack.begin(); stateIter != stack.end();++stateIter)
    {
        target.draw(*(*stateIter), states);
    }

    if(isTransitioning)
        target.draw(transitionFadeScreen, states);
}
