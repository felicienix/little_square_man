#include "settings_state.hpp"
#include "gui_menu.hpp"
#include "gui_control_key_button.hpp"
#include "character_entity_identifiers.hpp"
#include "state_stack.hpp"
#include "entity_character.hpp"


namespace
{

    std::string convertCharacterControlIDToString(GameResources::CharacterControls::ID controlID)
    {
        switch (controlID)
        {
        case GameResources::CharacterControls::MOVE_RIGHT:
            return "move right";
            break;
        case GameResources::CharacterControls::MOVE_LEFT:
            return "move left";
            break;
        case GameResources::CharacterControls::JUMP:
            return "jump";
            break;
        case GameResources::CharacterControls::SPRINT:
            return "sprint";
            break;
        case GameResources::CharacterControls::PICK_UP_ROCK:
            return "pick up rock";
            break;
        case GameResources::CharacterControls::STAND_ON_ROCK:
            return "stand on rock";
            break;
        
        default:
            break;
        }

        return "unknown";
    }
}

SettingsState::SettingsState(GameContext _context)
    :State(_context), settingsMenu(std::make_unique<alphGUI::Menu>()), menuBackgroundDim(sf::Vector2f(WIDTH, HEIGHT))
{
    menuBackgroundDim.setFillColor(sf::Color(0,0,0,175));
    buildSettingsMenu();
}

SettingsState::~SettingsState()
{
}

bool SettingsState::handleEvent(const sf::Event &event)
{
    settingsMenu->handleEvent(event, *stateContext.window);
    return false;
}

bool SettingsState::update(sf::Time deltaTime)
{
    settingsMenu->update(deltaTime);
    return true;
}

void SettingsState::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(menuBackgroundDim, states);
    target.draw(*(settingsMenu.get()), states);
}

void SettingsState::buildSettingsMenu()
{
    alphGUI::Button* backToMainMenuButton = new alphGUI::Button("go back");
    backToMainMenuButton->clickFunctionCallback = [this]()
    {
        EntityCharacter::updateCharacterControlFileContent();
        stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE);
    };
    settingsMenu->addComponent(backToMainMenuButton);

    EntityCharacter::updateCharacterControlsArray();
    for(size_t controlIndex = 0; controlIndex < GameResources::CharacterControls::CONTROLS_COUNT;++controlIndex)
    {
        alphGUI::ControlKeyButton* controlKeyButton = new alphGUI::ControlKeyButton(
            convertCharacterControlIDToString((GameResources::CharacterControls::ID)controlIndex)
             + std::string(": "), EntityCharacter::characterControls[controlIndex]);
        controlKeyButton->callBackWhenUntoggled = [controlIndex](sf::Keyboard::Key newKey){
            EntityCharacter::characterControls[controlIndex] = newKey;
        };
        controlKeyButton->setButtonSpriteScale(2.5f, 1.f);
        settingsMenu->addComponent(controlKeyButton);
    }

    settingsMenu->setPosition(50.f, 50.f);
}
