#include "entity.hpp"
#include "entity_character.hpp"
#include "entity_wall.hpp"
#include "entity_wall_square_with_gravity.hpp"
#include "collision_box.hpp"
#include "entity_rock.hpp"
#include "entity_deathblock.hpp"
#include "entity_platform.hpp"
#include "entity_checkpoint.hpp"

Entity::Entity(GameContext _context)
    :context(_context)
{
}

Entity::~Entity()
{
}

sf::Transformable *Entity::castEntityToTransformable(Entity *entity)
{

    switch (entity->getEntityType())
    {
    case GameResources::EntityType::CHARACTER:
        return static_cast<sf::Transformable*>(static_cast<EntityCharacter*>(entity));
        break;
    case GameResources::EntityType::WALL:
        return static_cast<sf::Transformable*>(static_cast<EntityWall*>(entity));
        break;
    case GameResources::EntityType::ROCK:
        return static_cast<sf::Transformable*>(static_cast<EntityRock*>(entity));
        break;
    case GameResources::EntityType::WALL_SQUARE_WITH_GRAVITY:
        return static_cast<sf::Transformable*>(static_cast<EntityWallSquareWithGravity*>(entity));
        break;
    case GameResources::EntityType::DEATH_BLOCK:
        return static_cast<sf::Transformable*>(static_cast<EntityDeathBlock*>(entity));
        break;
    case GameResources::EntityType::PLATFORM:
        return static_cast<sf::Transformable*>(static_cast<EntityPlatform*>(entity));
        break;
    case GameResources::EntityType::CHECKPOINT:
        return static_cast<sf::Transformable*>(static_cast<EntityCheckpoint*>(entity));
        break;

    default:
        break;
    }

    return nullptr;
}

Entity *Entity::createEntityFromType(GameResources::EntityType type, GameContext _context)
{
    switch (type)
    {
    case GameResources::EntityType::CHARACTER:
        return new EntityCharacter(_context);
        break;
    case GameResources::EntityType::WALL:
        return new EntityWall(_context);
        break;
    case GameResources::EntityType::ROCK:
        return new EntityRock(_context);
        break;
    case GameResources::EntityType::WALL_SQUARE_WITH_GRAVITY:
        return new EntityWallSquareWithGravity(_context);
        break;
    case GameResources::EntityType::DEATH_BLOCK:
        return new EntityDeathBlock(_context);
        break;
    case GameResources::EntityType::PLATFORM:
        return new EntityPlatform(_context);
        break;
    case GameResources::EntityType::CHECKPOINT:
        return new EntityCheckpoint(_context);
        break;
    
    default:
        std::cerr<<"couldn't create entity from type "
            <<type<<" because it doesn't exist"<<std::endl;
        break;
    }

    return nullptr;
}

std::unique_ptr<CollisionBox> Entity::getCollisionBox()
{
    throw std::logic_error("No collision box implemented for this entity, please override getCollisionBox");
}
