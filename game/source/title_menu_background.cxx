#include "title_menu_background.hpp"
#include "polygon.hpp"
#include "physical_aabb.hpp"
#include "random_functions.hpp"
#include "random_title_background_polygon.hpp"
#define BOUNDS_THICKNESS 50.f
#define NUMBER_OF_MOVING_SHAPES 15U

TitleMenuBackground::TitleMenuBackground(GameContext _context)
    :context(_context), engine(4), movingShapes(), currentMousePosition(), previousMousePosition()
{
    engine.setPixelToMetersConversionFactor(1e-2f);
    context.engine = &engine;
    buildWindowBounds();
    spawnMovingShapes();
}

TitleMenuBackground::~TitleMenuBackground()
{
}

void TitleMenuBackground::update(sf::Time deltaTime)
{
    if(attachedToCursor)
    {
        currentMousePosition = sf::Vector2f(sf::Mouse::getPosition(*context.window));
        attachedToCursor->fLinearVelocity = (currentMousePosition - previousMousePosition) / deltaTime.asSeconds();
        attachedToCursor->setPosition(currentMousePosition);       
        previousMousePosition = currentMousePosition;
    }

    engine.update(deltaTime);
}

void TitleMenuBackground::handleEvent(const sf::Event &event)
{
    switch (event.type)
    {
    case sf::Event::MouseButtonReleased:
        if(attachedToCursor)
            attachedToCursor = nullptr;
        else
        {
            previousMousePosition = sf::Vector2f(sf::Mouse::getPosition(*context.window));
            for(auto* poly : movingShapes)
            {
                if(poly->containsPoint(previousMousePosition))
                {
                    attachedToCursor = poly;
                    break;
                }
            }
        }
        break;
    default:
        break;
    }
}

void TitleMenuBackground::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    for(RandomTitleBackgroundPolygon* shape : movingShapes)
        target.draw(*shape, states);
}

void TitleMenuBackground::buildWindowBounds()
{
    PhysicalAABB* ceiling = new PhysicalAABB(WIDTH, BOUNDS_THICKNESS);
    ceiling->fRestitutionCoeff = 1.f;
    ceiling->fDynamicFriction = ceiling->fStaticFriction = 0.f;
    ceiling->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    ceiling->setPosition(WIDTH * 0.5f, -BOUNDS_THICKNESS * 0.5f);

    engine.addObject(ceiling);

    PhysicalAABB* floor = new PhysicalAABB(WIDTH, BOUNDS_THICKNESS);
    floor->fRestitutionCoeff = 1.f;
    floor->fDynamicFriction = floor->fStaticFriction = 0.f;
    floor->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    floor->setPosition(WIDTH * 0.5f, HEIGHT + BOUNDS_THICKNESS * 0.5f);

    engine.addObject(floor);

    PhysicalAABB* rightWall = new PhysicalAABB(BOUNDS_THICKNESS, HEIGHT);
    rightWall->fRestitutionCoeff = 1.f;
    rightWall->fDynamicFriction = rightWall->fStaticFriction = 0.f;
    rightWall->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    rightWall->setPosition(WIDTH + BOUNDS_THICKNESS * 0.5f, HEIGHT * 0.5f);

    engine.addObject(rightWall);

    PhysicalAABB* leftWall = new PhysicalAABB(BOUNDS_THICKNESS, HEIGHT);
    leftWall->fRestitutionCoeff = 1.f;
    leftWall->fDynamicFriction = leftWall->fStaticFriction = 0.f;
    leftWall->setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    leftWall->setPosition(-BOUNDS_THICKNESS * 0.5f, HEIGHT * 0.5f);

    engine.addObject(leftWall);
}

void TitleMenuBackground::spawnMovingShapes()
{
    for(size_t i = 0;i<NUMBER_OF_MOVING_SHAPES;++i)
    {
        RandomTitleBackgroundPolygon* shape = new RandomTitleBackgroundPolygon(context);
        shape->setPosition(lsmRandom::randomFloat(BOUNDS_THICKNESS, WIDTH - BOUNDS_THICKNESS), 
                            lsmRandom::randomFloat(BOUNDS_THICKNESS, HEIGHT - BOUNDS_THICKNESS));
        
        engine.addObject(shape);
        movingShapes.push_back(shape);
    }
}
