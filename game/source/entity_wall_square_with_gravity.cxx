#include "entity_wall_square_with_gravity.hpp"
#include "load_and_save_manager.hpp"

EntityWallSquareWithGravity::EntityWallSquareWithGravity(GameContext _context)
    :EntityRock(_context), square(context.textures->get(GameResources::SQUARE_TEXTURE))
{

    float width = square.getLocalBounds().width;
    float height = square.getLocalBounds().height;
    setPolygon({sf::Vector2f(0.f, 0.f), sf::Vector2f(width, 0.f), sf::Vector2f(width, height), sf::Vector2f(0.f, height)});
    setMass(5.f);
    fRestitutionCoeff = fInitialRestCoeff = 1.f;
    fStaticFriction = 0.7f;
    fGravitySensitive = true;
}

EntityWallSquareWithGravity::~EntityWallSquareWithGravity()
{
}

GameResources::EntityType EntityWallSquareWithGravity::getEntityType() const
{
    return GameResources::WALL_SQUARE_WITH_GRAVITY;
}

void EntityWallSquareWithGravity::loadEntity(const nlohmann::json &jsonToLoadFrom)
{
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadFrom);
    context.engine->addObject(this);
}

void EntityWallSquareWithGravity::saveEntity(nlohmann::json &jsonToSaveTo)
{

    nlohmann::json rockJSON;
    
    rockJSON[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getEntityType();
    LoadAndSaveManager::saveTransformable(*this, rockJSON);

    jsonToSaveTo.push_back(rockJSON);
}

void EntityWallSquareWithGravity::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(square, states);
}
