#include "game_state.hpp"
#include "state_stack.hpp"
#include "pause_state.hpp"
#include "game_level.hpp"
#include "finish_game_state.hpp"
#include <fstream>
#include "json.hpp"

namespace
{
    void modifyPlayerLevelProgressFile(unsigned int newLevelProgress)
    {
        std::ofstream playerLevelProgressFile(PLAYER_PROGRESS_DIR + std::string("do_not_open_this_file.txt"));
        if(!playerLevelProgressFile.is_open())
            throw std::runtime_error("couldn't open player's level progress file");

        playerLevelProgressFile << newLevelProgress;
        
        playerLevelProgressFile.close();
    }
}

GameState::GameState(sf::RenderWindow &contextWindow, StateStack &stateStack, size_t _levelToLoadNameIndex, bool isLevelOfficial)
    :State(contextWindow, stateStack), gameTextures(), gameFonts(), gameSounds(), gamePhysicsEngine(6U), activeLevel(),
    gameLevelsInOrder(isLevelOfficial ? getOfficialLevelNamesInOrder() : getNonOfficialLevelNamesInOrder()),
    levelToLoadNameIndex(_levelToLoadNameIndex), isLevelLoadedOfficial(isLevelOfficial)
{
    
    stateContext.textures = &gameTextures;
    stateContext.fonts = &gameFonts;
    stateContext.sounds = &gameSounds;
    stateContext.engine = &gamePhysicsEngine;
    gamePhysicsEngine.setPixelToMetersConversionFactor(1e-2f);
    gamePhysicsEngine.fGravityConstant *= 2.f;
}



GameState::~GameState()
{
}



bool GameState::handleEvent(const sf::Event &event)
{
    activeLevel->handleEvent(event);

    if(event.type == sf::Event::KeyPressed)
    {
        switch(event.key.code)
        {
            case sf::Keyboard::Escape:
                stateContext.stateStack->requestAction(StateStack::Actions::PUSH_STATE, [this]()
                {
                    return new PauseState(stateContext);
                });
                break;
            case sf::Keyboard::LControl:
                hasPressedControlKey = true;
                break;
            case sf::Keyboard::S:
                if(hasPressedControlKey)
                    saveGame();
                break;
            default: break;
        }
    }
    if(event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::LControl)
        hasPressedControlKey = false;
    return false;
}

std::vector<std::string> GameState::getOfficialLevelNamesInOrder()
{
    std::vector<std::string> levelNames;
    std::filesystem::directory_iterator iter(LEVEL_DIR + std::string("official_levels/"));
    for(;iter != std::filesystem::end(iter);++iter)
    {
        std::string fileNameOnly = iter->path().filename().stem().string();
        levelNames.push_back(fileNameOnly);
    }
    std::sort(levelNames.begin(), levelNames.end(), [](const std::string& s1, const std::string& s2)
    {
        int numS1 = atoi(s1.substr(6).c_str());
        int numS2 = atoi(s2.substr(6).c_str());
        return numS1 <= numS2;
    });
    
    return levelNames;
}

std::vector<std::string> GameState::getNonOfficialLevelNamesInOrder()
{
    std::vector<std::string> levelNames;
    std::map<std::string, std::filesystem::file_time_type> levelWriteTimes;
    std::filesystem::directory_iterator iter(LEVEL_DIR + std::string("non_official_levels/"));
    for(;iter != std::filesystem::end(iter);++iter)
    {
        std::string fileNameOnly = iter->path().filename().stem().string();
        levelNames.push_back(fileNameOnly);
        levelWriteTimes[fileNameOnly] = iter->last_write_time();
    }
    std::sort(levelNames.begin(), levelNames.end(), [&levelWriteTimes](const std::string& s1, const std::string& s2)
    {
        return levelWriteTimes[s1] <= levelWriteTimes[s2];
    });
    
    return levelNames;
}

bool GameState::update(sf::Time deltaTime)
{

    activeLevel->update(deltaTime);

    gamePhysicsEngine.update(deltaTime);

    if(activeLevel->canChangeLevel() && isAtForeGround)
    {
        if(++levelToLoadNameIndex >= gameLevelsInOrder.size())    //the game's finish
        {
            activeLevel->popInteractionRule();
            stateContext.stateStack->requestAction(StateStack::Actions::PUSH_STATE, [this]()
            {
                return new FinishGameState(stateContext);
            });
        }
        else
        {
            activeLevel->loadLevel((isLevelLoadedOfficial ? "official_levels/" : "non_official_levels/") + gameLevelsInOrder[levelToLoadNameIndex]);
        }
        
        if(isLevelLoadedOfficial)
            modifyPlayerLevelProgressFile(levelToLoadNameIndex);
    }
    return false;
}


void GameState::buildState(volatile float &progress, sf::Mutex &loadingThreadMutex)
{
    loadingThreadMutex.lock();
    progress = 0.f;
    loadingThreadMutex.unlock();

    float progressIncrement = 1.f / (GameResources::TEXTURES_COUNT + GameResources::FONTS_COUNT + GameResources::SOUNDS_COUNT + 4U);

    //load the textures
    gameTextures.load(TEXTURE_DIR + std::string("square.png"), GameResources::Textures::SQUARE_TEXTURE);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    gameTextures.load(TEXTURE_DIR + std::string("particle_default.png"), GameResources::Textures::DEFAULT_PARTICLE_TEXTURE);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    gameTextures.load(TEXTURE_DIR + std::string("particle_fancy.png"), GameResources::Textures::FANCY_PARTICLE_TEXTURE);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    gameTextures.load(TEXTURE_DIR + std::string("checkpoint.png"), GameResources::Textures::CHECKPOINT_TEXTURE);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    gameTextures.load(TEXTURE_DIR + std::string("platform.png"), GameResources::Textures::PLATFORM_TEXTURE);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    gameTextures.load(TEXTURE_DIR + std::string("arrow.png"), GameResources::Textures::ARROW_TEXTURE);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    gameFonts.load(FONT_DIR + std::string("title_font.ttf"), GameResources::Fonts::TITLE_FONT);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    gameTextures.load(TEXTURE_DIR + std::string("main_character.png"), GameResources::Textures::MAIN_CHARACTER);

    activeLevel.reset(new GameLevel(stateContext));
    activeLevel->loadLevel((isLevelLoadedOfficial ? "official_levels/" : "non_official_levels/") + gameLevelsInOrder[levelToLoadNameIndex]);
    stateContext.levelLoaded = activeLevel.get();

    loadingThreadMutex.lock();
    progress = 1.f;
    loadingThreadMutex.unlock();
}


void GameState::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(*(activeLevel.get()), states);
}

void GameState::saveGame()
{
    activeLevel->saveLevel();
}
