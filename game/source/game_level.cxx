#include "game_level.hpp"
#include "entity_character.hpp"
#include "collision_box.hpp"
#include "character_entity_identifiers.hpp"
#include "entity_platform.hpp"
#include "entity_checkpoint.hpp"
#include "entity_splash_particle_emitter.hpp"
#include "state_stack.hpp"
#include "finish_game_state.hpp"
#include <fstream>
#include <filesystem>

//gameplay specific stuff
namespace
{
    bool isEntityJumpableOn(GameResources::EntityType entityType)
    {
        switch (entityType)
        {
        case GameResources::EntityType::DEATH_BLOCK:
        case GameResources::EntityType::WALL:
        case GameResources::EntityType::WALL_SQUARE_WITH_GRAVITY:
        case GameResources::EntityType::ROCK:
        case GameResources::EntityType::PLATFORM:
            return true;
            break;
        
        default:
            break;
        }

        return false;
    }
}

GameLevel::GameLevel(GameContext _context)
    :context(_context), levelStartPosition(), levelLoaded(""), levelView(context.window->getDefaultView()), levelBounds(0,0,WIDTH, HEIGHT),
     littleSquareMan(nullptr), entitiesToProcess(),
     interactionRules(), interactionCommands(), entitiesToRender(), deadEntities()
{
}

GameLevel::~GameLevel()
{
    for(auto& layer : entitiesToRender)
        for(auto* entity : layer)
            delete entity;
}

void GameLevel::update(sf::Time deltaTime)
{

    //regular updates and check for DEAD state
    for(auto& layer : entitiesToRender)
    {
        for(auto* entity : layer)
        {
            entity->update(deltaTime);
            if(entity->entityState == EntityState::DEAD)
                deadEntities.push_back(entity);
        }
    }

    levelView.setCenter(littleSquareMan->getPosition().x, levelView.getCenter().y);
    keepCameraInsideLevelBounds();    

    executeCommands();
    
    executeInteractionRules();

    //remove and delete the dead entities
    for(auto* deadEntity : deadEntities)
    {
        entitiesToProcess[deadEntity->getEntityType()].remove(deadEntity);
        entitiesToRender[deadEntity->getEntityRenderingLayer()].remove(deadEntity);
        delete deadEntity;
    }
    deadEntities.clear();
}

void GameLevel::handleEvent(const sf::Event &event)
{
    for(auto& layer : entitiesToRender)
        for(auto* entity : layer)
            entity->handleEvent(event);
}

void GameLevel::loadLevel(const std::string& levelToLoad)
{
    context.levelLoaded = this;
    changeLevel = false;
    clearAllEntities();
    interactionRules.clear();
    interactionCommands.clear();
    noLongerInteractingRules.clear();
    levelLoaded = levelToLoad;
    std::ifstream loadingFile(LEVEL_DIR + levelLoaded + ".json");
    if(!loadingFile.is_open())
        throw std::runtime_error("couldn't open saved level file: " + std::string(LEVEL_DIR) + levelLoaded + ".json" + ", failed to load level");

    nlohmann::json levelJSON;
    loadingFile >> levelJSON;

    levelBounds = sf::FloatRect(levelJSON[GameResources::JSON_GAME_LEVELBOUNDS_STRING][0],
                                levelJSON[GameResources::JSON_GAME_LEVELBOUNDS_STRING][1],
                                levelJSON[GameResources::JSON_GAME_LEVELBOUNDS_STRING][2],
                                levelJSON[GameResources::JSON_GAME_LEVELBOUNDS_STRING][3]);
    levelStartPosition = sf::Vector2f(levelJSON[GameResources::JSON_GAME_LEVEL_INITIAL_CHARACTER_POSITION][0], 
                            levelJSON[GameResources::JSON_GAME_LEVEL_INITIAL_CHARACTER_POSITION][1]);
    // load all the entities
    for(auto& json : levelJSON[GameResources::JSON_ENTITIES_ARRAY_STRING])
    {
        Entity* newLoadedEntity = Entity::createEntityFromType((GameResources::EntityType)json[GameResources::JSON_ENTITY_TYPE_STRING], context);
        newLoadedEntity->levelContext = this;
        newLoadedEntity->loadEntity(json);
        entitiesToProcess[newLoadedEntity->getEntityType()].push_back(newLoadedEntity);
        if(isEntityJumpableOn(newLoadedEntity->getEntityType()))
            entitiesToProcess[GameResources::EntityType::ENTITIES_JUMPABLE_ON].push_back(newLoadedEntity);
        entitiesToRender[newLoadedEntity->getEntityRenderingLayer()].push_back(newLoadedEntity);
    }

    loadingFile.close();

    littleSquareMan = static_cast<EntityCharacter*>(entitiesToProcess[GameResources::EntityType::CHARACTER].front());
    littleSquareMan->setPosition(levelStartPosition);
    if(!littleSquareMan)
        throw std::logic_error("couldn't load the level, no main character");

    levelView.setCenter(littleSquareMan->getPosition().x, levelView.getCenter().y);
    keepCameraInsideLevelBounds();
    setupInteractionRules();
    //don't forget to load character's controls
    EntityCharacter::updateCharacterControlsArray();
}

void GameLevel::saveLevel()
{
    std::ofstream savingFile(LEVEL_DIR + levelLoaded + ".json", std::ios_base::trunc);
    if(!savingFile.is_open())
        throw std::runtime_error("couldn't open or create saving file");

    size_t totalEntityCount = 0;
    for(auto& layer : entitiesToRender)
        totalEntityCount += layer.size();
    nlohmann::json levelJSON;
    
    levelJSON[GameResources::JSON_GAME_LEVELBOUNDS_STRING] = {levelBounds.left, levelBounds.top, levelBounds.width, levelBounds.height};
    levelJSON[GameResources::JSON_GAME_LEVEL_INITIAL_CHARACTER_POSITION] = {levelStartPosition.x, levelStartPosition.y};
    //save all the entities
    levelJSON[GameResources::JSON_ENTITIES_ARRAY_STRING] = nlohmann::json::array();
    for(auto& layer : entitiesToRender)
        for(auto* entity : layer)
            entity->saveEntity(levelJSON[GameResources::JSON_ENTITIES_ARRAY_STRING]);

    savingFile << levelJSON;

    savingFile.close();
}

bool GameLevel::canChangeLevel() const
{
    return changeLevel;
}

void GameLevel::pushInteractionCommand(const InteractionCommand &command)
{
    interactionCommands.push_back(command);
}

void GameLevel::pushInteractionRule(const InteractionRule &rule)
{
    interactionRules.push_back(rule);
}

void GameLevel::pushNoLongerInteractingRule(const InteractionRule &rule)
{
    noLongerInteractingRules.push_back(std::make_pair(rule, NoLongerInteractingInformation{nullptr, nullptr}));
}

void GameLevel::popNoLongerInteractingRule()
{
    noLongerInteractingRules.pop_back();
}

void GameLevel::popInteractionRule()
{
    interactionRules.pop_back();
}

void GameLevel::clearAllEntities()
{
    for(auto& layer : entitiesToRender)
    {
        for(auto* entity : layer)
            delete entity;

        layer.clear();
    }
    entitiesToRender.fill(std::list<Entity*>());
    entitiesToProcess.fill(std::list<Entity*>());
    deadEntities.clear();
    littleSquareMan = nullptr;
}

std::string GameLevel::getLevelName() const
{
    return levelLoaded;
}

void GameLevel::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.setView(levelView);
    for(unsigned int layer = GameResources::RenderingLayers::BACKGROUND_LAYER;layer < GameResources::RenderingLayers::UI_LAYER;++layer)
        for(auto* entity : entitiesToRender[layer])
            target.draw(*entity, states);

/*     sf::RectangleShape levelBoundsRect(sf::Vector2f(levelBounds.width, levelBounds.height));
    levelBoundsRect.setPosition(levelBounds.left, levelBounds.top);
    levelBoundsRect.setFillColor(sf::Color(255,0,0,125));

    target.draw(levelBoundsRect, states); */
    target.setView(target.getDefaultView());
    for(auto* entity :  entitiesToRender[GameResources::UI_LAYER])
        target.draw(*entity, states);
}

void GameLevel::keepCameraInsideLevelBounds()
{
    sf::FloatRect cameraBounds(levelView.getCenter().x - levelView.getSize().x * 0.5f,
                                levelView.getCenter().y - levelView.getSize().y * 0.5f,
                                levelView.getSize().x, levelView.getSize().y);
    sf::FloatRect intersectRectangle;
    (void)cameraBounds.intersects(levelBounds, intersectRectangle);
    if(!MathOperations::equalFloat(intersectRectangle.width, cameraBounds.width, 1e-3f))
    {
        sf::Vector2f newTopLeftCameraPosition(((levelBounds.contains(cameraBounds.left, cameraBounds.top)) ? 
                                sf::Vector2f(levelBounds.left + levelBounds.width - cameraBounds.width, cameraBounds.top)
                             :  sf::Vector2f(levelBounds.left, cameraBounds.top)));
        //centering the position
        newTopLeftCameraPosition.x += cameraBounds.width * 0.5f;
        newTopLeftCameraPosition.y += cameraBounds.height * 0.5f;

        levelView.setCenter(newTopLeftCameraPosition);
    }
}

void GameLevel::setupInteractionRules()
{
    InteractionRule attachedCharacterToPlatformRule;
    attachedCharacterToPlatformRule.firstEntity = GameResources::EntityType::CHARACTER;
    attachedCharacterToPlatformRule.secondEntity = GameResources::EntityType::PLATFORM;
    attachedCharacterToPlatformRule.action = [](Entity* character, Entity* platform, const OverlapData& collisionData)
    {
        EntityPlatform* realPlatform = static_cast<EntityPlatform*>(platform);
        EntityCharacter* realCharacter = static_cast<EntityCharacter*>(character);
        float yCoordNormalCollisionCorrectlySigned = (abs(collisionData.fCollisionNormal.y) * 
                                        (realCharacter->getPosition().y - realPlatform->getPosition().y) < 0.f ? -1.f : 1.f);

        if(MathOperations::equalFloat(yCoordNormalCollisionCorrectlySigned, -1.f, 1e-7f) && MathOperations::equalFloat(collisionData.fCollisionNormal.x, 0.f, 1e-7f))
        {
            realPlatform->attachedCharacter = realCharacter;
        }
    };
    pushInteractionRule(attachedCharacterToPlatformRule);
    
    InteractionRule checkpointInteractionRule;
    checkpointInteractionRule.firstEntity = GameResources::EntityType::CHARACTER;
    checkpointInteractionRule.secondEntity = GameResources::EntityType::CHECKPOINT;
    checkpointInteractionRule.action = [this](Entity*, Entity* checkpoint, const OverlapData&)
    {
        EntityCheckpoint* realCheckpoint = static_cast<EntityCheckpoint*>(checkpoint);
        if(realCheckpoint->getCheckpointType() == EntityCheckpoint::CheckpointType::END)
        {
            //change level
            if(!changeLevel)
                changeLevel = true;
        }
        else
            realCheckpoint->setCheckpointType(EntityCheckpoint::CheckpointType::START);
    };

    pushInteractionRule(checkpointInteractionRule);


    InteractionRule deathBlockCharacterRule;
    deathBlockCharacterRule.firstEntity = GameResources::EntityType::CHARACTER;
    deathBlockCharacterRule.secondEntity = GameResources::EntityType::DEATH_BLOCK;
    deathBlockCharacterRule.action = [this](Entity* character, Entity*, const OverlapData&)
    {
        EntityCharacter* realCharacter = static_cast<EntityCharacter*>(character);
        realCharacter->setPosition(levelStartPosition);
    };

    pushInteractionRule(deathBlockCharacterRule);

}

void GameLevel::executeCommands()
{
    InteractionCommand command;
    while(!interactionCommands.empty())
    {
        command = interactionCommands.back();
        for(auto* entity : entitiesToProcess[command.entityToSendCommandTo])
            command.action(entity);

        interactionCommands.pop_back();
    }
}

void GameLevel::executeInteractionRules()
{
    //interaction rules
    for(InteractionRule rule : interactionRules)
    {
        OverlapData collisionData;
        for(auto* firstEntity : entitiesToProcess[rule.firstEntity])
        {
            for(auto* secondEntity : entitiesToProcess[rule.secondEntity])
            {
                //test collision
                if(firstEntity->getCollisionBox()->doesCollideWith(*(secondEntity->getCollisionBox().get()), collisionData))
                {
                    rule.action(firstEntity, secondEntity, collisionData);
                }
            }
        }
    }

    //no longer interacting rules
    for(auto& rulePair : noLongerInteractingRules)
    {
        OverlapData collisionData;
        for(auto* firstEntity : entitiesToProcess[rulePair.first.firstEntity])
        {
            for(auto* secondEntity : entitiesToProcess[rulePair.first.secondEntity])
            {
                //test collision
                if(firstEntity->getCollisionBox()->doesCollideWith(*(secondEntity->getCollisionBox().get()), collisionData))
                {
                    rulePair.second.firstEntity = firstEntity;
                    rulePair.second.secondEntity = secondEntity;
                    firstEntity->entityState = EntityState::ALIVE;
                    secondEntity->entityState = EntityState::ALIVE;
                }
                else
                {   
                    //if the same entity pair was colliding before but is no longer colliding now, then trigger the action
                    if(rulePair.second.firstEntity && (rulePair.second.firstEntity == firstEntity && rulePair.second.secondEntity == secondEntity))
                    {
                        rulePair.first.action(firstEntity, secondEntity, collisionData);
                        rulePair.second.firstEntity = nullptr;
                        rulePair.second.secondEntity = nullptr;
                    }
                }
            }
        }
    }
}
