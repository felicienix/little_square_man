#include "entity_deathblock.hpp"
#include "game_level.hpp"
#include "entity_character.hpp"

EntityDeathBlock::EntityDeathBlock(GameContext _context)
    :EntityWall(_context)
{
    wallSprite.setColor(sf::Color(125,255,255));
}

EntityDeathBlock::~EntityDeathBlock()
{
}

GameResources::EntityType EntityDeathBlock::getEntityType() const
{
    return GameResources::EntityType::DEATH_BLOCK;
}

void EntityDeathBlock::loadEntity(const nlohmann::json &jsonToLoadFrom)
{
    EntityWall::loadEntity(jsonToLoadFrom);
    wallSprite.setColor(sf::Color(125,255,255));
}

void EntityDeathBlock::saveEntity(nlohmann::json &jsonToSaveTo)
{
    EntityWall::saveEntity(jsonToSaveTo);
    jsonToSaveTo[jsonToSaveTo.size() - 1][GameResources::JSON_ENTITY_TYPE_STRING] = (int)getEntityType();
}
