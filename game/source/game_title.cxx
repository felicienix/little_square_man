#include "game_title.hpp"
#include "animation.hpp"
#include "entities_resources_and_identifiers.hpp"
#include "character_entity_identifiers.hpp"

GameTitle::GameTitle(GameContext _context)
    :context(_context), title(_context.textures->get(MenuResources::Textures::GAME_TITLE)), 
    character(_context.textures->get(MenuResources::Textures::TITLE_CHARACTER), 
        sf::IntRect(0,0,GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT)),
        characterAnimation(new Animation(_context.textures->get(MenuResources::Textures::TITLE_CHARACTER),
            sf::Vector2i(0,0), 1))
{
    characterAnimation->setDuration(sf::seconds(GameResources::CHARACTER_IDLE_DURATION));
    characterAnimation->setFrameSize(sf::Vector2i(GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT));
    characterAnimation->setNumFrames(GameResources::CHARACTER_IDLE_NUM_FRAMES);
    characterAnimation->Repeat(true);
    characterAnimation->Restart();

    character.setPosition(title.getLocalBounds().width - GameResources::CHARACTER_SPRITE_WIDTH - 20.f, -20.f);
    setOrigin(title.getLocalBounds().width * 0.5f, 
        title.getLocalBounds().height * 0.5f);
}

GameTitle::~GameTitle()
{
    delete characterAnimation;
}

void GameTitle::update(sf::Time deltaTime)
{
    characterAnimation->update(deltaTime, character);
}

void GameTitle::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(title, states);
    target.draw(character, states);
}
