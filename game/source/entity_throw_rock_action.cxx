#include "entity_throw_rock_action.hpp"
#include "entity_character.hpp"

EntityThrowRockAction::EntityThrowRockAction(GameContext _context, EntityCharacter *_character)
    :Entity(_context), character(_character), arrow(_context.textures->get(GameResources::ARROW_TEXTURE)), currentStep(FINISH), initialArrowWidth(0.f)
{
    initialArrowWidth = arrow.getGlobalBounds().width;
    arrow.setOrigin(arrow.getOrigin().x, arrow.getGlobalBounds().height * 0.5f);
}

EntityThrowRockAction::~EntityThrowRockAction()
{
}

void EntityThrowRockAction::update(sf::Time deltaTime)
{
    int charDir = character->whichWayIsCharacterFacing();
    float angleOffset = (charDir == -1) ? 180.0f : 0.f;

    switch (currentStep)
    {
    case START:
        arrow.setRotation(angleOffset);
        break;
    case CHOOSE_DIR:
        accuTimeRotationAngle += deltaTime;
        arrowRotationAngle = asin(sin(accuTimeRotationAngle.asSeconds()));
        arrow.setRotation(angleOffset + arrowRotationAngle * 180.f / M_PI);
        break;
    case CHOOSE_SPEED:
        accuTimeSpeedDial += deltaTime;
        arrowSpeedDial = fabs(sin(accuTimeSpeedDial.asSeconds()));
        arrow.setScale(arrowSpeedDial, 1.f);
        break;
    
    default:
        break;
    }


}

GameResources::EntityType EntityThrowRockAction::getEntityType() const
{
    return GameResources::EntityType::THROW_ROCK_ACTION;
}

void EntityThrowRockAction::startAction()
{
    currentStep = START;
    float xPosition = (character->getBoundingRectangle().width * 0.5f);
    setPosition(xPosition, 0.f);
    arrow.setScale(1.f, 1.f);
    arrow.setRotation(0.f);
}

void EntityThrowRockAction::nextStep()
{
    if(currentStep != FINISH)
        currentStep = (Steps)(currentStep + 1);
}

bool EntityThrowRockAction::isActionFinished() const
{
    return currentStep == FINISH;
}

sf::Vector2f EntityThrowRockAction::getFinalVelocity() const
{
    return 1e3f * arrowSpeedDial * sf::Vector2f(cos(arrowRotationAngle), sin(arrowRotationAngle)) * (float)character->whichWayIsCharacterFacing();
}

void EntityThrowRockAction::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if(currentStep != FINISH)
    {
        states.transform *= getTransform();

        target.draw(arrow, states);
    }
}
