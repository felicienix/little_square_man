#include "particle_system.hpp"

ParticleSystem::ParticleSystem(const sf::Texture& particleTexture, const PhysicsEngine& contextPhysicsEngine)
	:engine(contextPhysicsEngine), texture(particleTexture),
	particles(), vertexArray(sf::Quads), needsVertexUpdate(true)
{
}

ParticleSystem::~ParticleSystem()
{
}

void ParticleSystem::addParticle(const sf::Vector2f& position, sf::Color color, sf::Time lifeTime, const sf::Vector2f& velocity)
{
	Particle particle;
	particle.position = position;
	particle.color = color;
	particle.lifeTime = lifeTime;
	particle.timeToLive = lifeTime;
	particle.velocity = velocity;

	particles.push_back(particle);
}

void ParticleSystem::update(sf::Time dt)
{
	
	while (!particles.empty() && particles.front().timeToLive <= sf::Time::Zero)
	{
		particles.pop_front();
	}

	for (Particle& particle : particles)
	{
		particle.timeToLive -= dt;
		if(enableGravityOnParticles)
			particle.velocity.y += 0.5f * engine.fGravityConstant * dt.asSeconds() * engine.getInversePixelToMetersConversionFactor();
		particle.position += particle.velocity * dt.asSeconds();
	}

	needsVertexUpdate = true;
}


void ParticleSystem::draw(sf::RenderTarget& target, sf::RenderStates states) const
{

	if (needsVertexUpdate)
	{
		computeVertices();
		needsVertexUpdate = false;
	}

	states.texture = &texture;

	target.draw(vertexArray, states);
}

void ParticleSystem::addVertex(float worldX, float worldY, float texCoordX, float texCoordY, const sf::Color& color) const
{
	sf::Vertex vertex;
	vertex.position = sf::Vector2f(worldX, worldY);
	vertex.texCoords = sf::Vector2f(texCoordX, texCoordY);
	vertex.color = color;

	vertexArray.append(vertex);
}

void ParticleSystem::computeVertices() const
{
	sf::Vector2f size(texture.getSize());
	sf::Vector2f half = size * 0.5f;

	//refill vertex array
	vertexArray.clear();
	for (const Particle& particle : particles)
	{
		sf::Vector2f pos = particle.position;
		sf::Color color = particle.color;

		float ratio = particle.timeToLive.asSeconds() / particle.lifeTime.asSeconds();
		color.a = static_cast<sf::Uint8>(255 * std::max(ratio, 0.f));

		addVertex(pos.x - half.x, pos.y - half.y, 0.f, 0.f, color);
		addVertex(pos.x + half.x, pos.y - half.y, size.x, 0.f, color);
		addVertex(pos.x + half.x, pos.y + half.y, size.x, size.y, color);
		addVertex(pos.x - half.x, pos.y + half.y, 0.f, size.y, color);
	}
}
