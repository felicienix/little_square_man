#include "entity_particle_emitter.hpp"
#include "particle_emitter_specifier.hpp"

EntityParticleEmitter::EntityParticleEmitter(GameContext _context, GameResources::ParticleTypes typeOfParticles)
    :Entity(_context),
     emitterSpecifier(ParticleEmitterSpecifier::createParticleEmitterSpecifier(typeOfParticles)),
     amountOfParticlesEmittedPerSecond(emitterSpecifier->numberOfParticlesPerSecond),
     particleSystem(context.textures->get(emitterSpecifier->particleTextureID), *_context.engine), 
     emittingInterval(sf::seconds(1.f/(float)amountOfParticlesEmittedPerSecond)), emittingAccu(emittingInterval)
{
    particleSystem.enableGravityOnParticles = emitterSpecifier->isGravitySensitive;
}

EntityParticleEmitter::~EntityParticleEmitter()
{
}

void EntityParticleEmitter::update(sf::Time deltaTime)
{
    particleSystem.update(deltaTime);

    if(isEmitting)
    {
        emittingAccu += deltaTime;
        while (emittingAccu >= emittingInterval)
        {
            emittingAccu -= emittingInterval;

            particleSystem.addParticle(emitterSpecifier->particleInitialPosition(*this), emitterSpecifier->particleColor(deltaTime),
             emitterSpecifier->particleLifeTime, emitterSpecifier->particleVelocity(deltaTime));
        }
    }
}

GameResources::EntityType EntityParticleEmitter::getEntityType() const
{
    return GameResources::EntityType::PARTICLE_EMITTER;
}

GameResources::RenderingLayers EntityParticleEmitter::getEntityRenderingLayer() const
{
    return GameResources::RenderingLayers::INTERACTION_BOTTOM_LAYER;
}

void EntityParticleEmitter::loadEntity(const nlohmann::json &jsonToLoadFrom)
{
}

void EntityParticleEmitter::saveEntity(nlohmann::json &jsonToSaveTo)
{
}

void EntityParticleEmitter::startEmitting()
{
    isEmitting = true;
}

void EntityParticleEmitter::stopEmitting()
{
    isEmitting = false;
    emittingAccu = emittingInterval;
}

void EntityParticleEmitter::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(particleSystem, states);
}
