#include "entity_splash_particle_emitter.hpp"
#include "particle_emitter_specifier.hpp"

EntitySplashParticleEmitter::EntitySplashParticleEmitter(GameContext _context, GameResources::ParticleTypes typeOfParticles, sf::Time _emissionTime, float _width)
    :EntityParticleEmitter(_context, typeOfParticles), width(_width), emissionTime(_emissionTime)
{
}

EntitySplashParticleEmitter::~EntitySplashParticleEmitter()
{
}

void EntitySplashParticleEmitter::update(sf::Time deltaTime)
{
    particleSystem.update(deltaTime);
    if(isEmitting)
    {

        emittingAccu += deltaTime;
        while(emittingAccu >= emittingInterval)
        {
            emittingAccu -= emittingInterval;
            for(unsigned int particlePerSplashIncr = 0;particlePerSplashIncr < ParticlesPerSplashNumber;++particlePerSplashIncr)
            {
                particleSystem.addParticle(
                    emitterSpecifier->particleInitialPosition(*this) + (float)(1 - 2*((int)(particlePerSplashIncr) % 2)) * sf::Vector2f(width * 0.5f, 0.f)
                    , emitterSpecifier->particleColor(deltaTime / (float)ParticlesPerSplashNumber), emitterSpecifier->particleLifeTime
                    , emitterSpecifier->particleVelocity(deltaTime / (float)ParticlesPerSplashNumber)
                );
            }
        }

        emissionTimeAccu += deltaTime;
        if(emissionTimeAccu >= emissionTime)
        {
            emissionTimeAccu = sf::Time::Zero;
            stopEmitting();
        }
    }
}
