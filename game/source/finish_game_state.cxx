#include "finish_game_state.hpp"
#include "gui_menu.hpp"
#include "gui_button.hpp"
#include "state_stack.hpp"
#include "loading_state.hpp"
#include "title_menu_state.hpp"

/*
    IDEA TODO: make the finish game state something where the player 
    becomes invicible and can just walk around the level, potentially discovering easter eggs ?

*/

FinishGameState::FinishGameState(GameContext _context)
    :State(_context), finishMenu(std::make_unique<alphGUI::Menu>()), finishTitle("The end !", _context.fonts->get(GameResources::TITLE_FONT), 90U)
{
    buildFinishMenu();
    finishTitle.setPosition(WIDTH * 0.5f - finishTitle.getLocalBounds().width * 0.5f, finishTitle.getCharacterSize());
    finishTitle.setFillColor(sf::Color::Black);
}

FinishGameState::~FinishGameState()
{
}

bool FinishGameState::handleEvent(const sf::Event &event)
{
    finishMenu->handleEvent(event, *stateContext.window);
    return false;
}

bool FinishGameState::update(sf::Time deltaTime)
{
    finishMenu->update(deltaTime);
    return true;
}

void FinishGameState::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    sf::RectangleShape dimBackground(sf::Vector2f(WIDTH, HEIGHT));
    dimBackground.setFillColor(sf::Color(0,0,0,110));
    
    target.setView(target.getDefaultView());

    target.draw(dimBackground, states);
    target.draw(*(finishMenu.get()), states);
    target.draw(finishTitle, states);
}

void FinishGameState::buildFinishMenu()
{

    alphGUI::Button* quitButton = new alphGUI::Button("Quit");
    quitButton->clickFunctionCallback = [this]()
    {
        sf::RenderWindow* contextWindow = stateContext.window;
        StateStack* contextStack = stateContext.stateStack;
        stateContext.stateStack->requestAction(StateStack::Actions::POP_ALL_STATES);
        stateContext.stateStack->requestAction(StateStack::Actions::PUSH_STATE, [contextWindow, contextStack]()
        {
            return new LoadingState<TitleMenuState>(*contextWindow, *contextStack);
        });
    };

    finishMenu->addComponent(quitButton);

    finishMenu->setPosition(sf::Vector2f(WIDTH, HEIGHT) * 0.5f - sf::Vector2f(finishMenu->getBoundingRectangle().width,
                                                                finishMenu->getBoundingRectangle().height) * 0.5f);
}
