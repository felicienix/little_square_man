#include "state.hpp"
#include "state_stack.hpp"

State::State(sf::RenderWindow &contextWindow, StateStack& stateStack)
    :stateContext(), isAtForeGround(true)
{
    stateContext.window = &contextWindow;
    stateContext.stateStack = &stateStack;
}

State::State(GameContext _context)
    :stateContext(_context), isAtForeGround(true)
{
}

State::~State()
{
}

void State::setForeGroundState(bool isForeGround)
{
    isAtForeGround = isForeGround;
}

bool State::isStateAtForeGround() const
{
    return isAtForeGround;
}
