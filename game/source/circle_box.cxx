#include "circle_box.hpp"
#include "physical_circle.hpp"
#include "physical_aabb.hpp"
#include "polygon.hpp"
#include "polygon_box.hpp"
#include "aabb_box.hpp"

CircleBox::CircleBox()
    :CollisionBox(), center(0.f,0.f), radius(0.f)
{
}

CircleBox::CircleBox(const sf::Vector2f &_center, float _radius)
    :CollisionBox(), center(_center), radius(_radius)
{
}

CircleBox::~CircleBox()
{
}

bool CircleBox::doesCollideWith(const CollisionBox &otherBox, OverlapData &collisionData)
{
    switch (otherBox.getCollisionBoxType())
    {
    case Type::POLYGON:
        {
            const PolygonBox& otherPoly = static_cast<const PolygonBox&>(otherBox);
            return PhysicalCircle::doCircleCollideWithPoly(otherPoly.polygonPoints, center, radius, collisionData);
        }
        break;
    case Type::AABB:
        {
            const AABBBox& otherAABB = static_cast<const AABBBox&>(otherBox);
            return PhysicalAABB::doCircleAndAABBCollide(center, radius, otherAABB.box, collisionData);
        }
        break;
    case Type::CIRCLE:
        {
            const CircleBox& otherCircle = static_cast<const CircleBox&>(otherBox);
            return PhysicalCircle::doCircleCollideWithCircle(center, radius, otherCircle.center, otherCircle.radius, collisionData);
        }
        break;
    
    default:
        break;
    }

    return false;
}

CollisionBox::Type CircleBox::getCollisionBoxType() const
{
    return Type::CIRCLE;
}

void CircleBox::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    sf::CircleShape visualCircle(radius);
    visualCircle.setOrigin(radius, radius);
    visualCircle.setFillColor(sf::Color::Transparent);
    visualCircle.setOutlineColor(sf::Color::Green);
    visualCircle.setOutlineThickness(1.f);
    visualCircle.setPosition(center);

    target.draw(visualCircle, states);
}
