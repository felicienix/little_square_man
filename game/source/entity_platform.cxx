#include "entity_platform.hpp"
#include "game_level.hpp"
#include "entity_character.hpp"
#include "random_functions.hpp"
#include "aabb_box.hpp"
#include "load_and_save_manager.hpp"


EntityPlatform::EntityPlatform(GameContext _context)
    :Entity(_context), movingStartDuration(sf::seconds(lsmRandom::randomFloat(0.5f, 1.5f))),
    platformBeginning(_context.textures->get(GameResources::Textures::PLATFORM_TEXTURE),
                        sf::IntRect(0,0, GameResources::WALL_SQUARE_SIZE, GameResources::WALL_SQUARE_SIZE)),
    platformMiddle(_context.textures->get(GameResources::Textures::PLATFORM_TEXTURE), sf::IntRect(2 * GameResources::WALL_SQUARE_SIZE, 0,
                                            GameResources::WALL_SQUARE_SIZE, GameResources::WALL_SQUARE_SIZE)),
    platformEnding(_context.textures->get(GameResources::Textures::PLATFORM_TEXTURE),
                        sf::IntRect(GameResources::WALL_SQUARE_SIZE,0, GameResources::WALL_SQUARE_SIZE, GameResources::WALL_SQUARE_SIZE))

{
    platformMiddle.setPosition(GameResources::WALL_SQUARE_SIZE, 0.f);
    platformEnding.setPosition(platformMiddle.getPosition().x + GameResources::WALL_SQUARE_SIZE,0.f);
}

EntityPlatform::~EntityPlatform()
{
    context.engine->removeObject(this);
}

void EntityPlatform::update(sf::Time deltaTime)
{
    movingAccu += deltaTime;
    if(!movingStart && movingAccu >= movingStartDuration)
    {
        movingAccu = sf::Time::Zero;
        movingStart = true;
    }

    if(movingStart)
    {
        sf::Vector2f movingVelocity;
        if(movingAccu >= movingDuration)
        {
            movingAccu = sf::Time::Zero;
            movingDirection = !movingDirection;
            dir = 1.f - (2.f * (int)(movingDirection));
        }

        movingVelocity.x = speed * dir;

        computeSweptBounds(movingVelocity * deltaTime.asSeconds());
        move(movingVelocity * deltaTime.asSeconds());
        if(attachedCharacter)
        {
            attachedCharacter->move(movingVelocity * deltaTime.asSeconds());
        }
    }
    attachedCharacter = nullptr;
}

GameResources::EntityType EntityPlatform::getEntityType() const
{
    return GameResources::EntityType::PLATFORM;
}

GameResources::RenderingLayers EntityPlatform::getEntityRenderingLayer() const
{
    return GameResources::RenderingLayers::INTERACTION_BOTTOM_LAYER;
}

void EntityPlatform::loadEntity(const nlohmann::json &jsonToLoadFrom)
{
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadFrom);
    movingDuration = sf::seconds(jsonToLoadFrom[GameResources::JSON_ENTITY_PLATFORM_MOVING_DURATION]);
    speed = (double)(jsonToLoadFrom[GameResources::JSON_ENTITY_PLATFORM_MOVING_DISTANCE]) /  movingDuration.asSeconds();
    movingStartDuration = sf::seconds(jsonToLoadFrom[GameResources::JSON_ENTITY_PLATFORM_MOVING_START_DELAY]);

    int middlePlatformWidth = std::max(0,
     (int)jsonToLoadFrom[GameResources::JSON_ENTITY_WALL_SIZE_STRING][0] - 2 * GameResources::WALL_SQUARE_SIZE);

    platformMiddle.setScale(middlePlatformWidth / GameResources::WALL_SQUARE_SIZE, 1.f);
    platformEnding.setPosition(middlePlatformWidth + GameResources::WALL_SQUARE_SIZE, 0.f);

    setupPhysicalPolygon();

}

void EntityPlatform::saveEntity(nlohmann::json &jsonToSaveTo)
{
    nlohmann::json platformJSON;
    platformJSON[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getEntityType();
    LoadAndSaveManager::saveTransformable(*this, platformJSON);
    platformJSON[GameResources::JSON_ENTITY_WALL_SIZE_STRING] = {fLocalBounds.width, fLocalBounds.height};

    platformJSON[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getEntityType();
    platformJSON[GameResources::JSON_ENTITY_PLATFORM_MOVING_DISTANCE] = (unsigned int)(speed * movingDuration.asSeconds());
    platformJSON[GameResources::JSON_ENTITY_PLATFORM_MOVING_DURATION] = movingDuration.asSeconds();
    platformJSON[GameResources::JSON_ENTITY_PLATFORM_MOVING_START_DELAY] = movingStartDuration.asSeconds();

    jsonToSaveTo.push_back(platformJSON);
}

std::unique_ptr<CollisionBox> EntityPlatform::getCollisionBox()
{
    auto hitbox = std::make_unique<PolygonBox>(transformedPoints());
    return std::move(hitbox);
}

void EntityPlatform::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(platformBeginning, states);
    target.draw(platformMiddle, states);
    target.draw(platformEnding, states);
    //target.draw(*(getVisualMesh().get()));
}

void EntityPlatform::setupPhysicalPolygon()
{
    float platformWidth = platformEnding.getPosition().x + GameResources::WALL_SQUARE_SIZE;
    float platformHeightHalf = GameResources::WALL_SQUARE_SIZE * 0.5f;
    /*    
    platformHeight * 0.25,0
    platformWidth - platformHeight * 0.25, 0
    platformWidth, platformHeight * 0.5
    platformWidth - platformHeight * 0.25, platformHeight
    platformHeight * 0.25, platformHeight
    0, platformHeight * 0.5
    */

    setPolygon({
        sf::Vector2f(platformHeightHalf * 0.5f, 0.f),
        sf::Vector2f(platformWidth - platformHeightHalf * 0.5f, 0.f),
        sf::Vector2f(platformWidth, platformHeightHalf),
        sf::Vector2f(platformWidth - platformHeightHalf * 0.5f, platformHeightHalf * 2.f),
        sf::Vector2f(platformHeightHalf * 0.5f, platformHeightHalf * 2.f),
        sf::Vector2f(0.f, platformHeightHalf)
    });

    setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    fDynamicFriction = fStaticFriction = 0.f;

    context.engine->addObject(this);
}
