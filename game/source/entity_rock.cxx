#include "entity_rock.hpp"
#include "polygon_box.hpp"
#include "load_and_save_manager.hpp"

EntityRock::EntityRock(GameContext _context)
    :Entity(_context), visualRock()
{
    visualRock.setFillColor(sf::Color::Blue);
    visualRock.setOutlineThickness(1.f);
    visualRock.setOutlineColor(sf::Color(255,0,255));
}

EntityRock::~EntityRock()
{
    context.engine->removeObject(this);
}

GameResources::EntityType EntityRock::getEntityType() const
{
    return GameResources::EntityType::ROCK;
}

GameResources::RenderingLayers EntityRock::getEntityRenderingLayer() const
{
    return GameResources::RenderingLayers::INTERACTION_BOTTOM_LAYER;
}

std::unique_ptr<class CollisionBox> EntityRock::getCollisionBox()
{
    setScale(1.001f, 1.001f);
    auto hitbox = std::make_unique<PolygonBox>(transformedPoints());
    setScale(1.f, 1.f);
    return std::move(hitbox);
}

void EntityRock::loadEntity(const nlohmann::json &jsonToLoadFrom)
{
    std::vector<sf::Vector2f> points(jsonToLoadFrom[GameResources::JSON_ENTITY_ROCK_POINT_COUNT_STRING]);
    visualRock.setPointCount(points.size());
    for(int p = points.size() - 1;p>=0;--p)
    {
        points[p].x = jsonToLoadFrom[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING][p][0];
        points[p].y = jsonToLoadFrom[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING][p][1];
        visualRock.setPoint(p, points[p]);
    }
    setPolygon(points);
    setupPolygon();

    LoadAndSaveManager::loadTransformable(*this, jsonToLoadFrom);
}

void EntityRock::saveEntity(nlohmann::json &jsonToSaveTo)
{

    nlohmann::json rockJSON;
    
    rockJSON[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getEntityType();
    rockJSON[GameResources::JSON_ENTITY_ROCK_POINT_COUNT_STRING] = getPointsWithoutTranform().size() - 1;
    for(size_t p = 0;p<rockJSON[GameResources::JSON_ENTITY_ROCK_POINT_COUNT_STRING];++p)
        rockJSON[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING].push_back({getPointsWithoutTranform()[p].x, getPointsWithoutTranform()[p].y});
    
    LoadAndSaveManager::saveTransformable(*this, rockJSON);

    jsonToSaveTo.push_back(rockJSON);
}

void EntityRock::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(visualRock, states);
}

void EntityRock::setupPolygon()
{
    setMass(.5f);
    fGravitySensitive = true;
    fInteractionType = PhysicalObjectInteractionType::OnlyStaticAndItsKind;
    context.engine->addObject(this);
    fRestitutionCoeff = 0.5f;
    fInitialRestCoeff = fRestitutionCoeff;
}
