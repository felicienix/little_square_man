#include "level_menu_state.hpp"
#include "gui_pagination_menu.hpp"
#include "gui_menu_manager.hpp"
#include "gui_button.hpp"
#include "state_stack.hpp"
#include "loading_state.hpp"
#include "game_state.hpp"
#include <filesystem>
#include <fstream>

#define levelButtonCallBack(strIndex, isLevelOfficial) [this, strIndex](){ \
            sf::RenderWindow* contextWindow = stateContext.window; \
            StateStack* contextStack = stateContext.stateStack; \
            stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE); \
            stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE); \
            stateContext.stateStack->requestAction(StateStack::Actions::PUSH_STATE, [contextWindow, contextStack, strIndex](){ \
                return new LoadingState<GameState, size_t, bool>(*contextWindow, *contextStack, strIndex, isLevelOfficial); \
            }); \
        }

namespace
{
    std::string replaceUnderscoresBySpacesCopy(const std::string& ansiString)
    {
        std::string copyStr = ansiString;
        for(char& character : copyStr)
        {
            if(character == '_')
                character = ' ';
        }

        return copyStr;
    }

    unsigned int getPlayerLevelProgress()
    {
        std::ifstream playerProgressFile(PLAYER_PROGRESS_DIR + std::string("do_not_open_this_file.txt"));
        if(!playerProgressFile.is_open())
            throw std::runtime_error("couldn't open player's progress file");

        unsigned int playerProgress = 0;
        playerProgressFile >> playerProgress;

        playerProgressFile.close();

        return playerProgress;
    }
}

LevelMenuState::LevelMenuState(GameContext _context)
    :State(_context), levelsMenuManager(new alphGUI::MenuManager(sf::Color(86,86,86,125))), 
    levelsMenuBackground(_context.textures->get(MenuResources::LEVELS_MENU_BACKGROUND)),
    goBackText("go back (Escape)", _context.fonts->get(MenuResources::Fonts::TITLE_FONT))
{
    buildLevelsMenu();
    goBackText.setPosition(10.f, 10.f);
    goBackText.setFillColor(sf::Color::Black);

}

LevelMenuState::~LevelMenuState()
{
}

bool LevelMenuState::handleEvent(const sf::Event &event)
{
    levelsMenuManager->handleEvent(event, *stateContext.window);

    if(event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
    {
        stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE);   
    }
    return false;
}

bool LevelMenuState::update(sf::Time deltaTime)
{
    levelsMenuManager->update(deltaTime);
    return true;
}

bool LevelMenuState::isLevelOfficial(const std::string &levelName)
{
    if(levelName.substr(0, 8) == "official")
        return true;
    return false;
}

size_t LevelMenuState::getLevelNumberFromName(const std::string &levelName)
{
    return std::atoi(levelName.substr(22).c_str());
}

void LevelMenuState::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(levelsMenuBackground, states);
    target.draw(goBackText, states);
    target.draw(*(levelsMenuManager.get()), states);
}

void LevelMenuState::buildLevelsMenu()
{

    alphGUI::PaginationMenu* officialLevelsMenu = new alphGUI::PaginationMenu(WIDTH - alphGUI::Button::BUTTON_WIDTH, 2 * alphGUI::Button::BUTTON_HEIGHT);
    alphGUI::PaginationMenu* nonOfficialLevelsMenu = new alphGUI::PaginationMenu(WIDTH - alphGUI::Button::BUTTON_WIDTH, 2 * alphGUI::Button::BUTTON_HEIGHT);

    
    officialLevelsMenu->addComponents(buildOfficialLevelButtons());
    nonOfficialLevelsMenu->addComponents(buildNonOfficialLevelButtons());

    officialLevelsMenu->setPosition(alphGUI::Button::BUTTON_WIDTH * 0.5f, HEIGHT * 0.5f);    
    nonOfficialLevelsMenu->setPosition(alphGUI::Button::BUTTON_WIDTH * 0.5f, HEIGHT * 0.5f + alphGUI::Button::BUTTON_HEIGHT * 2.1f);    
    
    levelsMenuManager->addMenuToManage(officialLevelsMenu);
    levelsMenuManager->addMenuToManage(nonOfficialLevelsMenu);

    levelsMenuBackground.setPosition(officialLevelsMenu->getPosition() - sf::Vector2f(25.f, 25.f));
}

std::vector<alphGUI::Component *> LevelMenuState::buildOfficialLevelButtons()
{
    std::vector<alphGUI::Component*> officialLevelButtons;
    unsigned int userLevelProgress = getPlayerLevelProgress();
    std::vector<std::string> officialLevelNames = GameState::getOfficialLevelNamesInOrder();


    for(size_t strIndex = 0; strIndex < officialLevelNames.size();++strIndex)
    {
        alphGUI::Button* officialLevelButton = new alphGUI::Button(replaceUnderscoresBySpacesCopy(officialLevelNames[strIndex]));
        officialLevelButton->clickFunctionCallback = levelButtonCallBack(strIndex, true);
        if(userLevelProgress < strIndex)
            officialLevelButton->deactivate();
        officialLevelButtons.push_back(officialLevelButton);
    }

    return officialLevelButtons;
}

std::vector<alphGUI::Component *> LevelMenuState::buildNonOfficialLevelButtons()
{
    std::vector<alphGUI::Component*> nonOfficialLevelButtons;
    std::vector<std::string> nonOfficialLevelNames = GameState::getNonOfficialLevelNamesInOrder();

    for(size_t strIndex = 0; strIndex < nonOfficialLevelNames.size();++strIndex)
    {
        alphGUI::Button* nonOfficialLevelButton = new alphGUI::Button(replaceUnderscoresBySpacesCopy(nonOfficialLevelNames[strIndex]));
        nonOfficialLevelButton->clickFunctionCallback = levelButtonCallBack(strIndex, false);
        nonOfficialLevelButtons.push_back(nonOfficialLevelButton);
    }
    return nonOfficialLevelButtons;
}
