#include "random_title_background_polygon.hpp"
#include "random_functions.hpp"

RandomTitleBackgroundPolygon::RandomTitleBackgroundPolygon(GameContext _context)
    :context(_context), sprite(context.textures->get(MenuResources::SQUARE_TEXTURE))
{
    float spriteScaleFactor = lsmRandom::randomFloat(0.3f, 2.f);
    float width = sprite.getLocalBounds().width * spriteScaleFactor;
    float height = sprite.getLocalBounds().height * spriteScaleFactor;
    setPolygon({sf::Vector2f(0.f, 0.f), sf::Vector2f(width, 0.f), sf::Vector2f(width, height), sf::Vector2f(0.f, height)});
    setMass(5.f);
    fRestitutionCoeff = fInitialRestCoeff = 1.f;
    fStaticFriction = 0.7f;
    fGravitySensitive = false;
    fLinearVelocity = speed * MathOperations::normalize(sf::Vector2f(lsmRandom::randomFloat(-1.f, 1.f), lsmRandom::randomFloat(-1.f, 1.f)));
    sprite.setScale(spriteScaleFactor, spriteScaleFactor);
}

RandomTitleBackgroundPolygon::~RandomTitleBackgroundPolygon()
{
}

void RandomTitleBackgroundPolygon::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(sprite, states);
}
