#include "title_menu_state.hpp"
#include "state_stack.hpp"
#include "gui_menu.hpp"
#include "gui_button.hpp"
#include "title_menu_background.hpp"
#include "level_menu_state.hpp"
#include "settings_state.hpp"
#include "game_title.hpp"

TitleMenuState::TitleMenuState(sf::RenderWindow &contextWindow, StateStack& stateStack)
    :State(contextWindow, stateStack), textures(), fonts(), sounds(), gameTitle(), titleGUIMenu(), movingBackground()
{
    stateContext.textures = &textures;
    stateContext.fonts = &fonts;
    stateContext.sounds = &sounds;
}

TitleMenuState::~TitleMenuState()
{
}

bool TitleMenuState::handleEvent(const sf::Event &event)
{
    titleGUIMenu->handleEvent(event, *stateContext.window);
    movingBackground->handleEvent(event);
    return false;
}

bool TitleMenuState::update(sf::Time deltaTime)
{
    titleGUIMenu->update(deltaTime);
    movingBackground->update(deltaTime);
    gameTitle->update(deltaTime);
    return false;
}

void TitleMenuState::buildState(volatile float &progress, sf::Mutex& loadingThreadMutex)
{
    unsigned int totalResources = MenuResources::TEXTURES_COUNT + MenuResources::FONTS_COUNT + MenuResources::SOUNDS_COUNT;
    float progressIncrement = 1.f / totalResources;
    loadingThreadMutex.lock();
    progress = 0.f;
    loadingThreadMutex.unlock();

    //load the textures
    
    textures.load(TEXTURE_DIR + std::string("game_title.png"), MenuResources::GAME_TITLE);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    textures.load(TEXTURE_DIR + std::string("main_character.png"), MenuResources::TITLE_CHARACTER);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    textures.load(TEXTURE_DIR + std::string("square.png"), MenuResources::SQUARE_TEXTURE);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    textures.load(TEXTURE_DIR + std::string("levels_menu_background.png"), MenuResources::LEVELS_MENU_BACKGROUND);
    loadingThreadMutex.lock();
    progress += progressIncrement;
    loadingThreadMutex.unlock();

    //load the fonts
    fonts.load(FONT_DIR + std::string("title_font.ttf"), MenuResources::TITLE_FONT);
    //load the sounds

    //setup the background and the menu gui
    gameTitle.reset(new GameTitle(stateContext));
    //center the sprite and put it a little bit at the top
    gameTitle->setPosition(WIDTH * 0.5f, HEIGHT * 0.25f);
    //create and build the gui menu
    titleGUIMenu.reset(new alphGUI::Menu());
    buildGUIMenu();
    movingBackground.reset(new TitleMenuBackground(stateContext));

    loadingThreadMutex.lock();
    progress = 1.f;
    loadingThreadMutex.unlock();
}

void TitleMenuState::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    //target.draw(menuBackground, states);
    target.draw(*(movingBackground.get()), states);
    target.draw(*(gameTitle.get()), states);
    if(isAtForeGround)
    {
        target.draw(*(titleGUIMenu.get()), states);
    }
}

void TitleMenuState::buildGUIMenu()
{
    alphGUI::Button* playButton = new alphGUI::Button("play");
    playButton->clickFunctionCallback = [this](){
        stateContext.stateStack->requestAction(StateStack::Actions::PUSH_STATE, [this]()
        {
            return new LevelMenuState(stateContext);
        });
    };
    alphGUI::Button* settingButton = new alphGUI::Button("settings");
    settingButton->clickFunctionCallback = [this](){
        stateContext.stateStack->requestAction(StateStack::Actions::PUSH_STATE, [this]()
        {
            return new SettingsState(stateContext);
        });
    };

    alphGUI::Button* quitButton = new alphGUI::Button("quit");
    quitButton->clickFunctionCallback = [this](){stateContext.window->close();};

    titleGUIMenu->addComponent(playButton);
    titleGUIMenu->addComponent(settingButton);
    titleGUIMenu->addComponent(quitButton);

    sf::FloatRect menuBounds = titleGUIMenu->getBoundingRectangle();
    titleGUIMenu->setPosition(WIDTH * 0.5f - menuBounds.width * 0.5f, HEIGHT * 0.5f - menuBounds.height * 0.5f);
}
