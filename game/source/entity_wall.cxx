#include "entity_wall.hpp"
#include "aabb_box.hpp"
#include "load_and_save_manager.hpp"

EntityWall::EntityWall(GameContext _context)
    :Entity(_context), wallSprite()
{
    sf::Texture& wallTexture = context.textures->get(GameResources::SQUARE_TEXTURE);
    wallTexture.setRepeated(true);
    wallSprite.setTexture(wallTexture);
    wallSprite.setTextureRect(sf::IntRect(sf::Vector2i(), sf::Vector2i(wallTexture.getSize())));
}

EntityWall::~EntityWall()
{
    context.engine->removeObject(this);
}

void EntityWall::update(sf::Time deltaTime)
{
}

void EntityWall::handleEvent(const sf::Event &event)
{
}

GameResources::EntityType EntityWall::getEntityType() const
{
    return GameResources::EntityType::WALL;
}

GameResources::RenderingLayers EntityWall::getEntityRenderingLayer() const
{
    return GameResources::RenderingLayers::BACKGROUND_LAYER;
}

void EntityWall::loadEntity(const nlohmann::json &jsonToLoadFrom)
{
    wallSprite.setTextureRect(sf::IntRect(0,0,
        jsonToLoadFrom[GameResources::JSON_ENTITY_WALL_SIZE_STRING][0],
        jsonToLoadFrom[GameResources::JSON_ENTITY_WALL_SIZE_STRING][1]));
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadFrom);
    setupPhysicalAABB();
}

void EntityWall::saveEntity(nlohmann::json &jsonToSaveTo)
{
    nlohmann::json wallJSON;
    wallJSON[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getEntityType();
    LoadAndSaveManager::saveTransformable(*this, wallJSON);
    wallJSON[GameResources::JSON_ENTITY_WALL_SIZE_STRING] = {fLocalBounds.width, fLocalBounds.height};

    jsonToSaveTo.push_back(wallJSON);
}

std::unique_ptr<CollisionBox> EntityWall::getCollisionBox()
{
    auto ptr = std::make_unique<AABBBox>(sf::FloatRect(getPosition() + sf::Vector2f(fLocalBounds.left, fLocalBounds.top),
                    sf::Vector2f(fLocalBounds.width, fLocalBounds.height)));
    return std::move(ptr);
}

void EntityWall::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(wallSprite, states);
    //for debug purposes only
    //target.draw(*(getVisualMesh().get()));
}

void EntityWall::setupPhysicalAABB()
{
    setWidthHeight(wallSprite.getTextureRect().width, wallSprite.getTextureRect().height);
    setPhysicalObjectMovementType(PhysicalObjectMovementType::Static);
    fDynamicFriction = fStaticFriction = 0.f;

    context.engine->addObject(this);
}
