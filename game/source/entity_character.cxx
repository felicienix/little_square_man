#include "entity_character.hpp"
#include "polygon_box.hpp"
#include "animation.hpp"
#include "entity_wall_square_with_gravity.hpp"
#include "random_functions.hpp"
#include "game_level.hpp"
#include "entity_splash_particle_emitter.hpp"
#include "load_and_save_manager.hpp"
#include "entity_rock.hpp"
#include "entity_throw_rock_action.hpp"
#include <fstream>
#include <math.h>

EntityCharacter::EntityCharacter(GameContext _context)
    :Entity(_context), littleSquareMan(context.textures->get(GameResources::Textures::MAIN_CHARACTER), 
    sf::IntRect(0,0, GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT)),
    littleSquareManAnimations(),
    blinkingDuration(sf::seconds(0.15f)),
    stopBlinkingRandomDuration(sf::seconds(lsmRandom::randomFloat(1.f, 4.f))),
    speed(GameResources::CHARACTER_WALKING_SPEED),
    jumpingSpeed(GameResources::CHARACTER_JUMPING_SPEED),
    direction(0.f,0.f),
    walkingParticleEmitter(std::make_unique<EntityParticleEmitter>(context, GameResources::ParticleTypes::WALKING_PARTICLE)),
    jumpingBackDownParticleEmitter(std::make_unique<EntitySplashParticleEmitter>(context, 
                    GameResources::ParticleTypes::JUMPING_BACK_DOWN_SPLASH, 
                    sf::seconds(0.1f),
                    40.f)),  //TODO: make those hardcoded value, not hardcoded
    textHelperToSelectRock("Press E to select", _context.fonts->get(GameResources::Fonts::TITLE_FONT)),
    throwRockAction(new EntityThrowRockAction(_context, this))
{
    textHelperToSelectRock.setFillColor(sf::Color::Black);
    textHelperToSelectRock.setCharacterSize(20U);
    textHelperToSelectRock.setOrigin(textHelperToSelectRock.getGlobalBounds().width * 0.5f, textHelperToSelectRock.getGlobalBounds().height * 0.5f);
    jumpingBackDownParticleEmitter->ParticlesPerSplashNumber = 4U;
    setupPolygon();
    setupAnimations();
}

EntityCharacter::~EntityCharacter()
{
    context.engine->removeObject(this);
    for(Animation* anim : littleSquareManAnimations)
    {
        if(anim)
            delete anim;
    }
}

void EntityCharacter::update(sf::Time deltaTime)
{
    handleMovingAndJumpingAnimationChange();
    littleSquareManAnimations[currentlyActiveAnimation]->update(deltaTime, littleSquareMan);
    handleBlinkingAnim(deltaTime);

    handleMovement(deltaTime);

    handleParticleEmitters(deltaTime);

    isPreviouslyInTheAir = isInTheAir();
    
    if(hasRockInHand && selectedRock)
    {
        selectedRock->setPosition(getPosition().x, getPosition().y 
                                                    - getBoundingRectangle().height * 0.5f
                                                    - selectedRock->getBoundingRectangle().height * 0.5f);
        throwRockAction->update(deltaTime);
    }

    //for debug purposes only
    //hitbox = static_cast<PolygonBox&>(*(getCollisionBox().get()));
    isInRangeOfRock = false;
}

void EntityCharacter::handleEvent(const sf::Event &event)
{
    switch (event.type)
    {
    case sf::Event::KeyPressed:
        handleKeyPressedEvents(event);
        break;
    case sf::Event::KeyReleased:
        handleKeyReleasedEvents(event);
        break;
    default:
        break;
    }

}

void EntityCharacter::handleKeyPressedEvents(const sf::Event &event)
{
    if(event.key.code == characterControls[GameResources::CharacterControls::SPRINT])
        makeCharacterRun();
    else if(event.key.code == characterControls[GameResources::CharacterControls::PICK_UP_ROCK] && selectedRock)
    {
        if(!hasRockInHand)
        {
            throwRockAction->startAction();
            context.engine->removeObject(selectedRock);
            hasRockInHand = true;
        }
        else
        {
            throwRockAction->nextStep();
            if(throwRockAction->isActionFinished())
            {
                context.engine->addObject(selectedRock);
                selectedRock->fLinearVelocity = throwRockAction->getFinalVelocity();
                selectedRock = nullptr;
                hasRockInHand = false;
            }
        }

    }
    else if(event.key.code == characterControls[GameResources::CharacterControls::STAND_ON_ROCK])
    {
        static bool goesThrough = true;
        goesThrough = !goesThrough;
        InteractionCommand rockCommand;
        rockCommand.entityToSendCommandTo = GameResources::EntityType::ROCK;
        rockCommand.action = [](Entity* rock)
        {
            PhysicalObject* rockPhysical = static_cast<PhysicalObject*>(static_cast<EntityRock*>(rock));
            if(goesThrough)
                rockPhysical->fInteractionType = PhysicalObjectInteractionType::OnlyStaticAndItsKind;
            else
                rockPhysical->fInteractionType = PhysicalObjectInteractionType::WithEverything;
        };
        levelContext->pushInteractionCommand(rockCommand);

        InteractionCommand wallWithGravityCommand = rockCommand;
        wallWithGravityCommand.entityToSendCommandTo = GameResources::EntityType::WALL_SQUARE_WITH_GRAVITY;
        levelContext->pushInteractionCommand(wallWithGravityCommand);
    }
}

void EntityCharacter::handleKeyReleasedEvents(const sf::Event &event)
{
    if(event.key.code == characterControls[GameResources::CharacterControls::SPRINT])
        stopCharacterFromRunning();
}

void EntityCharacter::handleMovingAndJumpingAnimationChange()
{
    CharacterAnimations previouslyActiveAnimation = currentlyActiveAnimation;

    switch ((int)direction.x)
    {
    case -1:    //user is going towards the left
        if(isInTheAir())
            currentlyActiveAnimation = JUMPING_LEFT;
        else
        {
            currentlyActiveAnimation = WALKING_LEFT;
            if(isRunning)
                littleSquareManAnimations[currentlyActiveAnimation]->setDuration(sf::seconds(GameResources::CHARACTER_RUNNING_DURATION));
            else
                littleSquareManAnimations[currentlyActiveAnimation]->setDuration(sf::seconds(GameResources::CHARACTER_WALKING_DURATION));
        }
        break;
    case 0: //user is standing still
        if(!isInTheAir())
        {
            //if the previously active animation was towards the left
            if(previouslyActiveAnimation <= LEFT_ANIMATIONS_COUNT)
                currentlyActiveAnimation = IDLE_LEFT;
            else
                currentlyActiveAnimation = IDLE_RIGHT;
        }
        else
        {
            //if the previously active animation was towards the left
            if(previouslyActiveAnimation <= LEFT_ANIMATIONS_COUNT)
                currentlyActiveAnimation = JUMPING_LEFT;
            else
                currentlyActiveAnimation = JUMPING_RIGHT;
        }
        break;
    case 1: //user is going towards the right
        if(isInTheAir())
            currentlyActiveAnimation = JUMPING_RIGHT;
        else
        {
            currentlyActiveAnimation = WALKING_RIGHT;
            if(isRunning)
                littleSquareManAnimations[currentlyActiveAnimation]->setDuration(sf::seconds(GameResources::CHARACTER_RUNNING_DURATION));
            else
                littleSquareManAnimations[currentlyActiveAnimation]->setDuration(sf::seconds(GameResources::CHARACTER_WALKING_DURATION));
        }
        break;
    default:
        break;
    }

    if(previouslyActiveAnimation != currentlyActiveAnimation)
    {
        if(isBlinking)
            stopBlinking(previouslyActiveAnimation);
        littleSquareManAnimations[currentlyActiveAnimation]->Restart();
    }
}

void EntityCharacter::handleMovement(sf::Time deltaTime)
{
    if(sf::Keyboard::isKeyPressed(characterControls[GameResources::CharacterControls::JUMP]) && !isTheCharacterFlying)
    {
        fLinearVelocity.y = -jumpingSpeed;
        isTheCharacterFlying = true;
    }
    direction.x = 0.f;
    if(sf::Keyboard::isKeyPressed(characterControls[GameResources::CharacterControls::MOVE_LEFT]))
        direction.x = -1.f;
    if(sf::Keyboard::isKeyPressed(characterControls[GameResources::CharacterControls::MOVE_RIGHT]))
        direction.x = 1.f;

    move(speed * direction * deltaTime.asSeconds());
}

void EntityCharacter::handleParticleEmitters(sf::Time deltaTime)
{
    if(!isInTheAir() && !MathOperations::equalFloat(direction.x, 0.f))
        walkingParticleEmitter->startEmitting();
    else
        walkingParticleEmitter->stopEmitting();

    walkingParticleEmitter->setPosition(getPosition().x - fLocalBounds.width * 0.25f * direction.x, getPosition().y + fLocalBounds.height * 0.5f);
    walkingParticleEmitter->update(deltaTime);

    if(isInTheAir() && maxAirYVelocity <= fLinearVelocity.y)
        maxAirYVelocity = fLinearVelocity.y;
    if(isPreviouslyInTheAir && !isInTheAir() && maxAirYVelocity > 400.f)
    {
        maxAirYVelocity = 0.f;
        jumpingBackDownParticleEmitter->startEmitting();
        jumpingBackDownParticleEmitter->setPosition(getPosition().x, getPosition().y + fLocalBounds.height * 0.5f);
    }

    jumpingBackDownParticleEmitter->update(deltaTime);
}

void EntityCharacter::handleBlinkingAnim(sf::Time deltaTime)
{
    if(!isBlinking)
    {
        stopBlinkingAccu += deltaTime;
        if(stopBlinkingAccu >= stopBlinkingRandomDuration)
        {
            startBlinking(currentlyActiveAnimation);
        }
    }
    else
    {
        blinkingTimeAccu += deltaTime;
        if(blinkingTimeAccu >= blinkingDuration)
        {
            stopBlinking(currentlyActiveAnimation);
        }
    }
}


void EntityCharacter::startBlinking(CharacterAnimations animation)
{
    stopBlinkingAccu = sf::Time::Zero;
    isBlinking = true;
    stopBlinkingRandomDuration = sf::seconds(lsmRandom::randomFloat(1.f, 4.f));
    sf::IntRect spriteRect = littleSquareMan.getTextureRect();
    sf::Vector2i currentStartPoint = littleSquareManAnimations[animation]->getStartPoint();
    littleSquareMan.setTextureRect(sf::IntRect(spriteRect.left, spriteRect.top + spriteRect.height, spriteRect.width, spriteRect.height));
    littleSquareManAnimations[animation]->setStartPoint(sf::Vector2i(currentStartPoint.x, currentStartPoint.y + spriteRect.height));
}

void EntityCharacter::stopBlinking(CharacterAnimations animation)
{
    blinkingTimeAccu = sf::Time::Zero;
    isBlinking = false;
    sf::IntRect spriteRect = littleSquareMan.getTextureRect();
    sf::Vector2i currentStartPoint = littleSquareManAnimations[animation]->getStartPoint();
    littleSquareMan.setTextureRect(sf::IntRect(spriteRect.left, spriteRect.top - spriteRect.height, spriteRect.width, spriteRect.height));
    littleSquareManAnimations[animation]->setStartPoint(sf::Vector2i(currentStartPoint.x, currentStartPoint.y - spriteRect.height));
}

bool EntityCharacter::isInTheAir() const
{
    return isTheCharacterFlying;
}

void EntityCharacter::makeCharacterRun()
{
    isRunning = true;
    speed = 2.f * GameResources::CHARACTER_WALKING_SPEED;
    jumpingSpeed = 1.5f * GameResources::CHARACTER_JUMPING_SPEED;
}

void EntityCharacter::stopCharacterFromRunning()
{
    isRunning = false;
    speed = GameResources::CHARACTER_WALKING_SPEED;
    jumpingSpeed = GameResources::CHARACTER_JUMPING_SPEED;
}

GameResources::EntityType EntityCharacter::getEntityType() const
{
    return GameResources::CHARACTER;
}

GameResources::RenderingLayers EntityCharacter::getEntityRenderingLayer() const
{
    return GameResources::INTERACTION_TOP_LAYER;
}

void EntityCharacter::loadEntity(const nlohmann::json &jsonToLoadFrom)
{
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadFrom);

    walkingParticleEmitter->setPosition(getPosition().x - fLocalBounds.width * 0.25f * direction.x, getPosition().y + fLocalBounds.height * 0.5f);
    InteractionRule selectRock;
    selectRock.firstEntity = GameResources::EntityType::CHARACTER;
    selectRock.secondEntity = GameResources::EntityType::ROCK;
    selectRock.action = [this](Entity*, Entity* rock, OverlapData collisionData)
    {
        if(!hasRockInHand)
        {
            selectedRock = static_cast<EntityRock*>(rock);
            isInRangeOfRock = true;
        }
    };
    levelContext->pushInteractionRule(selectRock);
    InteractionRule selectWallWithGravity = selectRock;
    selectWallWithGravity.secondEntity = GameResources::EntityType::WALL_SQUARE_WITH_GRAVITY;
    levelContext->pushInteractionRule(selectWallWithGravity);

    InteractionRule canJump;
    canJump.firstEntity = GameResources::EntityType::CHARACTER;
    canJump.secondEntity = GameResources::EntityType::ENTITIES_JUMPABLE_ON;
    canJump.action = [this](Entity* me, Entity* other, OverlapData collisionData)
    {
        //if we remove the y_diff check, it can make the character attach to the ceiling, pretty cool !
        float y_diff = Entity::castEntityToTransformable(me)->getPosition().y - Entity::castEntityToTransformable(other)->getPosition().y;
        float cosAngle =  std::acos(MathOperations::dotProd(collisionData.fCollisionNormal, sf::Vector2f(0.f, 1.f)));
        if(y_diff < 0.f && !(cosAngle < 3.f * M_PI_4 && cosAngle > M_PI_4))
        {
            isTheCharacterFlying = false;
        }
    };
    InteractionRule inTheAir;
    inTheAir.firstEntity = GameResources::EntityType::CHARACTER;
    inTheAir.secondEntity = GameResources::EntityType::ENTITIES_JUMPABLE_ON;
    inTheAir.action = [this](Entity*, Entity*, OverlapData)
    {
        isTheCharacterFlying = true;
    };
    
    levelContext->pushInteractionRule(canJump);
    levelContext->pushNoLongerInteractingRule(inTheAir);
}

void EntityCharacter::saveEntity(nlohmann::json &jsonToSaveTo)
{
    nlohmann::json characterJSON;
    characterJSON[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getEntityType();
    LoadAndSaveManager::saveTransformable(*this, characterJSON);

    jsonToSaveTo.push_back(characterJSON);
}

std::unique_ptr<CollisionBox> EntityCharacter::getCollisionBox()
{
    setScale(1.010f, 1.010f);
    auto hitbox = std::make_unique<PolygonBox>(transformedPoints());
    setScale(1.f, 1.f);
    return std::move(hitbox);
}


void EntityCharacter::setupPolygon()
{
    setPolygon(GameResources::CHARACTER_HIT_BOX_POLYGON);
    setMass(GameResources::CHARACTER_MASS);
    fGravitySensitive = true;
    fStaticFriction = fDynamicFriction = 0.f;

    setPhysicalObjectMovementType(PhysicalObjectMovementType::Unmovable);
    
    context.engine->addObject(this);
}

void EntityCharacter::setupAnimations()
{
    for(auto& anim : littleSquareManAnimations)
        anim = nullptr;

    //IDLE LEFT animation
    littleSquareManAnimations[IDLE_LEFT] = new Animation(context.textures->get(GameResources::Textures::MAIN_CHARACTER), sf::Vector2i(0,0), 1);
    littleSquareManAnimations[IDLE_LEFT]->setDuration(sf::seconds(GameResources::CHARACTER_IDLE_DURATION));
    littleSquareManAnimations[IDLE_LEFT]->setFrameSize(sf::Vector2i(GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT));
    littleSquareManAnimations[IDLE_LEFT]->setNumFrames(GameResources::CHARACTER_IDLE_NUM_FRAMES);
    littleSquareManAnimations[IDLE_LEFT]->Repeat(true);
    littleSquareManAnimations[IDLE_LEFT]->Restart();

    //IDLE RIGHT animation
    littleSquareManAnimations[IDLE_RIGHT] = new Animation(context.textures->get(GameResources::Textures::MAIN_CHARACTER), 
                                                    sf::Vector2i(0,GameResources::CHARACTER_IDLE_RIGHT_Y_START_COORD), 1);
    littleSquareManAnimations[IDLE_RIGHT]->setDuration(sf::seconds(GameResources::CHARACTER_IDLE_DURATION));
    littleSquareManAnimations[IDLE_RIGHT]->setFrameSize(sf::Vector2i(GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT));
    littleSquareManAnimations[IDLE_RIGHT]->setNumFrames(GameResources::CHARACTER_IDLE_NUM_FRAMES);
    littleSquareManAnimations[IDLE_RIGHT]->Repeat(true);
    littleSquareManAnimations[IDLE_RIGHT]->Restart();

    //WALKING LEFT animation
    littleSquareManAnimations[WALKING_LEFT] = new Animation(context.textures->get(GameResources::MAIN_CHARACTER),
                                                    sf::Vector2i(0, GameResources::CHARACTER_WALKING_LEFT_Y_START_COORD), 1);
    littleSquareManAnimations[WALKING_LEFT]->setDuration(sf::seconds(GameResources::CHARACTER_WALKING_DURATION));
    littleSquareManAnimations[WALKING_LEFT]->setFrameSize(sf::Vector2i(GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT));
    littleSquareManAnimations[WALKING_LEFT]->setNumFrames(GameResources::CHARACTER_WALKING_NUM_FRAMES);
    littleSquareManAnimations[WALKING_LEFT]->Repeat(true);
    littleSquareManAnimations[WALKING_LEFT]->Restart();

    //WALKING RIGHT animation
    littleSquareManAnimations[WALKING_RIGHT] = new Animation(context.textures->get(GameResources::MAIN_CHARACTER),
                                                    sf::Vector2i(0, GameResources::CHARACTER_WALKING_RIGHT_Y_START_COORD), 1);
    littleSquareManAnimations[WALKING_RIGHT]->setDuration(sf::seconds(GameResources::CHARACTER_WALKING_DURATION));
    littleSquareManAnimations[WALKING_RIGHT]->setFrameSize(sf::Vector2i(GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT));
    littleSquareManAnimations[WALKING_RIGHT]->setNumFrames(GameResources::CHARACTER_WALKING_NUM_FRAMES);
    littleSquareManAnimations[WALKING_RIGHT]->Repeat(true);
    littleSquareManAnimations[WALKING_RIGHT]->Restart();

    //JUMPING LEFT animation
    littleSquareManAnimations[JUMPING_LEFT] = new Animation(context.textures->get(GameResources::MAIN_CHARACTER),
                                                    sf::Vector2i(0, GameResources::CHARACTER_JUMPING_LEFT_Y_START_COORD), 1);
    littleSquareManAnimations[JUMPING_LEFT]->setDuration(sf::seconds(GameResources::CHARACTER_JUMPING_DURATION));
    littleSquareManAnimations[JUMPING_LEFT]->setFrameSize(sf::Vector2i(GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT));
    littleSquareManAnimations[JUMPING_LEFT]->setNumFrames(GameResources::CHARACTER_JUMPING_NUM_FRAMES);
    littleSquareManAnimations[JUMPING_LEFT]->Repeat(true);
    littleSquareManAnimations[JUMPING_LEFT]->Restart();

    //JUMPING RIGHT animation
    littleSquareManAnimations[JUMPING_RIGHT] = new Animation(context.textures->get(GameResources::MAIN_CHARACTER),
                                                    sf::Vector2i(0, GameResources::CHARACTER_JUMPING_RIGHT_Y_START_COORD), 1);
    littleSquareManAnimations[JUMPING_RIGHT]->setDuration(sf::seconds(GameResources::CHARACTER_JUMPING_DURATION));
    littleSquareManAnimations[JUMPING_RIGHT]->setFrameSize(sf::Vector2i(GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT));
    littleSquareManAnimations[JUMPING_RIGHT]->setNumFrames(GameResources::CHARACTER_JUMPING_NUM_FRAMES);
    littleSquareManAnimations[JUMPING_RIGHT]->Repeat(true);
    littleSquareManAnimations[JUMPING_RIGHT]->Restart();
}

std::array<sf::Keyboard::Key, GameResources::CharacterControls::CONTROLS_COUNT> EntityCharacter::characterControls = 
{
    sf::Keyboard::D, sf::Keyboard::Q, sf::Keyboard::Z, sf::Keyboard::LShift, sf::Keyboard::E, sf::Keyboard::P
};

void EntityCharacter::updateCharacterControlFileContent()
{
    std::ofstream characterControlFile(PLAYER_PROGRESS_DIR + std::string("character_controls.txt"));
    if(!characterControlFile.is_open())
        throw std::runtime_error("couldn't open file character_controls.txt in updateCharacterControlFileContent");
    
    for(size_t controlIndex = 0;controlIndex < characterControls.size() - 1;++controlIndex)
    {
        characterControlFile << characterControls[controlIndex] << "\n";
    }
    characterControlFile << characterControls[characterControls.size() - 1];

    characterControlFile.close();
}

void EntityCharacter::updateCharacterControlsArray()
{
    std::ifstream characterControlFile(PLAYER_PROGRESS_DIR + std::string("character_controls.txt"));
    if(!characterControlFile.is_open())
        throw std::runtime_error("couldn't open file character_controls.txt in updateCharacterControlsArray");
    
    unsigned int uintKeyControl = 0;
    for(size_t controlIndex = 0;controlIndex < characterControls.size();++controlIndex)
    {
        characterControlFile >> uintKeyControl;
        characterControls[controlIndex] = (sf::Keyboard::Key)uintKeyControl;
    }

    characterControlFile.close();
}

int EntityCharacter::whichWayIsCharacterFacing() const
{    
    return (currentlyActiveAnimation <= LEFT_ANIMATIONS_COUNT) ? -1 : 1;
}

void EntityCharacter::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(littleSquareMan, states);

    target.draw(*(walkingParticleEmitter.get()));
    target.draw(*(jumpingBackDownParticleEmitter.get()));
    target.draw(*(throwRockAction.get()), states);

    if(selectedRock && isInRangeOfRock)
    {
        float aboveSelectedRock = selectedRock->getPosition().y - selectedRock->getBoundingRectangle().height * 0.5f
                                    - textHelperToSelectRock.getGlobalBounds().height - 20.f;
        textHelperToSelectRock.setPosition(selectedRock->getPosition().x, aboveSelectedRock);
        target.draw(textHelperToSelectRock);
    }
    //for debug purposes only
    //target.draw(*(getVisualMesh().get()));
    //target.draw(hitbox);
}