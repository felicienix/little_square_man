#include "aabb_box.hpp"
#include "polygon.hpp"
#include "physical_circle.hpp"
#include "physical_aabb.hpp"
#include "polygon_box.hpp"
#include "circle_box.hpp"

AABBBox::AABBBox()
    :CollisionBox(), box(0,0,0,0)
{
}

AABBBox::AABBBox(const sf::Vector2f &topleft, const sf::Vector2f &widthheight)
    :CollisionBox(), box(topleft, widthheight)
{
}

AABBBox::AABBBox(const sf::FloatRect &_box)
    :CollisionBox(), box(_box)
{
}

AABBBox::~AABBBox()
{
}

bool AABBBox::doesCollideWith(const CollisionBox &otherBox, OverlapData &collisionData)
{
    switch (otherBox.getCollisionBoxType())
    {
    case Type::POLYGON:
        {
            const PolygonBox& otherPoly = static_cast<const PolygonBox&>(otherBox);
            return Polygon::doPolygonsCollide({sf::Vector2f(box.left, box.top), sf::Vector2f(box.left + box.width, box.top)
                                            , sf::Vector2f(box.left + box.width, box.top + box.height), sf::Vector2f(box.left, box.top + box.height)
                                            , sf::Vector2f(box.left, box.top)}, otherPoly.polygonPoints, collisionData);
        }
        break;
    case Type::AABB:
        {
            const AABBBox& otherAABB = static_cast<const AABBBox&>(otherBox);
            return PhysicalAABB::doAABBAndAABBCollide(box, otherAABB.box, collisionData);
        }
        break;
    case Type::CIRCLE:
        {
            const CircleBox& otherCircle = static_cast<const CircleBox&>(otherBox);
            return PhysicalAABB::doCircleAndAABBCollide(otherCircle.center, otherCircle.radius, box, collisionData);
        }
        break;
    
    default:
        break;
    }

    return false;
}

CollisionBox::Type AABBBox::getCollisionBoxType() const
{
    return Type::AABB;
}

void AABBBox::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    sf::RectangleShape visualRectangle(sf::Vector2f(box.width, box.height));
    visualRectangle.setFillColor(sf::Color::Transparent);
    visualRectangle.setOutlineColor(sf::Color::Green);
    visualRectangle.setOutlineThickness(1.f);
    visualRectangle.setPosition(box.left, box.top);

    target.draw(visualRectangle, states);
}
