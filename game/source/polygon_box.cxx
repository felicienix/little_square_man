#include "polygon_box.hpp"
#include "polygon.hpp"
#include "physical_circle.hpp"
#include "physical_aabb.hpp"
#include "aabb_box.hpp"
#include "circle_box.hpp"

PolygonBox::PolygonBox()
    :CollisionBox(), polygonPoints()
{
}

PolygonBox::PolygonBox(const std::vector<sf::Vector2f> &points)
    :CollisionBox(), polygonPoints(points)
{
}

PolygonBox::~PolygonBox()
{
}

bool PolygonBox::doesCollideWith(const CollisionBox &otherBox, OverlapData &collisionData)
{
    switch (otherBox.getCollisionBoxType())
    {
    case Type::POLYGON:
        {
            const PolygonBox& otherPoly = static_cast<const PolygonBox&>(otherBox);
            return Polygon::doPolygonsCollide(polygonPoints, otherPoly.polygonPoints, collisionData);
        }
        break;
    case Type::AABB:
        {
            const AABBBox& otherAABB = static_cast<const AABBBox&>(otherBox);
            return Polygon::doPolygonsCollide({sf::Vector2f(otherAABB.box.left, otherAABB.box.top), sf::Vector2f(otherAABB.box.left + otherAABB.box.width, otherAABB.box.top)
                                            , sf::Vector2f(otherAABB.box.left + otherAABB.box.width, otherAABB.box.top + otherAABB.box.height), sf::Vector2f(otherAABB.box.left, otherAABB.box.top + otherAABB.box.height)
                                            , sf::Vector2f(otherAABB.box.left, otherAABB.box.top)}, polygonPoints, collisionData);
        }
        break;
    case Type::CIRCLE:
        {
            const CircleBox& otherCircle = static_cast<const CircleBox&>(otherBox);
            return PhysicalCircle::doCircleCollideWithPoly(polygonPoints, otherCircle.center, otherCircle.radius, collisionData);
        }
        break;
    
    default:
        break;
    }

    return false;
}

CollisionBox::Type PolygonBox::getCollisionBoxType() const
{
    return Type::POLYGON;
}

void PolygonBox::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    sf::VertexArray visualPoly(sf::PrimitiveType::LineStrip, polygonPoints.size());
    for(unsigned int i = 0;i<polygonPoints.size();++i)
        visualPoly[i] = sf::Vertex(polygonPoints[i], sf::Color::Green);
    
    target.draw(visualPoly, states);
}
