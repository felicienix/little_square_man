#include "particle_emitter_specifier.hpp"
#include "particle_emitter_specifier_types.hpp"

ParticleEmitterSpecifier::ParticleEmitterSpecifier()
    :particleTextureID(GameResources::Textures::DEFAULT_PARTICLE_TEXTURE),
    isGravitySensitive(false),
    numberOfParticlesPerSecond(1),
    particleLifeTime(sf::Time::Zero)    
{
}

ParticleEmitterSpecifier::~ParticleEmitterSpecifier()
{
}

ParticleEmitterSpecifier *ParticleEmitterSpecifier::createParticleEmitterSpecifier(GameResources::ParticleTypes type)
{
    switch (type)
    {
    case GameResources::ParticleTypes::WALKING_PARTICLE:
        return new WalkingParticleSpecifier();
        break;
    case GameResources::ParticleTypes::FIREWORK:
        return new FireworksParticleSpecifier();
        break;
    case GameResources::ParticleTypes::JUMPING_BACK_DOWN_SPLASH:
        return new JumpingBackDownParticleSpecifier();
        break;
    
    default:
        assert(false);
        break;
    }

    return nullptr;
}