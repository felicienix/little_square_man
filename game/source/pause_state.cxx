#include "pause_state.hpp"
#include "gui_menu.hpp"
#include "gui_button.hpp"
#include "state_stack.hpp"
#include "loading_state.hpp"
#include "title_menu_state.hpp"
#include "game_state.hpp"
#include "game_level.hpp"
#include "level_menu_state.hpp"

PauseState::PauseState(GameContext _context)
    :State(_context), pauseMenu(std::make_unique<alphGUI::Menu>())
{
    buildPauseMenu();
}

PauseState::~PauseState()
{
}

bool PauseState::handleEvent(const sf::Event &event)
{
    pauseMenu->handleEvent(event, *stateContext.window);
    return false;
}

bool PauseState::update(sf::Time deltaTime)
{
    pauseMenu->update(deltaTime);
    return false;
}

void PauseState::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    sf::RectangleShape dimBackground(sf::Vector2f(WIDTH, HEIGHT));
    dimBackground.setFillColor(sf::Color(0,0,0,110));
    
    target.setView(target.getDefaultView());

    target.draw(dimBackground, states);
    target.draw(*(pauseMenu.get()), states);
}

void PauseState::buildPauseMenu()
{
    alphGUI::Button* resumeButton = new alphGUI::Button("Resume");
    resumeButton->clickFunctionCallback = [this]()
    {
        stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE);
    };
    pauseMenu->addComponent(resumeButton);

    alphGUI::Button* restartButton = new alphGUI::Button("Restart");
    restartButton->clickFunctionCallback = [this]()
    {   
        std::string levelLoadedName = stateContext.levelLoaded->getLevelName();
        size_t levelLoadedID =  LevelMenuState::getLevelNumberFromName(levelLoadedName);
        sf::RenderWindow* contextWindow = stateContext.window;
        StateStack* contextStack = stateContext.stateStack;
        stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE);
        stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE);
        stateContext.stateStack->requestAction(StateStack::Actions::PUSH_STATE, [contextWindow, contextStack, levelLoadedID, levelLoadedName](){
            return new LoadingState<GameState, size_t, bool>(*contextWindow, *contextStack, levelLoadedID, LevelMenuState::isLevelOfficial(levelLoadedName));
        });
    };
    pauseMenu->addComponent(restartButton);

    alphGUI::Button* quitButton = new alphGUI::Button("Quit");
    quitButton->clickFunctionCallback = [this]()
    {
        sf::RenderWindow* contextWindow = stateContext.window;
        StateStack* contextStack = stateContext.stateStack;
        stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE); //pause state
        stateContext.stateStack->requestAction(StateStack::Actions::POP_STATE); //game state
        stateContext.stateStack->requestAction(StateStack::Actions::PUSH_STATE, [contextWindow, contextStack]()
        {
            return new LoadingState<TitleMenuState>(*contextWindow, *contextStack);
        });
    };

    pauseMenu->addComponent(quitButton);

    pauseMenu->setPosition(sf::Vector2f(WIDTH, HEIGHT) * 0.5f - sf::Vector2f(pauseMenu->getBoundingRectangle().width,
                                                                pauseMenu->getBoundingRectangle().height) * 0.5f);
}
