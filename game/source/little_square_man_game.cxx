#include "little_square_man_game.hpp"
#include "title_menu_state.hpp"
#include "loading_state.hpp"
#include "gui_component.hpp"
#include <iostream>


LittleSquareManGame::LittleSquareManGame()
    :window(sf::VideoMode(WIDTH, HEIGHT), "Little Square-Man!", sf::Style::Close),
    fps(sf::seconds(1.f/GAME_FPS)), fpsFont(), fpsText(), stateStack()
{
    if(!fpsFont.loadFromFile(FONT_DIR + std::string("calibri.ttf")))
    throw std::runtime_error("couldn't load resources/fonts/calibri.ttf (maybe it doesn't exist)");

    alphGUI::Component::setupComponentsResources(ALPH_GUI_RESOURCE_DIR_LOCATION);

    fpsText.setFont(fpsFont);
    fpsText.setCharacterSize(10U);
    fpsText.setPosition(5.f, 5.f);
    fpsText.setFillColor(sf::Color::Black);

    stateStack.requestAction(StateStack::Actions::PUSH_STATE, [this](){
        LoadingState<TitleMenuState>* loadingMenuState = new LoadingState<TitleMenuState>(window, stateStack);
        return loadingMenuState;
    });

}

LittleSquareManGame::~LittleSquareManGame()
{
    alphGUI::Component::freeComponentsResources();
}

void LittleSquareManGame::run()
{
    sf::Clock gameClock;
    sf::Time accu = sf::Time::Zero, realDeltaTime = sf::Time::Zero;
    sf::Time realFrameRateAccu = sf::Time::Zero;
    //game loop
    while(window.isOpen())
    {
        realDeltaTime = gameClock.restart();
        accu += realDeltaTime;
        realFrameRateAccu += realDeltaTime;

        //handle the polling of events from the window
        handleEvent();

        //update the application
        while(accu >= fps)
        {
            accu -= fps;

            update();

        } 

        //update the real frame rate displaying text every 0.5 seconds
        if(realFrameRateAccu >= sf::seconds(0.5f))
        {
            realFrameRateAccu = sf::Time::Zero;

            fpsText.setString(std::to_string((int)(1 / realDeltaTime.asSeconds())) + " fps\n" + std::to_string(realDeltaTime.asMicroseconds()) + "us");
        }
        
        render();
    }
}

void LittleSquareManGame::handleEvent()
{
    sf::Event e;
    while(window.pollEvent(e))
    {
        if(e.type == sf::Event::Closed)
            window.close();

        stateStack.handleEvent(e);
    }
}

void LittleSquareManGame::update()
{
    stateStack.update(fps);
}

void LittleSquareManGame::render()
{
    window.clear(sf::Color::White);

    window.draw(stateStack);

    window.setView(window.getDefaultView());
    window.draw(fpsText);

    window.display();
}

int main()
{
    try
    {
       LittleSquareManGame game;

       game.run(); 
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }
    

    return EXIT_SUCCESS;
}