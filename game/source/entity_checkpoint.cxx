#include "entity_checkpoint.hpp"
#include "aabb_box.hpp"
#include "entity_splash_particle_emitter.hpp"
#include "game_level.hpp"
#include "load_and_save_manager.hpp"

constexpr unsigned int CHECKPOINT_HEIGHT = GameResources::WALL_SQUARE_SIZE / 5;

EntityCheckpoint::EntityCheckpoint(GameContext _context, CheckpointType _checkpointType)
    :Entity(_context), 
    checkpointReachedParticleEmitter(std::make_unique<EntitySplashParticleEmitter>(_context, GameResources::ParticleTypes::FIREWORK, sf::seconds(0.1f)))
    , checkpointSprite(_context.textures->get(GameResources::Textures::CHECKPOINT_TEXTURE)), checkpointType(_checkpointType)
{
    setTextureRectAccordingToType();
    checkpointReachedParticleEmitter->setPosition(0.f, CHECKPOINT_HEIGHT);
    checkpointReachedParticleEmitter->stopEmitting();
    checkpointReachedParticleEmitter->ParticlesPerSplashNumber = 1;
}

EntityCheckpoint::~EntityCheckpoint()
{
}

void EntityCheckpoint::update(sf::Time deltaTime)
{
    checkpointReachedParticleEmitter->update(deltaTime);
}

GameResources::EntityType EntityCheckpoint::getEntityType() const
{
    return GameResources::EntityType::CHECKPOINT;
}

GameResources::RenderingLayers EntityCheckpoint::getEntityRenderingLayer() const
{
    return GameResources::RenderingLayers::INTERACTION_BOTTOM_LAYER;
}

std::unique_ptr<CollisionBox> EntityCheckpoint::getCollisionBox()
{
    auto ptrBox = std::make_unique<AABBBox>(getTransform().transformRect(checkpointSprite.getLocalBounds()));
    
    return std::move(ptrBox);
}

void EntityCheckpoint::loadEntity(const nlohmann::json &jsonToLoadFrom)
{
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadFrom);
    setCheckpointType(static_cast<CheckpointType>(jsonToLoadFrom[GameResources::JSON_ENTITY_CHECKPOINT_TYPE_STRING]));
}

void EntityCheckpoint::saveEntity(nlohmann::json &jsonToSaveTo)
{
    nlohmann::json checkpointJSON;
    checkpointJSON[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getEntityType();
    LoadAndSaveManager::saveTransformable(*this, checkpointJSON);
    checkpointJSON[GameResources::JSON_ENTITY_CHECKPOINT_TYPE_STRING] = static_cast<unsigned int>(checkpointType);

    jsonToSaveTo.push_back(checkpointJSON);
}

EntityCheckpoint::CheckpointType EntityCheckpoint::getCheckpointType() const
{
    return checkpointType;
}

void EntityCheckpoint::setCheckpointType(CheckpointType newCheckpointType)
{
    if(checkpointType != newCheckpointType)
    {
        switch (newCheckpointType)
        {
        case CheckpointType::START:
            checkpointReachedParticleEmitter->startEmitting();
            playerHasReachedCheckpoint = true;
            sentOutResetCommandForOtherNonEndTypeCheckpoint();
            levelContext->levelStartPosition.x = getPosition().x + checkpointSprite.getLocalBounds().width * 0.5f;
            levelContext->levelStartPosition.y = getPosition().y - GameResources::WALL_SQUARE_SIZE;
            break;
        case CheckpointType::MIDDLE_CHECKPOINT:
            playerHasReachedCheckpoint = false;
            break;
        default:
            break;
        }

        checkpointType = newCheckpointType;
        setTextureRectAccordingToType();
    }

}

void EntityCheckpoint::sentOutResetCommandForOtherNonEndTypeCheckpoint()
{
    InteractionCommand resetCommand;
    resetCommand.entityToSendCommandTo = GameResources::EntityType::CHECKPOINT;
    resetCommand.action = [this](Entity* otherCheckpoint)
    {
        EntityCheckpoint* realOtherCheckpoint = static_cast<EntityCheckpoint*>(otherCheckpoint);
        if(realOtherCheckpoint != this && realOtherCheckpoint->getCheckpointType() != CheckpointType::END)
        {
            realOtherCheckpoint->playerHasReachedCheckpoint = false;
            realOtherCheckpoint->setCheckpointType(CheckpointType::MIDDLE_CHECKPOINT);
        }
    };

    levelContext->pushInteractionCommand(resetCommand);
}

void EntityCheckpoint::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(checkpointSprite, states);
    target.draw(*(checkpointReachedParticleEmitter.get()), states);
}

void EntityCheckpoint::setTextureRectAccordingToType()
{
    sf::IntRect checkpointStartTextureRect = sf::IntRect(0,0, GameResources::WALL_SQUARE_SIZE, CHECKPOINT_HEIGHT);
    switch (checkpointType)
    {
    case CheckpointType::MIDDLE_CHECKPOINT:
        checkpointStartTextureRect.left = GameResources::WALL_SQUARE_SIZE;
        break;
    case CheckpointType::END:
        checkpointStartTextureRect.left = 2U * GameResources::WALL_SQUARE_SIZE;
        break;
    
    default:
        break;
    }

    checkpointSprite.setTextureRect(checkpointStartTextureRect);
}
