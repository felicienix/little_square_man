#ifndef ENTITY_PARTICLE_EMITTER_HPP
#define ENTITY_PARTICLE_EMITTER_HPP
#include "entity.hpp"
#include "particle_system.hpp"

class EntityParticleEmitter : public Entity, public sf::Transformable
{
public:
  
    EntityParticleEmitter(GameContext _context, GameResources::ParticleTypes typeOfParticles);
    virtual ~EntityParticleEmitter() override;

    virtual void update(sf::Time deltaTime) override;

    virtual GameResources::EntityType getEntityType() const override;
    virtual GameResources::RenderingLayers getEntityRenderingLayer() const override;

    virtual void loadEntity(const nlohmann::json& jsonToLoadFrom) override;
    virtual void saveEntity(nlohmann::json& jsonToSaveTo) override;

    virtual void startEmitting();
    virtual void stopEmitting();

protected:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    std::unique_ptr<class ParticleEmitterSpecifier> emitterSpecifier;
    unsigned int amountOfParticlesEmittedPerSecond;
    ParticleSystem particleSystem;
    sf::Time emittingInterval;
    sf::Time emittingAccu;
    bool isEmitting = false;
};


#endif //ENTITY_PARTICLE_EMITTER_HPP