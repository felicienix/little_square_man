#ifndef GAME_LEVEL_HPP
#define GAME_LEVEL_HPP
#include "global_resource_identifiers.hpp"
#include "entities_resources_and_identifiers.hpp"
#include <array>
#include <functional>

/// @brief an interaction rule is a collision
/// test that happens every frame between two types of entities,
/// and if they collide, action() is called with the corresponding collision overlap data
struct InteractionRule
{
    GameResources::EntityType firstEntity;
    GameResources::EntityType secondEntity;
    
    std::function<void(class Entity*, class Entity*, const OverlapData&)> action;
};

/// @brief an interaction command is an command sent out to an
/// entity type which, if it reaches the entity type, executes the
/// action() function call. It only happens once and the command is then popped
/// out of existence
struct InteractionCommand
{
    GameResources::EntityType entityToSendCommandTo;

    std::function<void(class Entity*)> action;
};

class GameLevel : public sf::Drawable
{
public:
    GameLevel(GameContext _context);
    ~GameLevel();

    void update(sf::Time deltaTime);
    void handleEvent(const sf::Event& event);

    void loadLevel(const std::string& levelToLoad);
    void saveLevel();

    bool canChangeLevel() const;

    // you cannot pop an interaction command because it is going 
    // to be popped next frame anyway
    void pushInteractionCommand(const InteractionCommand& command);
    // a rule, on the other hand, is here to stay si you might want to
    // remove a rule sometimes
    void pushInteractionRule(const InteractionRule& rule);
    //the actions of those kind of rules are triggered only the moment when the two kinds of entities are no longer colliding,
    //so only when one entity leaves the collision box of the other entity.
    //Note: the collision overlap information is useless and should not be used when the action is triggered as the entities are no longer colliding
    void pushNoLongerInteractingRule(const InteractionRule& rule);
    void popNoLongerInteractingRule();
    void popInteractionRule();

    void clearAllEntities();

    std::string getLevelName() const;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void keepCameraInsideLevelBounds();
    void setupInteractionRules();
    void executeCommands();
    void executeInteractionRules();
    
    GameContext context;
public:
    sf::Vector2f levelStartPosition;
private:
    std::string levelLoaded;
    sf::View levelView;
    sf::FloatRect levelBounds;
    class EntityCharacter* littleSquareMan; //the main character the user is controlling
    /// @brief list of entities by type, used to process the interaction rules and commands
    std::array<std::list<class Entity*>, GameResources::ENTITIES_COUNT> entitiesToProcess;
    
    struct NoLongerInteractingInformation
    {
        class Entity* firstEntity = nullptr;
        class Entity* secondEntity = nullptr;
    };
    //interaction and command stacks (implemented using vectors)
    std::vector<InteractionRule> interactionRules;
    std::vector<std::pair<InteractionRule, NoLongerInteractingInformation>> noLongerInteractingRules;
    std::vector<InteractionCommand> interactionCommands;

    /// @brief list of entities by rendering layers, used to render in a certain order
    std::array<std::list<class Entity*>, GameResources::LAYERS_COUNT> entitiesToRender;

    // entities that are dead
    std::list<class Entity*> deadEntities;

    bool changeLevel = false;
};

#endif //GAME_LEVEL_HPP