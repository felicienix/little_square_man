#ifndef LEVEL_MENU_STATE_HPP
#define LEVEL_MENU_STATE_HPP
#include "state.hpp"

namespace alphGUI
{
    class MenuManager;
    class Component;
}

class LevelMenuState : public State
{
public:
    //needs to have the window, stack, and the resources already loaded in the context
    LevelMenuState(GameContext _context);
    ~LevelMenuState();

    bool handleEvent(const sf::Event& event) override;
    bool update(sf::Time deltaTime) override;

    static bool isLevelOfficial(const std::string& levelName);
    static size_t getLevelNumberFromName(const std::string& levelName);
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void buildLevelsMenu();
    std::vector<alphGUI::Component*> buildOfficialLevelButtons();
    std::vector<alphGUI::Component*> buildNonOfficialLevelButtons();
    /// levelsMenuManager handles the two pagination menus for official and non official levels
    std::unique_ptr<alphGUI::MenuManager> levelsMenuManager;
    sf::Sprite levelsMenuBackground;
    sf::Text goBackText;

};



#endif //LEVEL_MENU_STATE_HPP