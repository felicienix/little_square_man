#ifndef ENTITY_ROCK_HPP
#define ENTITY_ROCK_HPP
#include "entity.hpp"
#include "polygon.hpp"

class EntityRock : public Entity, public Polygon
{
public:
    EntityRock(GameContext _context);
    virtual ~EntityRock() override;

    virtual GameResources::EntityType getEntityType() const override;
    virtual GameResources::RenderingLayers getEntityRenderingLayer() const override;
    virtual std::unique_ptr<class CollisionBox> getCollisionBox();

    virtual void loadEntity(const nlohmann::json& jsonToLoadFrom) override;
    virtual void saveEntity(nlohmann::json& jsonToSaveTo) override;

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
private:
    void setupPolygon();

    sf::ConvexShape visualRock;
};



#endif //ENTITY_ROCK_HPP