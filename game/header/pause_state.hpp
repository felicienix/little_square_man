#ifndef PAUSE_STATE_HPP
#define PAUSE_STATE_HPP
#include "state.hpp"

namespace alphGUI
{
    class Menu;
}

class PauseState : public State
{
public:
    PauseState(GameContext _context);
    ~PauseState() override;
    
    bool handleEvent(const sf::Event& event) override;
    bool update(sf::Time deltaTime) override;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override; 

    void buildPauseMenu();

    std::unique_ptr<alphGUI::Menu> pauseMenu;
};


#endif //PAUSE_STATE_HPP