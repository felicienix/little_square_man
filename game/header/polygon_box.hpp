#ifndef POLYGON_BOX_HPP
#define POLYGON_BOX_HPP
#include "collision_box.hpp"

class PolygonBox : public CollisionBox
{
public:
    PolygonBox();
    PolygonBox(const std::vector<sf::Vector2f>& points);
    ~PolygonBox() override;

    bool doesCollideWith(const CollisionBox& otherBox, OverlapData& collisionData) override;

    Type getCollisionBoxType() const override;

    std::vector<sf::Vector2f> polygonPoints;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif //POLYGON_BOX_HPP