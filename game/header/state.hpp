#ifndef STATE_HPP
#define STATE_HPP
#include "global_resource_identifiers.hpp"

class State : public sf::Drawable
{
public:
    State(sf::RenderWindow& contextWindow, class StateStack& stateStack);
    State(GameContext _context);
    virtual ~State();
    
    /// the boolean return values is used to say whether the other states that are under this state 
    /// in the state stack can also update / handleEvent (true --> they can, false --> they cannot)
    virtual bool handleEvent(const sf::Event& event) = 0;
    virtual bool update(sf::Time deltaTime) = 0;

    //whether this state is at the top of the stack
    virtual void setForeGroundState(bool isForeGround);
    bool isStateAtForeGround() const;
protected:

    GameContext stateContext;
    bool isAtForeGround;

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0; 
};


#endif //STATE_HPP