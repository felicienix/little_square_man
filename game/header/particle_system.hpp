#ifndef PARTICLE_SYSTEM_HPP
#define PARTICLE_SYSTEM_HPP
#include "global_resource_identifiers.hpp"
#include <deque>


class ParticleSystem : public sf::Drawable
{
public:
	explicit ParticleSystem(const sf::Texture& particleTexture, const PhysicsEngine& contextPhysicsEngine);
	~ParticleSystem() override;

	void addParticle(const sf::Vector2f& position, sf::Color color, sf::Time lifeTime, const sf::Vector2f& velocity);


	void update(sf::Time dt);

	bool enableGravityOnParticles = false;
private:
	struct Particle
	{

		sf::Vector2f position;
		sf::Color color;
		sf::Time lifeTime;
		sf::Time timeToLive;
		sf::Vector2f velocity;
	};

	void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void addVertex(float worldX, float worldY, float texCoordX, float texCoordY, const sf::Color& color) const;
	void computeVertices() const;

	const PhysicsEngine& engine;
	const sf::Texture& texture;
	std::deque<Particle> particles;

	mutable sf::VertexArray vertexArray;
	mutable bool needsVertexUpdate;
};

#endif //PARTICLE_SYSTEM_HPP