#ifndef THROW_ROCK_ACTION_HPP
#define THROW_ROCK_ACTION_HPP
#include "entity.hpp"

class EntityThrowRockAction : public Entity, public sf::Transformable
{

public:
    EntityThrowRockAction(GameContext _context, class EntityCharacter* _character);
    ~EntityThrowRockAction();

    void update(sf::Time deltaTime) override;
    
    GameResources::EntityType getEntityType() const override;

    void startAction();
    void nextStep();

    bool isActionFinished() const;

    sf::Vector2f getFinalVelocity() const;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    enum Steps
    {
        START, CHOOSE_DIR, CHOOSE_SPEED, FINISH
    };


    class EntityCharacter* character;
    sf::Sprite arrow;
    Steps currentStep;
    float arrowRotationAngle = 0.f;
    float arrowSpeedDial = 0.f;
    sf::Time accuTimeRotationAngle = sf::Time::Zero;
    sf::Time accuTimeSpeedDial = sf::Time::Zero;
    float initialArrowWidth;
};




#endif //THROW_ROCK_ACTION_HPP