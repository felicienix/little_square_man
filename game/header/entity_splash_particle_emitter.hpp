#ifndef ENTITY_SPLASH_PARTICLE_EMITTER_HPP
#define ENTITY_SPLASH_PARTICLE_EMITTER_HPP
#include "entity_particle_emitter.hpp"
#include "character_entity_identifiers.hpp"

class EntitySplashParticleEmitter : public EntityParticleEmitter
{
public:
    EntitySplashParticleEmitter(GameContext _context, GameResources::ParticleTypes typeOfParticles, sf::Time _emissionTime, float _width = GameResources::CHARACTER_SPRITE_WIDTH);
    ~EntitySplashParticleEmitter() override;

    void update(sf::Time deltaTime) override;

    float width;
    sf::Time emissionTime;  //how long  the 'splash' will be going for
    unsigned int ParticlesPerSplashNumber = 2;  //can be a lot
private:
    sf::Time emissionTimeAccu = sf::Time::Zero;
};

#endif //ENTITY_SPLASH_PARTICLE_EMITTER_HPP