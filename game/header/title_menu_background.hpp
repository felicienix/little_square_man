#ifndef TITLE_MENU_BACKGROUND_HPP
#define TITLE_MENU_BACKGROUND_HPP
#include "global_resource_identifiers.hpp"

/// @brief class handling the rendering, updating and events for the background of the title menu
class TitleMenuBackground final : private sf::NonCopyable, public sf::Drawable
{
public:
    TitleMenuBackground(GameContext _context);
    ~TitleMenuBackground();

    void update(sf::Time deltaTime);
    void handleEvent(const sf::Event& event);

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void buildWindowBounds();
    void spawnMovingShapes();

    GameContext context;
    PhysicsEngine engine;
    std::list<class RandomTitleBackgroundPolygon*> movingShapes;
    class RandomTitleBackgroundPolygon* attachedToCursor = nullptr;
    sf::Vector2f currentMousePosition;
    sf::Vector2f previousMousePosition;
};

#endif //TITLE_MENU_BACKGROUND_HPP