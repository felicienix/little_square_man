#ifndef ENTITY_DEATH_BLOCK_HPP
#define ENTITY_DEATH_BLOCK_HPP
#include "entity_wall.hpp"

class EntityDeathBlock : public EntityWall
{
public:
    EntityDeathBlock(GameContext _context);
    ~EntityDeathBlock() override;

    GameResources::EntityType getEntityType() const override;
    void loadEntity(const nlohmann::json& jsonToLoadFrom) override;
    void saveEntity(nlohmann::json& jsonToSaveTo) override;
};



#endif //ENTITY_DEATH_BLOCK_HPP