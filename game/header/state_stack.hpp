#ifndef STATE_STACK_HPP
#define STATE_STACK_HPP
#include "state.hpp"
#include <queue>
#include <vector>
#include <functional>

/// @brief  handles the transition between states and the pushing/popping of the states
class StateStack final : public sf::Drawable
{
public:

    /// @brief the list of actions the user of this class can do.
    /// all the requested actions will be performed at the end of each frame,
    /// once all the states have finished updating.
    /// the push state just pushes a state on top of the stack but the change state action 
    /// does a transition between the current state on top, pushes the new state and removes the older one.
    enum class Actions
    {
        PUSH_STATE, POP_STATE, CHANGE_STATE, POP_ALL_STATES
    };

    StateStack();
    ~StateStack();

    void handleEvent(const sf::Event& event);
    void update(sf::Time deltaTime);
    /// @brief  requesting that an action be done at the end of this frame
    /// @param action see Actions for the different actions possible
    /// @param createState function to create the state that we want to create in case of PUSH or CHANGE_STATE action
    /// optional param and returns a null pointer by default, isn't use in case of POP action
    void requestAction(Actions action, const std::function<State*()>& createState = [](){return nullptr;});

private:
    std::vector<State*> stack;
    std::queue<std::pair<Actions, std::function<State*()>>> actionRequests;
    
    /// state transition members
    sf::Time transitionDelay = sf::seconds(2.5f);
    sf::Time transitionAccumulator = sf::Time::Zero;
    sf::RectangleShape transitionFadeScreen;
    bool isTransitioning = false;
    bool transitioningMiddlePointReached = false;
    std::function<State*()> createChangeStateActionRequest = nullptr;
    
    void applyActions();
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};



#endif //STATE_STACK_HPP