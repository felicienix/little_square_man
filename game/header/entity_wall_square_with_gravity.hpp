#ifndef ENTITY_WALL_SQUARE_WITH_GRAVITY
#define ENTITY_WALL_SQUARE_WITH_GRAVITY
#include "entity_rock.hpp"


class EntityWallSquareWithGravity : public EntityRock
{
public:
    EntityWallSquareWithGravity(GameContext _context);
    ~EntityWallSquareWithGravity() override;

    GameResources::EntityType getEntityType() const override;

    void loadEntity(const nlohmann::json& jsonToLoadFrom) override;
    void saveEntity(nlohmann::json& jsonToSaveTo) override;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    sf::Sprite square;
};


#endif //ENTITY_WALL_SQUARE_WITH_GRAVITY