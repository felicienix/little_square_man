#ifndef ENTITY_CHECKPOINT_HPP
#define ENTITY_CHECKPOINT_HPP
#include "entity.hpp"

class EntityCheckpoint : public Entity, public sf::Transformable
{
public:
    enum class CheckpointType
    {
        START, MIDDLE_CHECKPOINT, END
    };

    EntityCheckpoint(GameContext _context, CheckpointType _checkpointType = CheckpointType::MIDDLE_CHECKPOINT);
    ~EntityCheckpoint() override;

    void update(sf::Time deltaTime) override;

    GameResources::EntityType getEntityType() const override;
    GameResources::RenderingLayers getEntityRenderingLayer() const override;
    std::unique_ptr<class CollisionBox> getCollisionBox();

    void loadEntity(const nlohmann::json& jsonToLoadFrom) override;
    void saveEntity(nlohmann::json& jsonToSaveTo) override;

    CheckpointType getCheckpointType() const;
    void setCheckpointType(CheckpointType newCheckpointType);

    void sentOutResetCommandForOtherNonEndTypeCheckpoint();

    std::unique_ptr<class EntitySplashParticleEmitter> checkpointReachedParticleEmitter;
    bool playerHasReachedCheckpoint = false;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void setTextureRectAccordingToType();

    sf::Sprite checkpointSprite;
    CheckpointType checkpointType;
};

#endif //ENTITY_CHECKPOINT_HPP