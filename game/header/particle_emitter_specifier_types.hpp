#ifndef PARTICLE_EMITTER_SPECIFIER_TYPES_HPP
#define PARTICLE_EMITTER_SPECIFIER_TYPES_HPP
#include "particle_emitter_specifier.hpp"

class WalkingParticleSpecifier : public ParticleEmitterSpecifier
{
public:
    WalkingParticleSpecifier();
    ~WalkingParticleSpecifier() override;

    sf::Vector2f particleInitialPosition(const class EntityParticleEmitter& parentEmitter) override;
    sf::Vector2f particleVelocity(sf::Time deltaTime) override;
    sf::Color particleColor(sf::Time deltaTime) override;
private:
    sf::Color color;
    sf::Vector2f velocity;
};

class FireworksParticleSpecifier : public ParticleEmitterSpecifier
{
public:
    FireworksParticleSpecifier();
    ~FireworksParticleSpecifier() override;

    sf::Vector2f particleInitialPosition(const class EntityParticleEmitter& parentEmitter) override;
    sf::Vector2f particleVelocity(sf::Time deltaTime) override;
    sf::Color particleColor(sf::Time deltaTime) override;
private:
    sf::Color color = sf::Color::Blue;
};

class JumpingBackDownParticleSpecifier : public ParticleEmitterSpecifier
{
public:
    JumpingBackDownParticleSpecifier();
    ~JumpingBackDownParticleSpecifier() override;

    sf::Vector2f particleInitialPosition(const class EntityParticleEmitter& parentEmitter) override;
    sf::Vector2f particleVelocity(sf::Time deltaTime) override;
    sf::Color particleColor(sf::Time deltaTime) override;
private:
    bool opositeXVelocity = false;
};


#endif //PARTICLE_EMITTER_SPECIFIER_TYPES_HPP