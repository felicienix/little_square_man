#ifndef LITTLE_SQUARE_MAN_GAME_HPP
#define LITTLE_SQUARE_MAN_GAME_HPP
#include "global_resource_identifiers.hpp"
#include "state_stack.hpp"


class LittleSquareManGame
{
public:
    LittleSquareManGame();
    ~LittleSquareManGame();

    void run();

private:

    sf::RenderWindow window;
    sf::Time fps;
    sf::Font fpsFont;
    sf::Text fpsText;

    StateStack stateStack;

    void handleEvent();
    void update();
    void render();
};


#endif //LITTLE_SQUARE_MAN_GAME_HPP