#ifndef ENTITY_WALL_HPP
#define ENTITY_WALL_HPP
#include "entity.hpp"
#include "physical_aabb.hpp"

class EntityWall : public Entity, public PhysicalAABB
{
public:
    EntityWall(GameContext _context);
    virtual ~EntityWall() override;

    virtual void update(sf::Time deltaTime) override;
    virtual void handleEvent(const sf::Event& event) override;

    virtual GameResources::EntityType getEntityType() const override;
    virtual GameResources::RenderingLayers getEntityRenderingLayer() const override;
    virtual void loadEntity(const nlohmann::json& jsonToLoadFrom) override;
    virtual void saveEntity(nlohmann::json& jsonToSaveTo) override;

    virtual std::unique_ptr<class CollisionBox> getCollisionBox() override;
protected:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    virtual void setupPhysicalAABB();

    sf::Sprite wallSprite;
};

#endif //ENTITY_WALL_HPP