#ifndef COLLISION_BOX_HPP
#define COLLISION_BOX_HPP
#include "global_resource_identifiers.hpp"

class CollisionBox : public sf::Drawable
{
public:
    enum class Type
    {
        NONE, POLYGON, CIRCLE, AABB
    };

    CollisionBox(){}
    virtual ~CollisionBox(){}

    virtual bool doesCollideWith(const CollisionBox& otherCollisionBox, OverlapData& collisionData){return false;};
    
    virtual Type getCollisionBoxType() const{return Type::NONE;};

private:
    //only to be used when debugging
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const{};
};

#endif //COLLISION_BOX_HPP