#ifndef SETTINGS_STATE_HPP
#define SETTINGS_STATE_HPP
#include "state.hpp"


namespace alphGUI
{
    class Menu;
}

class SettingsState : public State
{
public:
    SettingsState(GameContext _context);
    ~SettingsState();

    bool handleEvent(const sf::Event& event) override;
    bool update(sf::Time deltaTime) override;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override; 
    void buildSettingsMenu();

    std::unique_ptr<alphGUI::Menu> settingsMenu;
    sf::RectangleShape menuBackgroundDim;
};



#endif //SETTINGS_STATE_HPP