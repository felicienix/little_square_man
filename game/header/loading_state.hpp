#ifndef LOADING_STATE_HPP
#define LOADING_STATE_HPP
#include "state.hpp"
#include <tuple>

/**
 * @brief a loading state that loads another state with a progress bar
 * 
 * @tparam StateToLoad the other state to push once the loading is finished, must have a 'buildState' method (see TitleMenuState)
 * @tparam StateToLoadExtraArgs the state to load extra arguments to be passed on when creating it
 */
template<class StateToLoad, class... StateToLoadExtraArgs>
class LoadingState : public State
{
public:
    LoadingState(sf::RenderWindow& contextWindow, class StateStack& stateStack, StateToLoadExtraArgs... args);
    ~LoadingState() override;

    bool handleEvent(const sf::Event& event) override;
    bool update(sf::Time deltaTime) override;

private:
    std::tuple<StateToLoadExtraArgs...> stateToLoadExtraArgs;
    TextureHolder textures;
    sf::Sprite loadingBackground;
    sf::RectangleShape loadingBar;
    float barTotalProgressWidth = 334.f;
    volatile float progressAmount = 0.f;
    sf::Mutex loadingThreadMutex;
    sf::Thread loadingThread;
    StateToLoad* stateToLoad = nullptr;
    bool changeRequestDone = false;

    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void loadingThreadFunc();
};

#include "loading_state.inl"

#endif //LOADING_STATE_HPP