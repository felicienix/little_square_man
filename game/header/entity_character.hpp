#ifndef ENTITY_CHARACTER_HPP
#define ENTITY_CHARACTER_HPP
#include "entity.hpp"
#include "polygon.hpp"
#include "polygon_box.hpp"
#include <array>
#include "character_entity_identifiers.hpp"

//TODO: animate when the character comes back to the ground after jumping
//      and add particle effect when doing so (maybe within the animation ?)

//the main character of the game, controlled by the user
class EntityCharacter : public Entity, public Polygon
{
public:
    EntityCharacter(GameContext _context);
    ~EntityCharacter() override;

    void update(sf::Time deltaTime) override;
    void handleEvent(const sf::Event& event) override;

    GameResources::EntityType getEntityType() const override;
    GameResources::RenderingLayers getEntityRenderingLayer() const override;
    void loadEntity(const nlohmann::json& jsonToLoadFrom) override;
    void saveEntity(nlohmann::json& jsonToSaveTo) override;

    std::unique_ptr<CollisionBox> getCollisionBox() override;

    static std::array<sf::Keyboard::Key, GameResources::CharacterControls::CONTROLS_COUNT> characterControls;
    static void updateCharacterControlFileContent();
    static void updateCharacterControlsArray();

    // 1 for to the right, -1 for to the left
    int whichWayIsCharacterFacing() const;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    
    void setupPolygon();
    void setupAnimations();
    void handleKeyPressedEvents(const sf::Event& event);
    void handleKeyReleasedEvents(const sf::Event& event);
    bool isInTheAir() const;

    void handleMovingAndJumpingAnimationChange();
    void handleBlinkingAnim(sf::Time deltaTime);
    void handleMovement(sf::Time deltaTime);
    void handleParticleEmitters(sf::Time deltaTime);
    void makeCharacterRun();
    void stopCharacterFromRunning();

    enum CharacterAnimations
    {
        IDLE_LEFT, WALKING_LEFT, JUMPING_LEFT, IDLE_RIGHT, WALKING_RIGHT, JUMPING_RIGHT, CHARACTER_ANIMATIONS_COUNT,
        LEFT_ANIMATIONS_COUNT = JUMPING_LEFT
    };
    void startBlinking(CharacterAnimations animation);
    void stopBlinking(CharacterAnimations animation);
    
    sf::Sprite littleSquareMan;
    std::array<class Animation*, CHARACTER_ANIMATIONS_COUNT> littleSquareManAnimations;
    CharacterAnimations currentlyActiveAnimation = IDLE_LEFT;

    //blinking stuff
    sf::Time blinkingDuration;
    sf::Time blinkingTimeAccu = sf::Time::Zero;
    sf::Time stopBlinkingRandomDuration;
    sf::Time stopBlinkingAccu = sf::Time::Zero;
    bool isBlinking = false;

    //movement stuff
    float speed;
    float jumpingSpeed;
    sf::Vector2f direction;
    bool isRunning = false;
    bool isTheCharacterFlying = false;

    //particle stuff
    std::unique_ptr<class EntityParticleEmitter> walkingParticleEmitter;
    std::unique_ptr<class EntitySplashParticleEmitter> jumpingBackDownParticleEmitter;
    bool isPreviouslyInTheAir = false;
    float maxAirYVelocity = 0.f;

    class EntityRock* selectedRock = nullptr;
    mutable sf::Text textHelperToSelectRock;
    bool hasRockInHand = false;
    bool isInRangeOfRock = false;
    std::unique_ptr<class EntityThrowRockAction> throwRockAction;
    //debug stuff
    //PolygonBox hitbox;
};


#endif //ENTITY_CHARACTER_HPP