#ifndef RANDOM_FUNCTIONS_HPP
#define RANDOM_FUNCTIONS_HPP
#include <random>
#include <chrono>

namespace lsmRandom
{
    namespace
    {
        std::default_random_engine createRandomEngine()
        {
            auto seed = static_cast<unsigned long>(std::time(nullptr));
            return std::default_random_engine(seed);
        }

        auto RandomEngine = createRandomEngine();
    }

    static float randomFloat(float min, float max)
    {
        std::uniform_real_distribution<> distr(min, max);
        return distr(RandomEngine);
    }

    static int randomInt(int min, int max)
    {
        std::uniform_int_distribution<> distr(min, max);
        return distr(RandomEngine);
    }


}


#endif //RANDOM_FUNCTIONS_HPP