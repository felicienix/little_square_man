#ifndef CIRCLE_BOX_HPP
#define CIRCLE_BOX_HPP
#include "collision_box.hpp"

class CircleBox : public CollisionBox
{
public:
    CircleBox();
    CircleBox(const sf::Vector2f& _center, float _radius);
    ~CircleBox() override;

    bool doesCollideWith(const CollisionBox& otherBox, OverlapData& collisionData) override;

    Type getCollisionBoxType() const override;

    sf::Vector2f center;
    float radius;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif //CIRCLE_BOX_HPP