#ifndef ENTITIES_RESOURCES_AND_IDENTIFIERS_HPP
#define ENTITIES_RESOURCES_AND_IDENTIFIERS_HPP

namespace GameResources
{
	enum Textures
	{
		MAIN_CHARACTER, SQUARE_TEXTURE, CHECKPOINT_TEXTURE, PLATFORM_TEXTURE, DEFAULT_PARTICLE_TEXTURE, FANCY_PARTICLE_TEXTURE, ARROW_TEXTURE, TEXTURES_COUNT
	};

	enum Fonts
	{
		TITLE_FONT, FONTS_COUNT
	};

	enum Sounds
	{
		SOUNDS_COUNT
	};

	enum EntityType
	{
		CHARACTER, WALL, PARTICLE_EMITTER, ROCK, DEATH_BLOCK, PLATFORM, CHECKPOINT, WALL_SQUARE_WITH_GRAVITY, ENTITIES_JUMPABLE_ON, THROW_ROCK_ACTION, ENTITIES_COUNT
	};

	enum RenderingLayers
	{
		BACKGROUND_LAYER, INTERACTION_BOTTOM_LAYER, INTERACTION_TOP_LAYER, UI_LAYER, LAYERS_COUNT
	};

	enum ParticleTypes
	{
		WALKING_PARTICLE, FIREWORK, JUMPING_BACK_DOWN_SPLASH, PARTICLE_TYPES_COUNT
	};

	constexpr const char* JSON_ENTITIES_ARRAY_STRING = "entities";
	constexpr const char* JSON_GAME_LEVELBOUNDS_STRING = "levelBounds";
	constexpr const char* JSON_GAME_LEVEL_INITIAL_CHARACTER_POSITION = "levelStart";
	constexpr const char* JSON_ENTITY_TYPE_STRING = "type";
	constexpr const char* JSON_ENTITY_POSITION_STRING = "position";
	constexpr const char* JSON_ENTITY_ROTATION_STRING = "rotation";
	constexpr const char* JSON_ENTITY_SCALE_STRING = "scale";

	constexpr const char* JSON_ENTITY_WALL_SIZE_STRING = "wallSize";
	constexpr const char* JSON_ENTITY_CHECKPOINT_TYPE_STRING = "checkpointType";
	constexpr const char* JSON_ENTITY_ROCK_POINTS_ARRAY_STRING = "points";
	constexpr const char* JSON_ENTITY_ROCK_POINT_COUNT_STRING = "pointCount";
	constexpr const char* JSON_ENTITY_PLATFORM_MOVING_DISTANCE = "movingDistance";
	constexpr const char* JSON_ENTITY_PLATFORM_MOVING_DURATION = "movingDuration";	//the time it takes to complete the moving distance in seconds
	constexpr const char* JSON_ENTITY_PLATFORM_MOVING_START_DELAY = "movingStartDelay";

	constexpr const int WALL_SQUARE_SIZE = 80;

	//const std::vector<sf::Vector2f> PLATFORM_POLYGON_POINTS = {};
}


#endif //ENTITIES_RESOURCES_AND_IDENTIFIERS_HPP