#ifndef GAME_TITLE_HPP
#define GAME_TITLE_HPP
#include "global_resource_identifiers.hpp"

class GameTitle : public sf::Drawable, public sf::Transformable
{
public:
    GameTitle(GameContext _context);
    ~GameTitle();

    void update(sf::Time deltaTime);

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    GameContext context;
    sf::Sprite title;
    sf::Sprite character;
    class Animation* characterAnimation;
};


#endif //GAME_TITLE_HPP