#ifndef RANDOM_TITLE_BACKGROUND_POLYGON_HPP
#define RANDOM_TITLE_BACKGROUND_POLYGON_HPP
#include "polygon.hpp"
#include "global_resource_identifiers.hpp"

class RandomTitleBackgroundPolygon : public Polygon, public sf::Drawable
{
public:
    RandomTitleBackgroundPolygon(GameContext _context);
    ~RandomTitleBackgroundPolygon();

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    GameContext context;
    sf::Sprite sprite;
    float speed = 70.f;
};

#endif //RANDOM_TITLE_BACKGROUND_POLYGON_HPP