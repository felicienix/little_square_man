#include "resource_holder.hpp"
#include <exception>
#include <assert.h>
#include <iostream>

template<typename Resource, typename ID>
ResourceHolder<Resource, ID>::ResourceHolder()
	:fResources()
{
}

template<typename Resource, typename ID>
void ResourceHolder<Resource, ID>::load(const std::string& FileName, ID id)
{
	Resource* res = new Resource();
	if (!res->loadFromFile(FileName))
	{
		throw std::runtime_error("Unable to load file: " + FileName);
	}

	auto inserted = fResources.insert(std::make_pair(id, mapResource(res, FileName)));
	assert(inserted.second);
	if(!inserted.second)
		throw std::logic_error("couldn't insert resource " + FileName);
}

template<typename Resource, typename ID>
Resource* ResourceHolder<Resource, ID>::isResourceFileAlreadyLoaded(const std::string& fileName) const
{
	for(auto iter = fResources.begin(); iter != fResources.end(); ++iter)
	{
		if(iter->second.fileName == fileName)
			return iter->second.resource;
	}

	return nullptr;
}

template<typename Resource, typename ID>
const std::string& ResourceHolder<Resource, ID>::getResourceFileName(ID id) const
{
	auto found = fResources.find(id);
	assert(found != fResources.end());
	return found->second.fileName;
}

template<typename Resource, typename ID>
Resource& ResourceHolder<Resource, ID>::get(ID id)
{
	auto found = fResources.find(id);
	assert(found != fResources.end());
	return *found->second.resource;
}

template<typename Resource, typename ID>
const Resource& ResourceHolder<Resource, ID>::get(ID id) const
{
	auto found = fResources.find(id);
	assert(found != fResources.end());
	return *found->second.resource;
}

template <typename Resource, typename ID>
inline size_t ResourceHolder<Resource, ID>::getNumberOfResourceLoaded() const
{
    return fResources.size();
}

template<typename Resource, typename ID>
ResourceHolder<Resource, ID>::~ResourceHolder()
{
	for (auto res : fResources)
	{
		delete res.second.resource;
	}
}


template<typename Resource, typename ID>
template<typename Param>
void ResourceHolder<Resource, ID>::load(const std::string& FileName, ID id, Param SecondParam)
{
	Resource* res = new Resource();
	if (!res->loadFromFile(FileName, SecondParam))
	{
		throw std::runtime_error("Unable to load file: " + FileName);
	}
	auto inserted = fResources.insert(std::make_pair(id, mapResource(res, FileName)));
	assert(inserted.second);
	if(!inserted.second)
		throw std::logic_error("couldn't insert resource " + FileName);
}
