#ifndef AABB_BOX_HPP
#define AABB_BOX_HPP
#include "collision_box.hpp"

class AABBBox : public CollisionBox
{
public:
    AABBBox();
    AABBBox(const sf::Vector2f& topleft, const sf::Vector2f& widthheight);
    AABBBox(const sf::FloatRect& _box);
    ~AABBBox() override;

    bool doesCollideWith(const CollisionBox& otherBox, OverlapData& collisionData) override;

    Type getCollisionBoxType() const override;
    
    sf::FloatRect box;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
};

#endif //AABB_BOX_HPP