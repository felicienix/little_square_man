#ifndef ENTITY_HPP
#define ENTITY_HPP
#include "global_resource_identifiers.hpp"
#include "entities_resources_and_identifiers.hpp"

//DEAD entities will be removed from the level and deleted
enum class EntityState
{
    ALIVE, DEAD
};

/// anything that can be displayed in the game
class Entity : public sf::Drawable
{
public:

    Entity(GameContext _context);
    virtual ~Entity();

    //returns nullptr if it cannot be cast into a transformable
    static sf::Transformable* castEntityToTransformable(Entity* entity); 
    static Entity* createEntityFromType(GameResources::EntityType type, GameContext _context);

    virtual void update(sf::Time deltaTime){};
    virtual void handleEvent(const sf::Event& event){};

    virtual GameResources::EntityType getEntityType() const = 0;
    virtual GameResources::RenderingLayers getEntityRenderingLayer() const { return GameResources::RenderingLayers::BACKGROUND_LAYER; };
    virtual std::unique_ptr<class CollisionBox> getCollisionBox();

    virtual void loadEntity(const nlohmann::json& jsonToLoadFrom){};
    virtual void saveEntity(nlohmann::json& jsonToSaveTo){};

    /// @brief the game level where the entity lives
    class GameLevel* levelContext = nullptr;
    EntityState entityState = EntityState::ALIVE;
protected:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
    GameContext context;
};

#endif //ENTITY_HPP