#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP
#include "state.hpp"
#include "entities_resources_and_identifiers.hpp"

class GameState : public State
{
public:
    GameState(sf::RenderWindow& contextWindow, class StateStack& stateStack, size_t _levelToLoadNameIndex, bool isLevelOfficial);
    ~GameState();

    bool handleEvent(const sf::Event& event) override;
    bool update(sf::Time deltaTime) override;

    void buildState(volatile float& progress, sf::Mutex& loadingThreadMutex);

    static std::vector<std::string> getOfficialLevelNamesInOrder();
    static std::vector<std::string> getNonOfficialLevelNamesInOrder();
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void saveGame();

    TextureHolder gameTextures;
    FontHolder gameFonts;
    SoundBufferHolder gameSounds;
    PhysicsEngine gamePhysicsEngine;

    std::unique_ptr<class GameLevel> activeLevel;
    std::vector<std::string> gameLevelsInOrder;
    size_t levelToLoadNameIndex;
    bool isLevelLoadedOfficial;
    bool hasPressedControlKey = false;
};

#endif //GAME_STATE_HPP