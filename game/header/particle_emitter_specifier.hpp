#ifndef PARTICLE_EMITTER_SPECIFIER_HPP
#define PARTICLE_EMITTER_SPECIFIER_HPP
#include "global_resource_identifiers.hpp"
#include "entities_resources_and_identifiers.hpp"

class ParticleEmitterSpecifier
{
public:
    ParticleEmitterSpecifier();
    virtual ~ParticleEmitterSpecifier();

    virtual sf::Vector2f particleInitialPosition(const class EntityParticleEmitter& parentEmitter) = 0;
    virtual sf::Vector2f particleVelocity(sf::Time deltaTime) = 0;
    virtual sf::Color particleColor(sf::Time deltaTime) = 0;

    static ParticleEmitterSpecifier* createParticleEmitterSpecifier(GameResources::ParticleTypes type);

    GameResources::Textures particleTextureID; 
    bool isGravitySensitive;
    unsigned int numberOfParticlesPerSecond;
    sf::Time particleLifeTime;
};


#endif //PARTICLE_EMITTER_SPECIFIER_HPP