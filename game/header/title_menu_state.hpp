#ifndef TITLE_MENU_STATE_HPP
#define TITLE_MENU_STATE_HPP
#include "state.hpp"

namespace alphGUI
{
    class Menu;
}

class TitleMenuState : public State
{
public:
    /// @brief construct a title menu state without the texures
    /// or fonts, for his you need to call buildMenu
    TitleMenuState(sf::RenderWindow& contextWindow, class StateStack& stateStack);
    ~TitleMenuState() override;

    bool handleEvent(const sf::Event& event) override;
    bool update(sf::Time deltaTime) override;
    /// @brief builds the state by loading the resources and
    /// setting up the GUI
    /// @param progress the progress until completion of the state initialization
    /// (ratio percentage)
    void buildState(volatile float& progress, sf::Mutex& loadingThreadMutex);

private:
    TextureHolder textures;
    FontHolder fonts;
    SoundBufferHolder sounds;

    std::unique_ptr<class GameTitle> gameTitle;
    std::unique_ptr<alphGUI::Menu> titleGUIMenu;
    std::unique_ptr<class TitleMenuBackground> movingBackground;
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void buildGUIMenu();
};


#endif //TITLE_MENU_STATE_HPP