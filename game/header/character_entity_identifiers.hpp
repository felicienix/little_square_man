#ifndef CHARACTER_ENTITY_IDENTIFIERS_HPP
#define CHARACTER_ENTITY_IDENTIFIERS_HPP
#include <vector>
#include <array>
#include "SFML/System/Vector2.hpp"

namespace GameResources
{
	constexpr float CHARACTER_WALKING_SPEED = 150.f;
	constexpr float CHARACTER_JUMPING_SPEED = 400.f;
	constexpr int CHARACTER_SPRITE_WIDTH = 80;
	constexpr int CHARACTER_SPRITE_HEIGHT = 120;
	constexpr float CHARACTER_IDLE_DURATION = 1.f; //in seconds
	constexpr unsigned int CHARACTER_IDLE_NUM_FRAMES = 11;
	constexpr float CHARACTER_WALKING_DURATION = 0.5f; //in seconds
	constexpr float CHARACTER_RUNNING_DURATION = 0.25f; //in seconds
	constexpr unsigned int CHARACTER_WALKING_NUM_FRAMES = 6;
	constexpr float CHARACTER_JUMPING_DURATION = .5f; //in seconds
	constexpr unsigned int CHARACTER_JUMPING_NUM_FRAMES = 11;
	constexpr int CHARACTER_WALKING_LEFT_Y_START_COORD = CHARACTER_SPRITE_HEIGHT * 2;
	constexpr int CHARACTER_WALKING_RIGHT_Y_START_COORD = CHARACTER_SPRITE_HEIGHT * 6;
	constexpr int CHARACTER_IDLE_RIGHT_Y_START_COORD = CHARACTER_SPRITE_HEIGHT * 4;
	constexpr int CHARACTER_JUMPING_LEFT_Y_START_COORD = CHARACTER_SPRITE_HEIGHT * 8;
	constexpr int CHARACTER_JUMPING_RIGHT_Y_START_COORD = CHARACTER_SPRITE_HEIGHT * 10;
    /*    
    8,15
    70,15
    70,60
    61,103
    53,120
    25,120
    16,103
    8,60
    */
	const std::vector<sf::Vector2f> CHARACTER_HIT_BOX_POLYGON = {
		sf::Vector2f(8.f, 15.f),
		sf::Vector2f(70.f, 15.f),
		sf::Vector2f(70.f, 60.f),
		sf::Vector2f(61.f, 103.f),
		sf::Vector2f(53.f, 120.f),
		sf::Vector2f(25.f, 120.f),
		sf::Vector2f(16.f, 103.f),
		sf::Vector2f(8.f, 60.f)
	};
	constexpr float CHARACTER_MASS = 10.f;	//in kg

	namespace CharacterControls
	{
		enum ID
		{
			MOVE_RIGHT, MOVE_LEFT, JUMP, SPRINT, PICK_UP_ROCK, STAND_ON_ROCK, CONTROLS_COUNT
		};
	}
}

#endif //CHARACTER_ENTITY_IDENTIFIERS_HPP