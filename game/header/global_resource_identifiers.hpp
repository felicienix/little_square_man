#ifndef GLOBAL_RESOURCE_IDENTIFIERS_HPP
#define GLOBAL_RESOURCE_IDENTIFIERS_HPP
#include "resource_holder.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "physics_engine.hpp"
#include "entities_resources_and_identifiers.hpp"
#include "json.hpp"

#define WIDTH 1200
#define HEIGHT 800
/* #define TEXTURE_DIR "../resources/textures/"
#define FONT_DIR "../resources/fonts/"
#define SOUND_DIR "../resources/sounds/"
#define LEVEL_DIR "../resources/game_levels/" */
#define GAME_FPS 60.f



namespace MenuResources
{
	enum Textures
	{
		GAME_TITLE = GameResources::Textures::TEXTURES_COUNT, TITLE_CHARACTER, SQUARE_TEXTURE, LEVELS_MENU_BACKGROUND, CHECKPOINT_TEXTURE, TEXTURES_COUNT 
	};

	enum Fonts
	{
		TITLE_FONT, FONTS_COUNT
	};

	enum Sounds
	{
		SOUNDS_COUNT
	};

}

namespace LoadingScreenResources
{
	enum Textures
	{
		LOADING_BACKGROUND, TEXTURES_COUNT	
	};

	enum Fonts
	{
		FONTS_COUNT
	};

	enum Sounds
	{
		SOUNDS_COUNT
	};
}


typedef ResourceHolder<sf::Texture, unsigned int> TextureHolder;
typedef ResourceHolder<sf::Font, unsigned int> FontHolder;
typedef ResourceHolder<sf::SoundBuffer, unsigned int> SoundBufferHolder;

struct GameContext
{
	GameContext()
		:window(nullptr), textures(nullptr), fonts(nullptr), sounds(nullptr), engine(nullptr), stateStack(nullptr), levelLoaded(nullptr){}
	sf::RenderWindow* window;
	TextureHolder* textures;
	FontHolder* fonts;
	SoundBufferHolder* sounds;	
	PhysicsEngine* engine;
    class StateStack* stateStack;
	class GameLevel* levelLoaded;
};

#endif //GLOBAL_RESOURCE_IDENTIFIERS_HPP