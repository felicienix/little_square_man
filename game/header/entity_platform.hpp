#ifndef ENTITY_PLATFORM_HPP
#define ENTITY_PLATFORM_HPP
#include "entity.hpp"
#include "polygon.hpp"

class EntityPlatform : public Entity, public Polygon
{
public:
    EntityPlatform(GameContext _context);
    ~EntityPlatform() override;

    void update(sf::Time deltaTime) override;
    GameResources::EntityType getEntityType() const override;
    GameResources::RenderingLayers getEntityRenderingLayer() const override;
    void loadEntity(const nlohmann::json& jsonToLoadFrom) override;
    void saveEntity(nlohmann::json& jsonToSaveTo) override;

    class EntityCharacter* attachedCharacter = nullptr;

    std::unique_ptr<CollisionBox> getCollisionBox() override;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    void setupPhysicalPolygon();
    
    float speed = 50.f;
    float dir = 1.f;
    sf::Time movingDuration = sf::seconds(1.f);
    sf::Time movingAccu = sf::Time::Zero;
    sf::Time movingStartDuration;
    bool movingDirection = false;
    bool movingStart = false;

    sf::Sprite platformBeginning;
    sf::Sprite platformMiddle;
    sf::Sprite platformEnding;
};

#endif //ENTITY_PLATFORM_HPP