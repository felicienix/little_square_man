#ifndef FINISH_GAME_STATE_HPP
#define FINISH_GAME_STATE_HPP
#include "state.hpp"

namespace alphGUI
{
    class Menu;
}

class FinishGameState : public State
{
public:
    FinishGameState(GameContext _context);
    ~FinishGameState() override;
    
    bool handleEvent(const sf::Event& event) override;
    bool update(sf::Time deltaTime) override;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override; 

    void buildFinishMenu();

    std::unique_ptr<alphGUI::Menu> finishMenu;
    sf::Text finishTitle;
};


#endif //FINISH_GAME_STATE_HPP