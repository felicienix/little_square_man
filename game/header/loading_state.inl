#include "loading_state.hpp"

template<class StateToLoad, class... StateToLoadExtraArgs>
inline LoadingState<StateToLoad,StateToLoadExtraArgs...>::LoadingState(sf::RenderWindow & contextWindow, StateStack & stateStack, StateToLoadExtraArgs... args)
    :State(contextWindow, stateStack), stateToLoadExtraArgs(std::make_tuple(args...)), textures(), loadingBackground(), loadingBar(), loadingThreadMutex(), loadingThread(&LoadingState<StateToLoad, StateToLoadExtraArgs...>::loadingThreadFunc, this)
{
    textures.load(TEXTURE_DIR + std::string("loading_background.png"), LoadingScreenResources::LOADING_BACKGROUND);

    loadingBackground.setTexture(textures.get(LoadingScreenResources::LOADING_BACKGROUND), true);
    loadingBackground.setPosition(WIDTH * 0.5f - loadingBackground.getLocalBounds().width * 0.5f, HEIGHT * 0.5f - loadingBackground.getLocalBounds().height * 0.5f);
    //coord rect 64x80, wxh=334x11
    //rgb : 128,128,255
    loadingBar.setSize(sf::Vector2f(0.f,11.f));
    loadingBar.setPosition(loadingBackground.getPosition() + sf::Vector2f(64, 80));
    loadingBar.setFillColor(sf::Color(128, 128, 255, 255));

    loadingThread.launch();
}

template <class StateToLoad, class... StateToLoadExtraArgs>
inline LoadingState<StateToLoad, StateToLoadExtraArgs...>::~LoadingState()
{
}

template <class StateToLoad, class... StateToLoadExtraArgs>
inline bool LoadingState<StateToLoad, StateToLoadExtraArgs...>::handleEvent(const sf::Event &event)
{
    return false;
}

template <class StateToLoad, class... StateToLoadExtraArgs>
inline bool LoadingState<StateToLoad, StateToLoadExtraArgs...>::update(sf::Time deltaTime)
{
    loadingThreadMutex.lock();
    loadingBar.setSize(sf::Vector2f(progressAmount * barTotalProgressWidth, loadingBar.getSize().y));
    if(MathOperations::equalFloat(progressAmount, 1.f) && !changeRequestDone)
    {
        stateContext.stateStack->requestAction(StateStack::Actions::CHANGE_STATE, [this](){return stateToLoad;});
        changeRequestDone = true;
    }
    loadingThreadMutex.unlock();

    return false;
}

template <class StateToLoad, class... StateToLoadExtraArgs>
inline void LoadingState<StateToLoad, StateToLoadExtraArgs...>::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    target.draw(loadingBackground, states);
    target.draw(loadingBar, states);
}

template <class StateToLoad, class... StateToLoadExtraArgs>
inline void LoadingState<StateToLoad, StateToLoadExtraArgs...>::loadingThreadFunc()
{
    stateToLoad = new StateToLoad(*stateContext.window, *stateContext.stateStack, std::get<StateToLoadExtraArgs>(stateToLoadExtraArgs)...);

    stateToLoad->buildState(progressAmount, loadingThreadMutex);
}
