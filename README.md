# Little Square-Man Game and Editor

## Description

This project combines a game and a game editor for a little robot that goes through levels.
It uses a previous project of mine called [AlphEngine2D](https://gitlab.com/felicienix/falling_shapes)
which is a basic 2D physics engine.
If you just want to play the game / create levels via the game editor,
I suggest you go on my [itch.io](https://alphoenix.itch.io/little-square-man) page which has installers for both debian-based linux and windows systems. 

## Installation

### Requirements
If you want to compile the code in this project, you need to have my [AlphEngine2D](https://gitlab.com/felicienix/falling_shapes) installed
on your system as well as version 2.5 or higher of [SFML](https://www.sfml-dev.org) (which also needs to be installed for AlphEngine2D to work).
You also have to have cmake installed.


### Compiling with CMake

If you're on windows, you'll need to add sfml dlls and the AlphEngine2D dll in your target folder or in your Path environment variable.
Wether if you're on linux or windows, you'll need to modify the path to the cmake configuration files of sfml and AlphEngine2D.
For that to happen, you need to modify the 'SFML_PATH' variable (line 11 if you're on windows or line 13 if you're on linux)
as well as the variable 'AlphEngine2D_DIR' line 19.
Once you modified those variables to point to the directories where you installed sfml and AlphEngine2D, you're good to go.

To compile with cmake, just create a folder named `build`, go in there and type one of those commands:
```bash
cmake -DLSM_PROGRAM_TO_COMPILE="game" -DBUILD_LSM_PACKAGE=OFF .. ; cmake --build . #if you want to build the game
cmake -DLSM_PROGRAM_TO_COMPILE="editor" -DBUILD_LSM_PACKAGE=OFF .. ; cmake --build . #if you want to build the editor
cmake -DLSM_PROGRAM_TO_COMPILE="both" -DBUILD_LSM_PACKAGE=OFF .. ; cmake --build . --target little_square_man little_square_man_editor #if you want to build the game and the editor
```

The executable(s) will be in the same folder as this readme.

## Support
If you have any questions, feel free to email me at felicien.fgay@gmail.com

## Contributing
Don't hesitate to contribute by doing push requests if you want to improve this project !

## Authors and acknowledgment
Félicien Fiscus

## License
GNU General Public License (GPL) 2024

## Project status
In a pause state while I finish my master's semester.
