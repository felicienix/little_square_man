#include "deathblock_asset.hpp"
#include "../../game/header/entities_resources_and_identifiers.hpp"

DeathBlockAsset::DeathBlockAsset(EditorAppContext _context)
    :WallAsset(_context)
{
    sprite.setColor(sf::Color(125,255,255));
}

DeathBlockAsset::DeathBlockAsset(const DeathBlockAsset &otherAsset)
    :WallAsset(otherAsset)
{
    sprite.setColor(sf::Color(125,255,255));
}

DeathBlockAsset::~DeathBlockAsset()
{
}

EditorResources::AssetType DeathBlockAsset::getAssetType() const
{
    return EditorResources::AssetType::DEATH_BLOCK;
}

EditorAsset *DeathBlockAsset::cloneAsset() const
{
    return new DeathBlockAsset(*this);
}

void DeathBlockAsset::saveAssetToJSON(nlohmann::json &jsonToSaveAssetTo)
{
    WallAsset::saveAssetToJSON(jsonToSaveAssetTo);
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
}

void DeathBlockAsset::exportAssetToJSON(nlohmann::json &jsonToExportTo)
{
    WallAsset::exportAssetToJSON(jsonToExportTo);
    jsonToExportTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
}
