#ifndef CHECKPOINT_ASSET_HPP
#define CHECKPOINT_ASSET_HPP
#include "sprite_asset.hpp"

class CheckpointAsset : public SpriteAsset
{
public:
    enum class CheckpointType
    {
        START, MIDDLE_CHECKPOINT, END, TYPE_COUNT
    };
    CheckpointAsset(EditorAppContext _context, CheckpointType _checkpointType = CheckpointType::MIDDLE_CHECKPOINT);
    CheckpointAsset(const CheckpointAsset& otherAsset);
    ~CheckpointAsset() override;

    void handleEvent(const sf::Event& event) override;

    EditorResources::AssetType getAssetType() const override;
    void loadAssetFromJSON(const nlohmann::json& jsonToLoadAssetFrom) override;
    void saveAssetToJSON(nlohmann::json& jsonToSaveAssetTo) override;
    void exportAssetToJSON(nlohmann::json& jsonToExportTo) override;

    EditorAsset* cloneAsset() const override;

    alphGUI::Menu* generateAssetMenu() override;

    CheckpointType getCheckpointType() const;
private:
    void setTextureRectAccordingToType();

    CheckpointType checkpointType;
    std::unique_ptr<class TileAssetTool> tileCoordTool;
};

#endif //CHECKPOINT_ASSET_HPP