#include "tile_asset_tool.hpp"
#include "../viewmodel/editor_level.hpp"

TileAssetTool::TileAssetTool(EditorAppContext _context, sf::Vector2u _tileSize)
    :context(_context), tileSize(_tileSize)
{
}

TileAssetTool::TileAssetTool(const TileAssetTool &otherTool)
    :tileGridOriginPosition(otherTool.tileGridOriginPosition), context(otherTool.context), tileSize(otherTool.tileSize)
{
}

TileAssetTool::~TileAssetTool()
{
}

sf::Vector2i TileAssetTool::fromRealWorldToTilePosition(sf::Vector2f realWorldPosition)
{
    realWorldPosition -= tileGridOriginPosition;
    return sf::Vector2i((int)realWorldPosition.x / (int)tileSize.x, (int)realWorldPosition.y / (int)tileSize.y);
}

sf::Vector2f TileAssetTool::fromTileToRealWorldPosition(sf::Vector2i tilePosition)
{
    return sf::Vector2f((float)tilePosition.x * (int)tileSize.x + tileGridOriginPosition.x, (float)tilePosition.y * (int)tileSize.y + tileGridOriginPosition.y);
}

sf::Vector2f TileAssetTool::getTileRealWorldPositionFromMousePosition() const
{
    sf::Vector2f mousePosRealWorld;
    if(context.levelBeingEdited)
        mousePosRealWorld = context.levelBeingEdited->getMousePositionInLevel();
    else
        mousePosRealWorld = sf::Vector2f(sf::Mouse::getPosition(*context.window));
    
    mousePosRealWorld.x = ((int)(mousePosRealWorld.x) / (int)tileSize.x) * (int)tileSize.x;
    mousePosRealWorld.y = ((int)(mousePosRealWorld.y) / (int)tileSize.y) * (int)tileSize.y;
    return mousePosRealWorld;
}
