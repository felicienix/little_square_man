#ifndef PLATFORM_ASSET_HPP
#define PLATFORM_ASSET_HPP
#include "wall_asset.hpp"

class PlatformAsset : public WallAsset
{
public:
    PlatformAsset(EditorAppContext _context);
    PlatformAsset(const PlatformAsset& otherAsset);
    ~PlatformAsset() override;

    virtual sf::FloatRect getBoundingRectangle() const override;

    virtual EditorResources::AssetType getAssetType() const override;
    virtual void loadAssetFromJSON(const nlohmann::json& jsonToLoadAssetFrom) override;
    virtual void saveAssetToJSON(nlohmann::json& jsonToSaveAssetTo) override;
    virtual void exportAssetToJSON(nlohmann::json& jsonToExportTo) override;

    virtual EditorAsset* cloneAsset() const override;

    virtual alphGUI::Menu* generateAssetMenu() override;

private:
    virtual void expandWall(const sf::Vector2f& mousePositionInLevel) override;
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    unsigned int platformDistanceSpan = 3U;//the unit is in blocks of the wall asset
    double timeItTakesToDoAllTheDistance = 2.0; // in seconds
    double delayToStartMoving = 0.0; //in seconds

    sf::Sprite platformBeginning;
    sf::Sprite platformEnding;
};

#endif //PLATFORM_ASSET_HPP