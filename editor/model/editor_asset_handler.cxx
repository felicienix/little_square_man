#include "editor_asset_handler.hpp"
#include "../viewmodel/editor_level.hpp"

EditorAssetHandler::EditorAssetHandler(EditorAppContext _context)
    :context(_context), assets(), currentlySelectedAsset(nullptr)
{
}

EditorAssetHandler::~EditorAssetHandler()
{
    for(auto* asset : assets)
        delete asset;
}

void EditorAssetHandler::handleEvent(const sf::Event &event)
{
        
    switch (event.type)
    {
    case sf::Event::MouseButtonPressed:
        if(currentlySelectedAsset)
        {
            currentlySelectedAsset->isSelected = false;
            currentlySelectedAsset = nullptr;
        }
        for(auto* asset : assets)
        {
            if(asset->getBoundingRectangle().contains(context.levelBeingEdited->getMousePositionInLevel()))
            {
                currentlySelectedAsset = asset;
                currentlySelectedAsset->isSelected = true;
                break;
            }
        }
        break;
    case sf::Event::KeyPressed:
        keyPressedEvents(event.key.code);
        break;
    
    default:
        break;
    }

    if(currentlySelectedAsset)
        currentlySelectedAsset->handleEvent(event);
}

void EditorAssetHandler::addAsset(EditorAsset *asset)
{
    if(!asset)
        return;

    assets.push_front(asset);
    if(currentlySelectedAsset)
        currentlySelectedAsset->isSelected = false;
    currentlySelectedAsset = asset;
    asset->isSelected = true;
}

void EditorAssetHandler::removeAsset(EditorAsset *asset)
{
    assets.remove(asset);
    delete asset;
}

void EditorAssetHandler::clearAllAssets()
{
    for(auto* asset : assets)
        delete asset;
    
    assets.clear();
    currentlySelectedAsset = nullptr;
}

std::list<EditorAsset *> &EditorAssetHandler::getAssets()
{
    return assets;
}

EditorAsset* EditorAssetHandler::getCurrentlySelectedAsset() const
{
    return currentlySelectedAsset;
}

void EditorAssetHandler::keyPressedEvents(sf::Keyboard::Key keyPressed)
{
    switch (keyPressed)
    {
    case sf::Keyboard::C: //clone
        if(currentlySelectedAsset)
            addAsset(currentlySelectedAsset->cloneAsset());
        break;
    case sf::Keyboard::R:
        if(currentlySelectedAsset)
        {
            removeAsset(currentlySelectedAsset);
            currentlySelectedAsset = nullptr;
        }
        break;
    case sf::Keyboard::Escape:
        if(currentlySelectedAsset)
            currentlySelectedAsset = nullptr;
        break;
    
    default:
        break;
    }
}
