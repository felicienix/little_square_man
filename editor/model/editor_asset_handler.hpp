#ifndef EDITOR_ASSET_HANDLER_HPP
#define EDITOR_ASSET_HANDLER_HPP
#include "editor_asset.hpp"
#include "../viewmodel/resource_identifiers.hpp"
#include <list>


/// @brief controls the assets once they are created
class EditorAssetHandler
{
public:
    EditorAssetHandler(EditorAppContext _context);
    ~EditorAssetHandler();

    void handleEvent(const sf::Event& event);
    
    void addAsset(EditorAsset* asset);
    //deletes the asset as well
    void removeAsset(EditorAsset* asset);

    //deletes the assets as well
    void clearAllAssets();

    std::list<EditorAsset*>& getAssets();

    EditorAsset* getCurrentlySelectedAsset() const;
private:
    void keyPressedEvents(sf::Keyboard::Key keyPressed);

    EditorAppContext context;
    std::list<EditorAsset*> assets;
    EditorAsset* currentlySelectedAsset;
};

#endif //EDITOR_ASSET_HANDLER_HPP