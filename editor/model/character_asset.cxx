#include "character_asset.hpp"
#include "../../game/header/character_entity_identifiers.hpp"
#include "../viewmodel/load_and_save_manager.hpp"

CharacterAsset::CharacterAsset(EditorAppContext _context)
    :SpriteAsset(_context, EditorResources::Textures::MAIN_CHARACTER, 
        sf::IntRect(0,0, GameResources::CHARACTER_SPRITE_WIDTH, GameResources::CHARACTER_SPRITE_HEIGHT))
{
    setOrigin(GameResources::CHARACTER_SPRITE_WIDTH * 0.5f, GameResources::CHARACTER_SPRITE_HEIGHT * 0.5f);
}

CharacterAsset::~CharacterAsset()
{
}

EditorResources::AssetType CharacterAsset::getAssetType() const
{
    return EditorResources::AssetType::CHARACTER;
}

void CharacterAsset::loadAssetFromJSON(const nlohmann::json &jsonToLoadAssetFrom)
{
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadAssetFrom);
}

void CharacterAsset::saveAssetToJSON(nlohmann::json &jsonToSaveAssetTo)
{
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
    LoadAndSaveManager::saveTransformable(*this, jsonToSaveAssetTo);
}

void CharacterAsset::exportAssetToJSON(nlohmann::json &jsonToExportTo)
{
    jsonToExportTo[GameResources::JSON_ENTITY_TYPE_STRING] = GameResources::EntityType::CHARACTER;
    LoadAndSaveManager::saveTransformable(*this, jsonToExportTo);
}

EditorAsset *CharacterAsset::cloneAsset() const
{
    return nullptr;
}
