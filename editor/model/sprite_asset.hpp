#ifndef SPRITE_ASSET_HPP
#define SPRITE_ASSET_HPP
#include "editor_asset.hpp"

class SpriteAsset : public EditorAsset
{
public:
    // textureRect of 0,0,0,0 means we take the whole texture  size as texture rectangle
    SpriteAsset(EditorAppContext _context, EditorResources::Textures textureID, sf::IntRect textureRect = sf::IntRect(0,0,0,0));
    SpriteAsset(const SpriteAsset& spriteAsset);
    virtual ~SpriteAsset() override;

    virtual void handleEvent(const sf::Event& event) override;

    virtual EditorResources::AssetType getAssetType() const override;
    virtual void loadAssetFromJSON(const nlohmann::json& jsonToLoadAssetFrom) override;
    virtual void saveAssetToJSON(nlohmann::json& jsonToSaveAssetTo) override;
    virtual void exportAssetToJSON(nlohmann::json& jsonToExportTo) override;

    virtual EditorAsset* cloneAsset() const override;

    virtual sf::FloatRect getBoundingRectangle() const override;

protected:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    sf::Sprite sprite;
    bool selectSprite = false;
};

#endif //SPRITE_ASSET_HPP