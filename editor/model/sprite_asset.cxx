#include "sprite_asset.hpp"
#include "../../game/header/entities_resources_and_identifiers.hpp"
#include "../viewmodel/editor_level.hpp"
#include "../viewmodel/load_and_save_manager.hpp"

SpriteAsset::SpriteAsset(EditorAppContext _context, EditorResources::Textures textureID, sf::IntRect textureRect)
    :EditorAsset(_context), sprite(_context.textures->get(textureID), textureRect)
{
    if(textureRect == sf::IntRect(0,0,0,0))
        sprite.setTextureRect(sf::IntRect(sf::Vector2i(0,0),sf::Vector2i(sprite.getTexture()->getSize())));
}

SpriteAsset::SpriteAsset(const SpriteAsset &spriteAsset)
    :EditorAsset(spriteAsset.context), sprite(spriteAsset.sprite)
{
}

SpriteAsset::~SpriteAsset()
{
}

void SpriteAsset::handleEvent(const sf::Event &event)
{
    switch (event.type)
    {
    case sf::Event::MouseButtonPressed:
        selectSprite = !selectSprite;
        break;
    case sf::Event::MouseMoved:
        if(selectSprite)
            setPosition(context.levelBeingEdited->getMousePositionInLevel());
        break;
    
    default:
        break;
    }
}

EditorResources::AssetType SpriteAsset::getAssetType() const
{
    return EditorResources::AssetType::SPRITE_ASSET;
}

void SpriteAsset::loadAssetFromJSON(const nlohmann::json &jsonToLoadAssetFrom)
{
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadAssetFrom);
}

void SpriteAsset::saveAssetToJSON(nlohmann::json &jsonToSaveAssetTo)
{
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
    LoadAndSaveManager::saveTransformable(*this, jsonToSaveAssetTo);
}

void SpriteAsset::exportAssetToJSON(nlohmann::json &jsonToExportTo)
{
    jsonToExportTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)GameResources::EntityType::WALL_SQUARE_WITH_GRAVITY;
    LoadAndSaveManager::saveTransformable(*this, jsonToExportTo);
}

EditorAsset *SpriteAsset::cloneAsset() const
{
    EditorAsset* clone = new SpriteAsset(*this);
    clone->setPosition(getPosition());
    return clone;
}

sf::FloatRect SpriteAsset::getBoundingRectangle() const
{
    return getTransform().transformRect(sprite.getGlobalBounds());
}

void SpriteAsset::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();

    target.draw(sprite, states);
    if(isSelected)
    {
        sf::FloatRect boundingRect = getBoundingRectangle();
        sf::RectangleShape rect(sf::Vector2f(boundingRect.width, boundingRect.height));
        rect.setFillColor(sf::Color::Transparent);
        rect.setOutlineColor(sf::Color::Green);
        rect.setOutlineThickness(2.f);
        rect.setPosition(boundingRect.left, boundingRect.top);

        target.draw(rect);
    }
}
