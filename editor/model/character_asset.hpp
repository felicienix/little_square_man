#ifndef CHARACTER_ASSET_HPP
#define CHARACTER_ASSET_HPP
#include "sprite_asset.hpp"

class CharacterAsset : public SpriteAsset
{
public:
    CharacterAsset(EditorAppContext _context);
    ~CharacterAsset() override;

    EditorResources::AssetType getAssetType() const override;
    void loadAssetFromJSON(const nlohmann::json& jsonToLoadAssetFrom) override;
    void saveAssetToJSON(nlohmann::json& jsonToSaveAssetTo) override;
    void exportAssetToJSON(nlohmann::json& jsonToExportTo) override;

    EditorAsset* cloneAsset() const override;

};

#endif //CHARACTER_ASSET_HPP