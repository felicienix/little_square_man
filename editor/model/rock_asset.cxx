#include "rock_asset.hpp"
#include "../viewmodel/editor_level.hpp"
#include "../viewmodel/load_and_save_manager.hpp"
#include "../../game/header/random_functions.hpp"
#include "maths_operations.hpp"

RockAsset::RockAsset(EditorAppContext _context)
    :EditorAsset(_context), rockShape()
{
    generateConvexShape();
    rockShape.setFillColor(sf::Color::Blue);
    rockShape.setOutlineThickness(1.f);
    rockShape.setOutlineColor(sf::Color(255,0,255));
}

RockAsset::RockAsset(const RockAsset &otherRock)
    :EditorAsset(otherRock.context), rockShape(otherRock.rockShape)
{
}

RockAsset::~RockAsset()
{
}

void RockAsset::handleEvent(const sf::Event &event)
{
    static bool selectSprite = false;
    switch (event.type)
    {
    case sf::Event::MouseButtonPressed:
        selectSprite = !selectSprite;
        break;
    case sf::Event::MouseMoved:
        if(selectSprite)
            setPosition(context.levelBeingEdited->getMousePositionInLevel());
        break;
    
    case sf::Event::KeyPressed:
        if(event.key.code == sf::Keyboard::Up)
            rotate(5.f);
        else if(event.key.code == sf::Keyboard::Down)
            rotate(-5.f);
        break;

    default:
        break;
    }
}

EditorResources::AssetType RockAsset::getAssetType() const
{
    return EditorResources::AssetType::ROCK_ASSET;
}

void RockAsset::loadAssetFromJSON(const nlohmann::json &jsonToLoadAssetFrom)
{
    rockShape.setPointCount(jsonToLoadAssetFrom[GameResources::JSON_ENTITY_ROCK_POINT_COUNT_STRING]);
    for(size_t i = 0;i<rockShape.getPointCount();++i)
    {
        rockShape.setPoint(i, 
            sf::Vector2f(jsonToLoadAssetFrom[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING][i][0],
                        jsonToLoadAssetFrom[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING][i][1]));
    }
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadAssetFrom);
}

void RockAsset::saveAssetToJSON(nlohmann::json &jsonToSaveAssetTo)
{
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
    LoadAndSaveManager::saveTransformable(*this, jsonToSaveAssetTo);
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_ROCK_POINT_COUNT_STRING] = rockShape.getPointCount();
    if(jsonToSaveAssetTo[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING].size() > 0)
        jsonToSaveAssetTo[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING].clear();
    for(size_t p = 0;p<rockShape.getPointCount();++p)
        jsonToSaveAssetTo[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING].push_back({rockShape.getPoint(p).x, rockShape.getPoint(p).y});
}

void RockAsset::exportAssetToJSON(nlohmann::json &jsonToExportTo)
{
    jsonToExportTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)GameResources::EntityType::ROCK;
    LoadAndSaveManager::saveTransformable(*this, jsonToExportTo);
    jsonToExportTo[GameResources::JSON_ENTITY_ROCK_POINT_COUNT_STRING] = rockShape.getPointCount();
    if(jsonToExportTo[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING].size() > 0)
        jsonToExportTo[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING].clear();
    for(size_t p = 0;p<rockShape.getPointCount();++p)
        jsonToExportTo[GameResources::JSON_ENTITY_ROCK_POINTS_ARRAY_STRING].push_back({rockShape.getPoint(p).x, rockShape.getPoint(p).y});
}

EditorAsset *RockAsset::cloneAsset() const
{
    return new RockAsset(*this);
}

sf::FloatRect RockAsset::getBoundingRectangle() const
{
    return getTransform().transformRect(rockShape.getGlobalBounds());
}

void RockAsset::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    states.transform *= getTransform();
    target.draw(rockShape, states);
}

bool doNotIntersect(const sf::Vector2f& edge_beginPoint, const sf::Vector2f& edge_endPoint, const std::vector<sf::Vector2f>& points)
{
    sf::Vector2f edge = edge_endPoint - edge_beginPoint;
    for(unsigned int i = 0;i<points.size() - 1;++i)
    {
        sf::Vector2f second_edge = points[i+1] - points[i];
        float cp1 = MathOperations::crossProduct2D(points[i+1] - edge_beginPoint, edge)
                * MathOperations::crossProduct2D(points[i] - edge_beginPoint, edge);
        float cp2 = MathOperations::crossProduct2D(edge_endPoint - points[i], second_edge)
                * MathOperations::crossProduct2D(edge_beginPoint - points[i], second_edge);
        if(cp1 < 0.f && cp2 < 0.f)
            return false;
    }

    return true;
}

bool checkIfLastPointDoesntKeepConvexProperty(const sf::ConvexShape& rockShape, 
                                          const sf::Vector2f& lastPoint, 
                                          const std::vector<sf::Vector2f>& newShapePoints,
                                          size_t p)
{
    bool isLastAndSecondToLastIntersectingWithShape = 
        !doNotIntersect(rockShape.getPoint(p - 1), lastPoint, newShapePoints);
    bool isLastAndFirstPointIntersectingWithShape = 
        !doNotIntersect(lastPoint, rockShape.getPoint(0), newShapePoints);

    bool isInteriorAngleTooWideWithFirstPoint = 
        MathOperations::crossProduct2D(lastPoint - rockShape.getPoint(0),
                                       rockShape.getPoint(1) - rockShape.getPoint(0))
        > 0.f;

    bool isInteriorAngleTooWideWithSecondToLastPoint = 
        MathOperations::crossProduct2D(rockShape.getPoint(0) - lastPoint,
                                       rockShape.getPoint(p -1) - lastPoint)
        < 0.f;
    
    return isLastAndSecondToLastIntersectingWithShape
        || isLastAndFirstPointIntersectingWithShape 
        || isInteriorAngleTooWideWithFirstPoint 
        || isInteriorAngleTooWideWithSecondToLastPoint;
}

#define MAX_CONVEX_SHAPE_ATTEMPTS 50
#define EDGE_WIDTH_MIN 20.f
#define EDGE_WIDTH_MAX 50.f

void RockAsset::generateConvexShape()
{
    // in case of a snail effect, no matter where the last point is set, the shape will not be convex,
    //so it will loop through it infinitely, hence the maximum numer of attempts to redo the shape entirely
    size_t attempts = 0;
    do
    {
        float edgeWidth = lsmRandom::randomFloat(EDGE_WIDTH_MIN, EDGE_WIDTH_MAX);
        size_t numberOfPoints = (size_t)lsmRandom::randomInt(3, 10);
        sf::Vector2f firstPoint;
        sf::Vector2f secondPoint = firstPoint + sf::Vector2f(edgeWidth, 0.f);
        std::vector<sf::Vector2f> newShapePoints = {firstPoint, secondPoint};
        rockShape.setPointCount(numberOfPoints);
        rockShape.setPoint(0, firstPoint);
        rockShape.setPoint(1, secondPoint);

        auto rotatePointAround = [](const sf::Vector2f& pointToRotate, const sf::Vector2f& originRotation, float angleRadians)
        {
            sf::Vector2f rotatedVector = pointToRotate - originRotation;
            MathOperations::rotateVector(rotatedVector, angleRadians);
            return originRotation + rotatedVector;
        };

        //all the points except the last one
        for(size_t p = 2;p < numberOfPoints - 1;++p)
        {
            sf::Vector2f newPoint;
            do
            {
                edgeWidth = lsmRandom::randomFloat(EDGE_WIDTH_MIN, EDGE_WIDTH_MAX);
                sf::Vector2f previousEdgeNormalized =
                    MathOperations::normalize(rockShape.getPoint(p - 1) - rockShape.getPoint(p-2));
                newPoint = rockShape.getPoint(p - 1) + edgeWidth * previousEdgeNormalized;
                newPoint = rotatePointAround(newPoint, rockShape.getPoint(p-1), lsmRandom::randomFloat(M_PI/12.f, M_PI_2));
                
            } while (!doNotIntersect(rockShape.getPoint(p - 1), newPoint, newShapePoints));
            
            rockShape.setPoint(p, newPoint);
            newShapePoints.push_back(newPoint);
        }

        sf::Vector2f lastPoint;
        attempts = 0;
        do
        {
            edgeWidth = lsmRandom::randomFloat(EDGE_WIDTH_MIN, EDGE_WIDTH_MAX);
            sf::Vector2f previousEdgeNormalized =
                MathOperations::normalize(rockShape.getPoint(numberOfPoints - 2) - rockShape.getPoint(numberOfPoints - 3));
            lastPoint = rockShape.getPoint(numberOfPoints - 2) + edgeWidth * previousEdgeNormalized;
            lastPoint = rotatePointAround(lastPoint, rockShape.getPoint(numberOfPoints-2), lsmRandom::randomFloat(M_PI/12.f, M_PI_2));
            attempts++;
        } while (checkIfLastPointDoesntKeepConvexProperty(rockShape, lastPoint, newShapePoints, numberOfPoints - 1) && attempts <= MAX_CONVEX_SHAPE_ATTEMPTS);
        
        rockShape.setPoint(numberOfPoints - 1, lastPoint);
    } while(attempts > MAX_CONVEX_SHAPE_ATTEMPTS);
}
