#include "platform_asset.hpp"
#include "gui_menu.hpp"
#include "gui_value_button.hpp"
#include "../../game/header/entities_resources_and_identifiers.hpp"

PlatformAsset::PlatformAsset(EditorAppContext _context)
    :WallAsset(_context), platformBeginning(_context.textures->get(EditorResources::Textures::PLATFORM_TEXTURE), 
                                            sf::IntRect(0,0,EditorResources::SQUARE_SIZE, EditorResources::SQUARE_SIZE)), 
    platformEnding(_context.textures->get(EditorResources::Textures::PLATFORM_TEXTURE),
                    sf::IntRect(EditorResources::SQUARE_SIZE, 0, EditorResources::SQUARE_SIZE,EditorResources::SQUARE_SIZE))
{
    sprite.setTexture(_context.textures->get(EditorResources::Textures::PLATFORM_TEXTURE));
    sprite.setTextureRect(sf::IntRect(2 * EditorResources::SQUARE_SIZE, 0, EditorResources::SQUARE_SIZE, EditorResources::SQUARE_SIZE));
    sprite.setPosition(EditorResources::SQUARE_SIZE, 0.f);
    platformEnding.setPosition(2 * EditorResources::SQUARE_SIZE, 0.f);
}

PlatformAsset::PlatformAsset(const PlatformAsset &otherAsset)
    :WallAsset(otherAsset),
     platformDistanceSpan(otherAsset.platformDistanceSpan),
     timeItTakesToDoAllTheDistance(otherAsset.timeItTakesToDoAllTheDistance),
     delayToStartMoving(otherAsset.delayToStartMoving),
     platformBeginning(otherAsset.platformBeginning),
     platformEnding(otherAsset.platformEnding)
{
    sprite = otherAsset.sprite;
}

PlatformAsset::~PlatformAsset()
{
}

sf::FloatRect PlatformAsset::getBoundingRectangle() const
{
    return sf::FloatRect(getPosition(), sf::Vector2f(platformEnding.getPosition().x + EditorResources::SQUARE_SIZE, EditorResources::SQUARE_SIZE));
}

EditorResources::AssetType PlatformAsset::getAssetType() const
{
    return EditorResources::AssetType::PLATFORM_ASSET;
}

void PlatformAsset::loadAssetFromJSON(const nlohmann::json &jsonToLoadAssetFrom)
{
    WallAsset::loadAssetFromJSON(jsonToLoadAssetFrom);
    platformDistanceSpan = jsonToLoadAssetFrom[GameResources::JSON_ENTITY_PLATFORM_MOVING_DISTANCE];
    timeItTakesToDoAllTheDistance = jsonToLoadAssetFrom[GameResources::JSON_ENTITY_PLATFORM_MOVING_DURATION];
    delayToStartMoving = jsonToLoadAssetFrom[GameResources::JSON_ENTITY_PLATFORM_MOVING_START_DELAY];
    
    int middlePlatformSpriteWidth = std::max(0, (int)jsonToLoadAssetFrom[GameResources::JSON_ENTITY_WALL_SIZE_STRING][0] - 2 * (int)EditorResources::SQUARE_SIZE);
    sprite.setScale(middlePlatformSpriteWidth / EditorResources::SQUARE_SIZE, 1.f);

    platformEnding.setPosition(EditorResources::SQUARE_SIZE + middlePlatformSpriteWidth, 0.f);
    sprite.setTextureRect(sf::IntRect(sprite.getTextureRect().left, sprite.getTextureRect().top, EditorResources::SQUARE_SIZE, EditorResources::SQUARE_SIZE));
}

void PlatformAsset::saveAssetToJSON(nlohmann::json &jsonToSaveAssetTo)
{
    WallAsset::saveAssetToJSON(jsonToSaveAssetTo);
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_PLATFORM_MOVING_DISTANCE] = platformDistanceSpan;
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_PLATFORM_MOVING_DURATION] = timeItTakesToDoAllTheDistance;
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_PLATFORM_MOVING_START_DELAY] = delayToStartMoving;
}

void PlatformAsset::exportAssetToJSON(nlohmann::json &jsonToExportTo)
{
    WallAsset::exportAssetToJSON(jsonToExportTo);
    jsonToExportTo[GameResources::JSON_ENTITY_POSITION_STRING] = {getPosition().x + (sprite.getGlobalBounds().width 
                                                                                    + platformBeginning.getGlobalBounds().width
                                                                                    + platformEnding.getGlobalBounds().width) * 0.5f, 
                                                                getPosition().y + sprite.getGlobalBounds().height * 0.5f};
    jsonToExportTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
    jsonToExportTo[GameResources::JSON_ENTITY_PLATFORM_MOVING_DISTANCE] = platformDistanceSpan * EditorResources::SQUARE_SIZE;
    jsonToExportTo[GameResources::JSON_ENTITY_PLATFORM_MOVING_DURATION] = timeItTakesToDoAllTheDistance;
    jsonToExportTo[GameResources::JSON_ENTITY_PLATFORM_MOVING_START_DELAY] = delayToStartMoving;
}

EditorAsset *PlatformAsset::cloneAsset() const
{
    return new PlatformAsset(*this);
}

alphGUI::Menu *PlatformAsset::generateAssetMenu()
{
    alphGUI::Menu* platformMenu = new alphGUI::Menu();

    alphGUI::ValueButton<unsigned int>* platformDistanceSpanButton = new alphGUI::ValueButton<unsigned int>(platformDistanceSpan, 1U);
    platformDistanceSpanButton->setBeforeAndAfterValueText("distance: ", "b");
    platformMenu->addComponent(platformDistanceSpanButton);

    alphGUI::ValueButton<double>* timeToDoAllTheDistanceButton = new alphGUI::ValueButton<double>(timeItTakesToDoAllTheDistance, 0.1);
    timeToDoAllTheDistanceButton->setBeforeAndAfterValueText("delay: ", "s");
    platformMenu->addComponent(timeToDoAllTheDistanceButton);

    alphGUI::ValueButton<double>* movingStartDelayButton = new alphGUI::ValueButton<double>(delayToStartMoving, 0.1);
    movingStartDelayButton->setBeforeAndAfterValueText("start delay:", "s");
    platformMenu->addComponent(movingStartDelayButton);

    return platformMenu;
}

void PlatformAsset::expandWall(const sf::Vector2f &mousePositionInLevel)
{
    WallAsset::expandWall(mousePositionInLevel);

    int middlePlatformSpriteWidth = std::max(0, sprite.getTextureRect().width - 2 * (int)EditorResources::SQUARE_SIZE);
    sprite.setScale(middlePlatformSpriteWidth / EditorResources::SQUARE_SIZE, 1.f);

    platformEnding.setPosition(EditorResources::SQUARE_SIZE + middlePlatformSpriteWidth, 0.f);
    sprite.setTextureRect(sf::IntRect(sprite.getTextureRect().left, sprite.getTextureRect().top, EditorResources::SQUARE_SIZE, EditorResources::SQUARE_SIZE));
}

void PlatformAsset::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    WallAsset::draw(target, states);

    states.transform *= getTransform();
    target.draw(platformBeginning, states);
    target.draw(platformEnding, states);

    sf::RectangleShape distanceSpanShadow(sf::Vector2f(platformDistanceSpan * EditorResources::SQUARE_SIZE, EditorResources::SQUARE_SIZE));
    distanceSpanShadow.setFillColor(sf::Color(0,0,0,50));
    distanceSpanShadow.setPosition(getBoundingRectangle().width, 0.f);

    target.draw(distanceSpanShadow, states);
}
