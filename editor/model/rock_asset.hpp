#ifndef ROCK_ASSET_HPP
#define ROCK_ASSET_HPP
#include "editor_asset.hpp"

class RockAsset : public EditorAsset
{
public:
    RockAsset(EditorAppContext _context);
    RockAsset(const RockAsset& otherRock);
    ~RockAsset() override;

    void handleEvent(const sf::Event& event) override;

    EditorResources::AssetType getAssetType() const override;
    void loadAssetFromJSON(const nlohmann::json& jsonToLoadAssetFrom) override;
    void saveAssetToJSON(nlohmann::json& jsonToSaveAssetTo) override;
    void exportAssetToJSON(nlohmann::json& jsonToExportTo) override;

    EditorAsset* cloneAsset() const override;

    sf::FloatRect getBoundingRectangle() const override;
private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

    void generateConvexShape();
    sf::ConvexShape rockShape;
};


#endif //ROCK_ASSET_HPP