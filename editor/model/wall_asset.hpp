#ifndef WALL_ASSET_HPP
#define WALL_ASSET_HPP
#include "sprite_asset.hpp"


class WallAsset : public SpriteAsset
{
public:
    enum InteractionState
    {
        INTERACTION_NONE = 0, MOVE_WALL, EXPAND_WALL, INTERACTION_STATE_COUNT
    };
    WallAsset(EditorAppContext _context);
    WallAsset(const WallAsset& otherAsset);
    virtual ~WallAsset() override;

    virtual void handleEvent(const sf::Event& event) override;

    virtual EditorResources::AssetType getAssetType() const override;
    virtual void loadAssetFromJSON(const nlohmann::json& jsonToLoadAssetFrom) override;
    virtual void saveAssetToJSON(nlohmann::json& jsonToSaveAssetTo) override;
    virtual void exportAssetToJSON(nlohmann::json& jsonToExportTo) override;

    virtual EditorAsset* cloneAsset() const override;

protected:
    virtual void expandWall(const sf::Vector2f& mousePositionInLevel);

    std::unique_ptr<class TileAssetTool> tileCoordTool;
    InteractionState stateOfInteraction;
};

#endif //WALL_ASSET_HPP