#include "wall_asset.hpp"
#include "tile_asset_tool.hpp"
#include "../viewmodel/editor_level.hpp"
#include "../viewmodel/load_and_save_manager.hpp"

/* namespace
{
    std::ostream& operator<<(std::ostream& os, const sf::Vector2f& v)
    {
        return os<<v.x<<" "<<v.y;
    }
} */

WallAsset::WallAsset(EditorAppContext _context)
    :SpriteAsset(_context, EditorResources::Textures::SQUARE_TEXTURE, sf::IntRect(0,0,EditorResources::SQUARE_SIZE,EditorResources::SQUARE_SIZE)), 
        tileCoordTool(std::make_unique<TileAssetTool>(_context)),
        stateOfInteraction(InteractionState::INTERACTION_NONE)
{
    context.textures->get(EditorResources::Textures::SQUARE_TEXTURE).setRepeated(true);
}

WallAsset::WallAsset(const WallAsset &otherAsset)
    :SpriteAsset(otherAsset.context, EditorResources::Textures::SQUARE_TEXTURE, otherAsset.sprite.getTextureRect()),
        tileCoordTool(std::make_unique<TileAssetTool>(*(otherAsset.tileCoordTool.get()))),
        stateOfInteraction(otherAsset.stateOfInteraction)
{
}

WallAsset::~WallAsset()
{
}

void WallAsset::handleEvent(const sf::Event &event)
{
    switch (event.type)
    {
    case sf::Event::MouseButtonPressed:
        switch (stateOfInteraction)
        {
        case InteractionState::EXPAND_WALL:
        case InteractionState::MOVE_WALL:
            stateOfInteraction = InteractionState::INTERACTION_NONE;
            break;
        case InteractionState::INTERACTION_NONE:
            stateOfInteraction = InteractionState::MOVE_WALL;
            break;
        default: break;
        }
    case sf::Event::MouseMoved:
        if(stateOfInteraction == MOVE_WALL)
            setPosition(tileCoordTool->getTileRealWorldPositionFromMousePosition());
        else if(stateOfInteraction == EXPAND_WALL)
            expandWall(context.levelBeingEdited->getMousePositionInLevel());
        break;
    case sf::Event::KeyPressed:
        if(event.key.code == sf::Keyboard::E)
            stateOfInteraction = EXPAND_WALL;
        break;
    default:
        break;
    }
}

EditorResources::AssetType WallAsset::getAssetType() const
{
    return EditorResources::AssetType::WALL_ASSET;
}

void WallAsset::loadAssetFromJSON(const nlohmann::json &jsonToLoadAssetFrom)
{
    sprite.setTextureRect(sf::IntRect(sprite.getTextureRect().left,sprite.getTextureRect().top, jsonToLoadAssetFrom[GameResources::JSON_ENTITY_WALL_SIZE_STRING][0],
                               jsonToLoadAssetFrom[GameResources::JSON_ENTITY_WALL_SIZE_STRING][1]));
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadAssetFrom);

}

void WallAsset::saveAssetToJSON(nlohmann::json &jsonToSaveAssetTo)
{
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_WALL_SIZE_STRING] = {getBoundingRectangle().width, getBoundingRectangle().height};
    LoadAndSaveManager::saveTransformable(*this, jsonToSaveAssetTo);
}

void WallAsset::exportAssetToJSON(nlohmann::json &jsonToExportTo)
{
    jsonToExportTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)GameResources::EntityType::WALL;
    jsonToExportTo[GameResources::JSON_ENTITY_POSITION_STRING] = {getPosition().x + sprite.getGlobalBounds().width * 0.5f, 
                                                                getPosition().y + sprite.getGlobalBounds().height * 0.5f};
    jsonToExportTo[GameResources::JSON_ENTITY_ROTATION_STRING] = getRotation();
    jsonToExportTo[GameResources::JSON_ENTITY_SCALE_STRING] = {getScale().x, getScale().y};
    jsonToExportTo[GameResources::JSON_ENTITY_WALL_SIZE_STRING] = {getBoundingRectangle().width, getBoundingRectangle().height};
}

EditorAsset *WallAsset::cloneAsset() const
{
    EditorAsset* clone = new WallAsset(*this);
    clone->setPosition(getPosition());
    return clone;
}

void WallAsset::expandWall(const sf::Vector2f& mousePositionInLevel)
{
    sf::Vector2i tileGridCoordDifferenceAmount = tileCoordTool->fromRealWorldToTilePosition(mousePositionInLevel) 
                                                - tileCoordTool->fromRealWorldToTilePosition(getPosition());
    if(tileGridCoordDifferenceAmount.x < 0)
        setPosition(tileCoordTool->getTileRealWorldPositionFromMousePosition().x, getPosition().y);
    if(tileGridCoordDifferenceAmount.y < 0)
        setPosition(getPosition().x, tileCoordTool->getTileRealWorldPositionFromMousePosition().y);

    tileGridCoordDifferenceAmount.x = abs(tileGridCoordDifferenceAmount.x);
    tileGridCoordDifferenceAmount.y = abs(tileGridCoordDifferenceAmount.y);

    sprite.setTextureRect(sf::IntRect(sprite.getTextureRect().left, sprite.getTextureRect().top, (tileGridCoordDifferenceAmount.x + 1) * tileCoordTool->tileSize.x,
        (tileGridCoordDifferenceAmount.y + 1) * tileCoordTool->tileSize.y));
}
