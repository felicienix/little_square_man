#ifndef DEATH_BLOCK_ASSET_HPP
#define DEATH_BLOCK_ASSET_HPP
#include "wall_asset.hpp"

class DeathBlockAsset : public WallAsset
{
public:
    DeathBlockAsset(EditorAppContext _context);
    DeathBlockAsset(const DeathBlockAsset& otherAsset);
    ~DeathBlockAsset() override;

    EditorResources::AssetType getAssetType() const override;
    
    EditorAsset* cloneAsset() const override;
    void saveAssetToJSON(nlohmann::json& jsonToSaveAssetTo) override;
    void exportAssetToJSON(nlohmann::json& jsonToExportTo) override;

};

#endif //DEATH_BLOCK_ASSET_HPP