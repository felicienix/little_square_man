#ifndef EDITOR_ASSET_HPP
#define EDITOR_ASSET_HPP
#include "../viewmodel/resource_identifiers.hpp"

namespace alphGUI
{
    class Menu;
}

class EditorAsset : public sf::Drawable, public sf::Transformable
{
public:

    EditorAsset(EditorAppContext _context) :context(_context){};
    virtual ~EditorAsset(){};
    virtual void handleEvent(const sf::Event& event) = 0;

    virtual EditorResources::AssetType getAssetType() const = 0;
    virtual void loadAssetFromJSON(const nlohmann::json& jsonToLoadAssetFrom) = 0;
    virtual void saveAssetToJSON(nlohmann::json& jsonToSaveAssetTo) = 0;
    virtual void exportAssetToJSON(nlohmann::json& jsonToExportTo) = 0;

    virtual EditorAsset* cloneAsset() const = 0;

    virtual sf::FloatRect getBoundingRectangle() const = 0;

    virtual alphGUI::Menu* generateAssetMenu(){return nullptr;};

    bool isSelected = false;
protected:
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;

    EditorAppContext context;
};

#endif //EDITOR_ASSET_HPP