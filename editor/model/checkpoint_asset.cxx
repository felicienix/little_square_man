#include "checkpoint_asset.hpp"
#include "gui_menu.hpp"
#include "gui_button.hpp"
#include "tile_asset_tool.hpp"
#include "../viewmodel/load_and_save_manager.hpp"

CheckpointAsset::CheckpointAsset(EditorAppContext _context, CheckpointType _checkpointType)
    :SpriteAsset(_context, EditorResources::Textures::CHECKPOINT_TEXTURE, sf::IntRect(0,0,EditorResources::SQUARE_SIZE, EditorResources::CHECKPOINT_HEIGHT))
    , checkpointType(_checkpointType), tileCoordTool(std::make_unique<TileAssetTool>(_context, sf::Vector2u(EditorResources::SQUARE_SIZE, EditorResources::CHECKPOINT_HEIGHT)))
{
    setTextureRectAccordingToType();
}

CheckpointAsset::CheckpointAsset(const CheckpointAsset &otherAsset)
    :SpriteAsset(otherAsset), checkpointType(otherAsset.checkpointType)
{
    setTextureRectAccordingToType();
}

CheckpointAsset::~CheckpointAsset()
{
}

void CheckpointAsset::handleEvent(const sf::Event &event)
{
    switch (event.type)
    {
    case sf::Event::MouseButtonPressed:
        selectSprite = ! selectSprite;
        break;
    case sf::Event::MouseMoved:
        if(selectSprite)
        {
            setPosition(tileCoordTool->getTileRealWorldPositionFromMousePosition());
        }
    default:
        break;
    }
}

EditorResources::AssetType CheckpointAsset::getAssetType() const
{
    return EditorResources::AssetType::CHECKPOINT_ASSET;
}

void CheckpointAsset::loadAssetFromJSON(const nlohmann::json &jsonToLoadAssetFrom)
{
    LoadAndSaveManager::loadTransformable(*this, jsonToLoadAssetFrom);
    checkpointType = static_cast<CheckpointType>(jsonToLoadAssetFrom[GameResources::JSON_ENTITY_CHECKPOINT_TYPE_STRING]);

    setTextureRectAccordingToType();
}

void CheckpointAsset::saveAssetToJSON(nlohmann::json &jsonToSaveAssetTo)
{
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_TYPE_STRING] = (int)getAssetType();
    jsonToSaveAssetTo[GameResources::JSON_ENTITY_CHECKPOINT_TYPE_STRING] = static_cast<unsigned int>(checkpointType);
    LoadAndSaveManager::saveTransformable(*this, jsonToSaveAssetTo);
}

void CheckpointAsset::exportAssetToJSON(nlohmann::json &jsonToExportTo)
{
    jsonToExportTo[GameResources::JSON_ENTITY_TYPE_STRING] = GameResources::EntityType::CHECKPOINT;
    jsonToExportTo[GameResources::JSON_ENTITY_CHECKPOINT_TYPE_STRING] = static_cast<unsigned int>(checkpointType);
    LoadAndSaveManager::saveTransformable(*this, jsonToExportTo);
}

EditorAsset *CheckpointAsset::cloneAsset() const
{
    return new CheckpointAsset(*this);
}

std::string checkpointTypeToString(CheckpointAsset::CheckpointType type)
{
    switch (type)
    {
    case CheckpointAsset::CheckpointType::START:
        return "start";
        break;
    case CheckpointAsset::CheckpointType::MIDDLE_CHECKPOINT:
        return "middle";
        break;
    case CheckpointAsset::CheckpointType::END:
        return "end";
        break;
    
    default:
        break;
    }

    return std::string();
}

alphGUI::Menu *CheckpointAsset::generateAssetMenu()
{
    alphGUI::Menu* checkpointAssetMenu = new alphGUI::Menu();


    alphGUI::Button* checkpointTypeButton = new alphGUI::Button("type: " + checkpointTypeToString(checkpointType));
    checkpointTypeButton->clickFunctionCallback = [this, checkpointTypeButton]()
    {
        checkpointType = (CheckpointType)(((int)checkpointType + 1)%(int)CheckpointType::TYPE_COUNT);
        checkpointTypeButton->setTextInButton("type: " + checkpointTypeToString(checkpointType));
        setTextureRectAccordingToType();
    };

    checkpointAssetMenu->addComponent(checkpointTypeButton);

    return checkpointAssetMenu;
}

CheckpointAsset::CheckpointType CheckpointAsset::getCheckpointType() const
{
    return checkpointType;
}

void CheckpointAsset::setTextureRectAccordingToType()
{
    sf::IntRect checkpointStartTextureRect = sf::IntRect(0,0, EditorResources::SQUARE_SIZE, EditorResources::CHECKPOINT_HEIGHT);
    switch (checkpointType)
    {
    case CheckpointType::MIDDLE_CHECKPOINT:
        checkpointStartTextureRect.left = EditorResources::SQUARE_SIZE;
        break;
    case CheckpointType::END:
        checkpointStartTextureRect.left = 2U * EditorResources::SQUARE_SIZE;
        break;
    
    default:
        break;
    }

    sprite.setTextureRect(checkpointStartTextureRect);
}
