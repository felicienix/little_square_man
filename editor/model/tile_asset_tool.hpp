#ifndef TILE_ASSET_TOOL_HPP
#define TILE_ASSET_TOOL_HPP
#include "../viewmodel/resource_identifiers.hpp"

class TileAssetTool
{
public:
    TileAssetTool(EditorAppContext _context, sf::Vector2u _tileSize = sf::Vector2u(EditorResources::SQUARE_SIZE, EditorResources::SQUARE_SIZE));
    TileAssetTool(const TileAssetTool& otherTool);
    ~TileAssetTool();

    sf::Vector2i fromRealWorldToTilePosition(sf::Vector2f realWorldPosition);
    sf::Vector2f fromTileToRealWorldPosition(sf::Vector2i tilePosition);

    sf::Vector2f getTileRealWorldPositionFromMousePosition() const;

    sf::Vector2f tileGridOriginPosition = sf::Vector2f(0.f,0.f);
private:
    EditorAppContext context;
public:
    sf::Vector2u tileSize;
};

#endif //TILE_ASSET_TOOL_HPP