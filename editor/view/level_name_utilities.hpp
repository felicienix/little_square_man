#ifndef LEVEL_NAME_UTILITIES_HPP
#define LEVEL_NAME_UTILITIES_HPP
#include "SFML/System/String.hpp"

class LevelNameUtilities
{
public:
    static bool checkIfNameIsOfficialLevel(const sf::String& name)
    {
        bool hasNumberAtTheEnd = false;
        std::string nameWithoutNumber = "";
        try
        {
            (void)std::stoi(name.substring(6).toAnsiString());
            nameWithoutNumber = name.substring(0,6).toAnsiString();
            hasNumberAtTheEnd = true;
        }
        catch(const std::exception&)
        {
            hasNumberAtTheEnd = false;
        }
        
        return nameWithoutNumber == "level " 
            && hasNumberAtTheEnd;
    }


};




#endif //LEVEL_NAME_UTILITIES_HPP