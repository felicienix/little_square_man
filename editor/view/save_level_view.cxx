#include "save_level_view.hpp"
#define SAVE_WINDOW_WIDTH 800
#define SAVE_WINDOW_HEIGHT 600
#include <fstream>
#include "level_name_utilities.hpp"

#define unicode_UpperCase_letter_start 0x00000041
#define unicode_LowerCase_letter_start 0x00000061
#define unicode_number_start 0x00000030
#define alphabetSize 26
#define spaceCharacter 0x00000020

namespace
{
    //checks if the character is either '_' or a lower case letter [a-z]
    //or an upper case letter [A-Z] or a number [0-9]
    bool isValidCharacter(sf::Uint32 unicodeChar)
    {
        return (unicodeChar >= unicode_UpperCase_letter_start && unicodeChar <= unicode_UpperCase_letter_start + alphabetSize)
            || (unicodeChar >= unicode_LowerCase_letter_start && unicodeChar <= unicode_LowerCase_letter_start + alphabetSize)
            || (unicodeChar >= unicode_number_start && unicodeChar <= unicode_number_start + 9)
            || unicodeChar == spaceCharacter;
    }

    void addCharacterToText(sf::Text& text, sf::Uint32 unicodeChar, size_t characterLimit)
    {
        if(isValidCharacter(unicodeChar) && text.getString().getSize() < characterLimit)
        {
            text.setString(text.getString() + sf::String(unicodeChar));
            text.setPosition(SAVE_WINDOW_WIDTH * 0.5f - text.getGlobalBounds().width * 0.5f, 
                                        SAVE_WINDOW_HEIGHT * 0.5f - text.getGlobalBounds().height * 0.5f);
        }
    }
    void removeLastCharacterToText(sf::Text& text)
    {
        if(text.getString().getSize() <= 0U)
            return;

        text.setString(text.getString().substring(0, text.getString().getSize() - 1));
        text.setPosition(SAVE_WINDOW_WIDTH * 0.5f - text.getGlobalBounds().width * 0.5f, 
                                    SAVE_WINDOW_HEIGHT * 0.5f - text.getGlobalBounds().height * 0.5f);
    }
}

SaveLevelView::SaveLevelView(EditorAppContext _context, std::string &_levelNameToBeChosen, bool isExportView)
    :EditorView(SAVE_WINDOW_WIDTH, SAVE_WINDOW_HEIGHT), context(_context), levelNameToBeChosen(_levelNameToBeChosen),
    promptText("Enter the name of the file\nto " + std::string(isExportView ? "export" : "save") + " and press Enter, for example 'level 0'.", context.fonts->get(EditorResources::TITLE_FONT)),
    tooManyCharacterPrompt("Maximum number of characters reached!", context.fonts->get(EditorResources::TITLE_FONT), 15U),
    userEnteredText("", context.fonts->get(EditorResources::TITLE_FONT), 40U),
    cantOverrideOfficialGameLevelsText("Cannot save or export as official level file name", context.fonts->get(EditorResources::TITLE_FONT), 15U),
    levelNamingErrorClock()
{
    loadResources();
    promptText.setPosition(10.f,10.f);

    tooManyCharacterPrompt.setPosition(10.f, promptText.getGlobalBounds().height + 15.f);
    tooManyCharacterPrompt.setFillColor(sf::Color::Red);

    userEnteredText.setPosition(SAVE_WINDOW_WIDTH * 0.5f, 
                                SAVE_WINDOW_HEIGHT * 0.5f);

    cantOverrideOfficialGameLevelsText.setPosition(userEnteredText.getPosition().x + 10.f,
        userEnteredText.getPosition().y + userEnteredText.getGlobalBounds().height + 15.f);
    cantOverrideOfficialGameLevelsText.setFillColor(sf::Color::Red);

    backgroundClearColor = sf::Color::Magenta;
}

SaveLevelView::~SaveLevelView()
{
}

void SaveLevelView::loadResources()
{
    context.window = &window;
}

void SaveLevelView::handleEvent(const sf::Event &event)
{
    EditorView::handleEvent(event);

    if(event.type == sf::Event::KeyPressed)
    {
        switch (event.key.code)
        {
        case sf::Keyboard::Enter:
            levelNameToBeChosen = userEnteredText.getString().toAnsiString();             
            #ifdef LSM_DEVELOPER_MODE
                isLevelNameValid = true;
            #else
                isLevelNameValid = !LevelNameUtilities::checkIfNameIsOfficialLevel(levelNameToBeChosen);
            #endif
            if(isLevelNameValid)
                window.close();
            else
            {
                levelNameToBeChosen = "";
                (void)levelNamingErrorClock.restart();
            }
            break;
        case sf::Keyboard::Backspace:
            removeLastCharacterToText(userEnteredText);
            break;
        default:
            break;
        }
    }
    else if(event.type == sf::Event::TextEntered)
        addCharacterToText(userEnteredText, event.text.unicode, maxCharacterNumber);
}

void SaveLevelView::update(sf::Time deltaTime)
{
}

void SaveLevelView::render()
{
    window.draw(promptText);
    window.draw(userEnteredText);
    if(userEnteredText.getString().getSize() >= maxCharacterNumber)
        window.draw(tooManyCharacterPrompt);

    if(levelNamingErrorClock.getElapsedTime() <= levelNamingErrorShowingDelay && !isLevelNameValid)
    {
        cantOverrideOfficialGameLevelsText.setPosition(userEnteredText.getPosition().x + 10.f,
            userEnteredText.getPosition().y + userEnteredText.getGlobalBounds().height + 15.f);
        window.draw(cantOverrideOfficialGameLevelsText);
    }

    EditorView::render();
}
