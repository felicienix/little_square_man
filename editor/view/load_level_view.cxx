#include "load_level_view.hpp"
#include "gui_pagination_menu.hpp"
#include "gui_menu_manager.hpp"
#include "gui_button.hpp"
#include "level_name_utilities.hpp"
#include <fstream>
#include <vector>
#include <algorithm>
#define LOAD_WINDOW_WIDTH 800
#define LOAD_WINDOW_HEIGHT 600
#define MAX_BUTTON_PER_PAGE 9

static unsigned int getPlayerLevelProgress()
{
    std::ifstream playerProgressFile(PLAYER_PROGRESS_DIR + std::string("do_not_open_this_file.txt"));
    if(!playerProgressFile.is_open())
        throw std::runtime_error("couldn't open player's progress file");

    unsigned int playerProgress = 0;
    playerProgressFile >> playerProgress;

    playerProgressFile.close();

    return playerProgress;
}

LoadLevelView::LoadLevelView(EditorAppContext _context, std::string &_levelNameToBeChosen)
    :EditorView(LOAD_WINDOW_WIDTH, LOAD_WINDOW_HEIGHT), context(_context), levelNameToBeChosen(_levelNameToBeChosen),
    levelsMenuManager(std::make_unique<alphGUI::MenuManager>()),
    promptText("Choose which level to load", context.fonts->get(EditorResources::TITLE_FONT))
{
    promptText.setPosition(10.f, 10.f);
    loadResources();
    buildMenu();
    backgroundClearColor = sf::Color::Magenta;
}

LoadLevelView::~LoadLevelView()
{
}

void LoadLevelView::loadResources()
{
    context.window = &window;
}

void LoadLevelView::handleEvent(const sf::Event &event)
{
    EditorView::handleEvent(event);

    levelsMenuManager->handleEvent(event, *context.window);
}

void LoadLevelView::update(sf::Time deltaTime)
{
    levelsMenuManager->update(deltaTime);
}

void LoadLevelView::render()
{
    window.draw(promptText);
    window.draw(*(levelsMenuManager.get()));

    EditorView::render();
}

static std::string replaceUnderscoresBySpacesCopy(const std::string& ansiString)
{
    std::string copyStr = ansiString;
    for(char& character : copyStr)
    {
        if(character == '_')
            character = ' ';
    }

    return copyStr;
}

//returns the official level names as pair.first and the non official level names as pair.second
static std::pair<std::vector<std::string>, std::vector<std::string>> getLevelNamesInOrder()
{
    std::vector<std::string> officalLevelNames;
    std::vector<std::string> nonOfficalLevelNames;
    std::map<std::string, std::filesystem::file_time_type> nonOfficialLevelFileWriteTimes;
    std::filesystem::directory_iterator iter(LEVEL_LOAD_DIR);
    for(;iter != std::filesystem::end(iter);++iter)
    {
        std::string fileNameOnly = replaceUnderscoresBySpacesCopy(iter->path().filename().stem().string());
        if(LevelNameUtilities::checkIfNameIsOfficialLevel(fileNameOnly))
            officalLevelNames.push_back(fileNameOnly);
        else
        {
            nonOfficialLevelFileWriteTimes[fileNameOnly] = iter->last_write_time();
            nonOfficalLevelNames.push_back(fileNameOnly);
        }
    }
    std::sort(officalLevelNames.begin(), officalLevelNames.end(), [](const std::string& s1, const std::string& s2)
    {
        int numS1 = std::atoi(s1.substr(6).c_str());
        int numS2 = std::atoi(s2.substr(6).c_str());
        return numS1 <= numS2;
    });
    std::sort(nonOfficalLevelNames.begin(), nonOfficalLevelNames.end(), [&nonOfficialLevelFileWriteTimes](const std::string& s1, const std::string& s2)
    {
        return nonOfficialLevelFileWriteTimes[s1] < nonOfficialLevelFileWriteTimes[s2];
    });
    
    return std::make_pair(officalLevelNames, nonOfficalLevelNames);
}

void LoadLevelView::buildMenu()
{
    alphGUI::PaginationMenu* officalLevelsPaginationMenu = new alphGUI::PaginationMenu();
    alphGUI::PaginationMenu* nonOfficalLevelsPaginationMenu = new alphGUI::PaginationMenu();
    officalLevelsPaginationMenu->setMaxMenuSize(LOAD_WINDOW_WIDTH * 0.7f, LOAD_WINDOW_HEIGHT * 0.35f);
    nonOfficalLevelsPaginationMenu->setMaxMenuSize(LOAD_WINDOW_WIDTH * 0.7f, LOAD_WINDOW_HEIGHT * 0.35f);

    auto levelNamesInOrder = getLevelNamesInOrder();
    LevelButtons levelButtons = buildButtons(levelNamesInOrder);

    officalLevelsPaginationMenu->addComponents(levelButtons.officialLevelButtons);
    nonOfficalLevelsPaginationMenu->addComponents(levelButtons.nonOfficialLevelButtons);


    officalLevelsPaginationMenu->setPosition(LOAD_WINDOW_WIDTH * 0.5f - officalLevelsPaginationMenu->getBoundingRectangle().width * 0.5f, 
                        100.f);
    nonOfficalLevelsPaginationMenu->setPosition(LOAD_WINDOW_WIDTH * 0.5f - nonOfficalLevelsPaginationMenu->getBoundingRectangle().width * 0.5f, 
                        officalLevelsPaginationMenu->getBoundingRectangle().top + officalLevelsPaginationMenu->getBoundingRectangle().height + 10.f);

    levelsMenuManager->addMenuToManage(officalLevelsPaginationMenu);
    levelsMenuManager->addMenuToManage(nonOfficalLevelsPaginationMenu);
}

LoadLevelView::LevelButtons LoadLevelView::buildButtons(const std::pair<std::vector<std::string>, std::vector<std::string>> &levelNamesInOrder)
{
    std::vector<alphGUI::Component*> listOfOfficialButtons;
    std::vector<alphGUI::Component*> listOfNonOfficialButtons;
    alphGUI::Button* fileButton = nullptr;
    size_t playerLevelProgress = getPlayerLevelProgress();
    size_t officialLevelIndex = 0;
    //official levels
    for(const std::string& officialLevelName : levelNamesInOrder.first)
    {
        fileButton = new alphGUI::Button(officialLevelName);
        fileButton->clickFunctionCallback = [this, officialLevelName]()
        {
            levelNameToBeChosen = officialLevelName;
            window.close();
        };
        if(playerLevelProgress < ++officialLevelIndex)
            fileButton->deactivate();
        
        listOfOfficialButtons.push_back(fileButton);
    }
    //non official levels
    for(const std::string& nonOfficialLevelName : levelNamesInOrder.second)
    {
        fileButton = new alphGUI::Button(nonOfficialLevelName);
        fileButton->clickFunctionCallback = [this, nonOfficialLevelName]()
        {
            levelNameToBeChosen = nonOfficialLevelName;
            window.close();
        };
        
        listOfNonOfficialButtons.push_back(fileButton);
    }  

    return {listOfOfficialButtons, listOfNonOfficialButtons};
}
