#ifndef EDITOR_LEVEL_VIEW_HPP
#define EDITOR_LEVEL_VIEW_HPP
#include "editor_view.hpp"

namespace alphGUI
{
    class Menu;
    class Button;
}

class EditorLevelView : public EditorView
{
public:
    EditorLevelView();
    ~EditorLevelView();


    void setSelectedAssetMenu(alphGUI::Menu* currentlySelectedAssetParametersMenu);
private:

    void update(sf::Time deltaTime) override;
    void handleEvent(const sf::Event& event) override;
    void render() override;
    void loadResources() override;
    
    void buildMenu();
public:
    sf::View levelCameraView;
private:
    std::unique_ptr<alphGUI::Menu> assetsMenu;
    alphGUI::Menu* selectedAssetMenu;
    sf::Text helpText;
    std::unique_ptr<class EditorLevel> levelEditor;
    std::string levelLoadedName;
    sf::Clock lastEscapePressedClock;
};

#endif //EDITOR_LEVEL_VIEW_HPP