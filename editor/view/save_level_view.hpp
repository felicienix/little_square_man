#ifndef SAVE_LEVEL_VIEW_HPP
#define SAVE_LEVEL_VIEW_HPP
#include "editor_view.hpp"

class SaveLevelView : public EditorView
{
public:
    SaveLevelView(EditorAppContext _context, std::string& _levelNameToBeChosen, bool isExportView = false);
    ~SaveLevelView() override;

private:
    void loadResources() override;
    void handleEvent(const sf::Event& event) override;
    void update(sf::Time deltaTime) override;
    void render() override;

    EditorAppContext context;
    std::string& levelNameToBeChosen;
    sf::Text promptText;
    sf::Text tooManyCharacterPrompt;
    sf::Text userEnteredText;
    sf::Text cantOverrideOfficialGameLevelsText;
    sf::Clock levelNamingErrorClock;
    sf::Time levelNamingErrorShowingDelay = sf::seconds(3.f);
    bool isLevelNameValid = true;
    size_t maxCharacterNumber = 10;

};

#endif //SAVE_LEVEL_VIEW_HPP