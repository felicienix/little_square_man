#ifndef EDITOR_VIEW_HPP
#define EDITOR_VIEW_HPP
#include "../viewmodel/resource_identifiers.hpp"

class EditorView
{
public:
    EditorView(unsigned int windowWidth = WIDTH, unsigned int windowHeight = HEIGHT);
    virtual ~EditorView();

    void run();

protected:

    virtual void loadResources();
    virtual void handleEvent(const sf::Event& event);
    virtual void update(sf::Time deltaTime);
    virtual void render();
    
    sf::RenderWindow window;
    TextureHolder textures;
    FontHolder fonts;
    SoundBufferHolder sounds;
    sf::Color backgroundClearColor = sf::Color::White;
private:
    sf::Time fps;
    sf::Font fpsFont;
    sf::Text fpsText;


};


#endif //EDITOR_VIEW_HPP