#include "editor_view.hpp"
#include "editor_level_view.hpp"
#include "gui_component.hpp"

EditorView::EditorView(unsigned int windowWidth, unsigned int windowHeight)
    :window(sf::VideoMode(windowWidth, windowHeight), "Little square-man editor !", sf::Style::Close), textures(), fonts(), sounds(),
    fps(sf::seconds(1.f/EDITOR_FPS)), fpsFont(), fpsText()
{
    if(!fpsFont.loadFromFile(FONT_DIR + std::string("calibri.ttf")))
    throw std::runtime_error("couldn't load resources/fonts/calibri.ttf (maybe it doesn't exist)");
    
    fpsText.setFont(fpsFont);
    fpsText.setCharacterSize(10U);
    fpsText.setPosition(5.f, 5.f);
    fpsText.setFillColor(sf::Color::Black);


    loadResources();
}

EditorView::~EditorView()
{
}

void EditorView::run()
{
    sf::Clock appClock;
    sf::Time realDeltaTime, realAccu = sf::Time::Zero;
    sf::Time realFrameRateAccu = sf::Time::Zero;

    while(window.isOpen())
    {
        realDeltaTime = appClock.restart();
        realAccu += realDeltaTime;
        realFrameRateAccu += realDeltaTime;

        sf::Event event;
        while(window.pollEvent(event))
        {
            handleEvent(event);
        }

        while(realAccu >= fps)
        {
            realAccu -= fps;

            update(fps);
        }
        
        //update the real frame rate displaying text every 0.5 seconds
        if(realFrameRateAccu >= sf::seconds(0.5f))
        {
            realFrameRateAccu = sf::Time::Zero;

            fpsText.setString(std::to_string((int)(1 / realDeltaTime.asSeconds())) + " fps\n" + std::to_string(realDeltaTime.asMicroseconds()) + "us");
        }

        window.clear(backgroundClearColor);
        render();
        window.display();
    } 
}

void EditorView::loadResources()
{
}

void EditorView::handleEvent(const sf::Event& event)
{
    switch (event.type)
    {
    case sf::Event::Closed:
        window.close();
        break;
    
    default:
        break;
    }
}

void EditorView::update(sf::Time deltaTime)
{
    
}

void EditorView::render()
{
    window.setView(window.getDefaultView());
    window.draw(fpsText);
}

int main()
{
    try
    {

        alphGUI::Component::setupComponentsResources(ALPH_GUI_RESOURCE_DIR_LOCATION);

        EditorLevelView editorApp;
        editorApp.run();  
        
        alphGUI::Component::freeComponentsResources();     
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
    }
    
}
