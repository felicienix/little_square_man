#ifndef LOAD_LEVEL_VIEW_HPP
#define LOAD_LEVEL_VIEW_HPP
#include "editor_view.hpp"

namespace alphGUI
{
    class MenuManager;
    class Button;
    class Component;
}

class LoadLevelView : public EditorView
{
public:
    LoadLevelView(EditorAppContext _context, std::string& _levelNameToBeChosen);
    ~LoadLevelView() override;

private:
    
    struct LevelButtons
    {
        std::vector<alphGUI::Component*> officialLevelButtons;
        std::vector<alphGUI::Component*> nonOfficialLevelButtons;
    };

    void loadResources() override;
    void handleEvent(const sf::Event& event) override;
    void update(sf::Time deltaTime) override;
    void render() override;
    void buildMenu();
    LevelButtons buildButtons(const std::pair<std::vector<std::string>, std::vector<std::string>> &levelNamesInOrder);
    
    EditorAppContext context;
    std::string& levelNameToBeChosen;

    std::unique_ptr<alphGUI::MenuManager> levelsMenuManager;
    sf::Text promptText;
};


#endif //LOAD_LEVEL_VIEW_HPP