#include "editor_level_view.hpp"
#include "../viewmodel/editor_level.hpp"
#include "../viewmodel/load_and_save_manager.hpp"
#include "load_level_view.hpp"
#include "save_level_view.hpp"
#include "gui_menu.hpp"
#include "gui_button.hpp"
#include "level_name_utilities.hpp"
#include <fstream>
#include <iostream>


static std::string replaceSpacesByUnderscoresCopy(const std::string& ansiString)
{
    std::string copyStr = ansiString;
    for(char& character : copyStr)
    {
        if(character == ' ')
            character = '_';
    }

    return copyStr;
}

EditorLevelView::EditorLevelView()
    :EditorView(), levelCameraView(window.getDefaultView())
    , assetsMenu(std::make_unique<alphGUI::Menu>()), selectedAssetMenu(nullptr), helpText(),
     levelEditor(std::make_unique<EditorLevel>(EditorAppContext(window, textures, fonts, sounds), *this)), levelLoadedName(""), lastEscapePressedClock()
{
    loadResources();
    helpText.setFont(fonts.get(EditorResources::TITLE_FONT));
    helpText.setString("You can press E to expand a selected wall/platform and R to remove it");
    helpText.setCharacterSize(20U);
    helpText.setPosition(50.f, 5.f);
    helpText.setFillColor(sf::Color::Black);
    
    assetsMenu->maxMenuSize.x = WIDTH * 0.5f;
    assetsMenu->maxMenuSize.y = HEIGHT;
    buildMenu();
    
    assetsMenu->setPosition(WIDTH - assetsMenu->getBoundingRectangle().width, 0.f);

    levelCameraView.setViewport(sf::FloatRect(0.f,0.f, assetsMenu->getPosition().x / levelCameraView.getSize().x,1.f));

    levelEditor->addNewAssetToLevel(EditorResources::AssetType::CHARACTER);
}

EditorLevelView::~EditorLevelView()
{
    if(selectedAssetMenu)
        delete selectedAssetMenu;
}

void EditorLevelView::update(sf::Time deltaTime)
{
    if(selectedAssetMenu)
        selectedAssetMenu->update(deltaTime);
    else
        assetsMenu->update(deltaTime);
    
}

void EditorLevelView::handleEvent(const sf::Event &event)
{
    EditorView::handleEvent(event);
    
    if(sf::Mouse::getPosition(window).x < assetsMenu->getBoundingRectangle().left)
        levelEditor->handleEvent(event);
    if(selectedAssetMenu)
        selectedAssetMenu->handleEvent(event, window);
    else
    {
        assetsMenu->handleEvent(event, window);

        if(event.type == sf::Event::KeyPressed)
        {
            switch (event.key.code)
            {
            case sf::Keyboard::Escape:
                if(lastEscapePressedClock.restart() <= sf::seconds(0.2f))
                    window.close();
                break;
            default:
                break;
            }
        }
    }
}

void EditorLevelView::render()
{
    window.setView(levelCameraView);
    window.draw(*(levelEditor.get()));
    
    window.setView(window.getDefaultView());

    sf::RectangleShape editorMenuBackgroundRect(sf::Vector2f((selectedAssetMenu) ? selectedAssetMenu->getBoundingRectangle().width 
        : assetsMenu->getBoundingRectangle().width, HEIGHT));
    editorMenuBackgroundRect.setFillColor(sf::Color::Black);
    editorMenuBackgroundRect.setPosition((selectedAssetMenu) ? selectedAssetMenu->getPosition().x : assetsMenu->getPosition().x, 0.f);

    helpText.setString("You can press E to expand a selected wall/platform and R to remove it."
     + sf::String((levelLoadedName == "") ? "\nNo level currently loaded" : "\nLevel currently loaded: " + levelLoadedName));

    window.draw(editorMenuBackgroundRect);
    window.draw(helpText);
    if(selectedAssetMenu)
        window.draw(*selectedAssetMenu);
    else
        window.draw(*(assetsMenu.get()));

    EditorView::render();
}

void EditorLevelView::loadResources()
{
    //load textures
    textures.load(TEXTURE_DIR + std::string("main_character.png"), EditorResources::Textures::MAIN_CHARACTER);
    textures.load(TEXTURE_DIR + std::string("square.png"), EditorResources::Textures::SQUARE_TEXTURE);
    textures.load(TEXTURE_DIR + std::string("checkpoint.png"), EditorResources::Textures::CHECKPOINT_TEXTURE);
    textures.load(TEXTURE_DIR + std::string("platform.png"), EditorResources::Textures::PLATFORM_TEXTURE);

    //load fonts
    fonts.load(FONT_DIR + std::string("title_font.ttf"), EditorResources::TITLE_FONT);

    //load sounds
}

void EditorLevelView::buildMenu()
{
    EditorAppContext context{window, textures, fonts, sounds};
    alphGUI::Button* wallAssetButton = new alphGUI::Button("wall");
    wallAssetButton->clickFunctionCallback = [this](){ levelEditor->addNewAssetToLevel(EditorResources::WALL_ASSET);};
    assetsMenu->addComponent(wallAssetButton);
    
    alphGUI::Button* deathBlockAssetButton = new alphGUI::Button("deathBlock");
    deathBlockAssetButton->clickFunctionCallback = [this](){ levelEditor->addNewAssetToLevel(EditorResources::DEATH_BLOCK);};
    assetsMenu->addComponent(deathBlockAssetButton);

    alphGUI::Button* platformAssetButton = new alphGUI::Button("platform");
    platformAssetButton->clickFunctionCallback = [this](){ levelEditor->addNewAssetToLevel(EditorResources::PLATFORM_ASSET);};
    assetsMenu->addComponent(platformAssetButton);
    
    alphGUI::Button* checkpointAssetButton = new alphGUI::Button("checkpoint");
    checkpointAssetButton->clickFunctionCallback = [this](){ levelEditor->addNewAssetToLevel(EditorResources::CHECKPOINT_ASSET);};
    assetsMenu->addComponent(checkpointAssetButton);

    alphGUI::Button* rockAssetButton = new alphGUI::Button("rock");
    rockAssetButton->clickFunctionCallback = [this](){ levelEditor->addNewAssetToLevel(EditorResources::ROCK_ASSET);};
    assetsMenu->addComponent(rockAssetButton);

    alphGUI::Button* spriteAssetButton = new alphGUI::Button("square");
    spriteAssetButton->clickFunctionCallback = [this](){ levelEditor->addNewAssetToLevel(EditorResources::SPRITE_ASSET);};
    assetsMenu->addComponent(spriteAssetButton);


    alphGUI::Button* exportButton = new alphGUI::Button("export");
    exportButton->clickFunctionCallback = [this, context](){ 
        #ifdef LSM_DEVELOPER_MODE
            bool isLevelNameOfficial = true;
            std::string levelSubFolder = (LevelNameUtilities::checkIfNameIsOfficialLevel(levelLoadedName) ? "official_levels/" : "non_official_levels/");
        #else
            bool isLevelNameOfficial = !LevelNameUtilities::checkIfNameIsOfficialLevel(levelLoadedName);
            std::string levelSubFolder = "non_official_levels/";
        #endif
        if(levelLoadedName != "" && isLevelNameOfficial)
            levelEditor->exportLevelForGame(LEVEL_EXPORT_DIR + levelSubFolder + replaceSpacesByUnderscoresCopy(levelLoadedName) + std::string(".json"));
        else
        {
            std::string levelLoadedNameBuffer = levelLoadedName;
            SaveLevelView exportLevelView(context, levelLoadedName, true);
            exportLevelView.run();
            if(levelLoadedName != "")
                levelEditor->exportLevelForGame(LEVEL_EXPORT_DIR + levelSubFolder + replaceSpacesByUnderscoresCopy(levelLoadedName) + std::string(".json"));
            else
                levelLoadedName = levelLoadedNameBuffer;
        }
    };
    assetsMenu->addComponent(exportButton);

    alphGUI::Button* saveButton = new alphGUI::Button("save");
    saveButton->clickFunctionCallback = [this, context](){  
        #ifdef LSM_DEVELOPER_MODE
            bool isLevelNameOfficial = true;
        #else
            bool isLevelNameOfficial = !LevelNameUtilities::checkIfNameIsOfficialLevel(levelLoadedName);
        #endif
        if(levelLoadedName != "" && isLevelNameOfficial)
            levelEditor->saveLevel(LEVEL_LOAD_DIR + replaceSpacesByUnderscoresCopy(levelLoadedName) + std::string(".json"));
        else
        {
            std::string levelLoadedNameBuffer = levelLoadedName;
            SaveLevelView saveLevelView(context, levelLoadedName);
            saveLevelView.run();
            if(levelLoadedName != "")
                levelEditor->saveLevel(LEVEL_LOAD_DIR + replaceSpacesByUnderscoresCopy(levelLoadedName) + std::string(".json"));
            else
                levelLoadedName = levelLoadedNameBuffer;
        }
    };
    assetsMenu->addComponent(saveButton);
    
    alphGUI::Button* saveAsButton = new alphGUI::Button("save as");
    saveAsButton->clickFunctionCallback = [this, context](){ 
        std::string levelLoadedNameBuffer = levelLoadedName;
        SaveLevelView saveLevelView(context, levelLoadedName);
        saveLevelView.run();
        if(levelLoadedName != "")
            levelEditor->saveLevel(LEVEL_LOAD_DIR + replaceSpacesByUnderscoresCopy(levelLoadedName) + std::string(".json"));
        else
            levelLoadedName = levelLoadedNameBuffer;
    };
    assetsMenu->addComponent(saveAsButton);

    alphGUI::Button* loadButton = new alphGUI::Button("load");
    loadButton->clickFunctionCallback = [this, context](){ 
        LoadLevelView loadLevelView(context, levelLoadedName);
        loadLevelView.run();
        if(levelLoadedName != "")
            levelEditor->loadLevel(LEVEL_LOAD_DIR + replaceSpacesByUnderscoresCopy(levelLoadedName) + std::string(".json"));
    };
    assetsMenu->addComponent(loadButton);

}

void EditorLevelView::setSelectedAssetMenu(alphGUI::Menu *currentlySelectedAssetParametersMenu)
{

    if(currentlySelectedAssetParametersMenu)
    {
        currentlySelectedAssetParametersMenu->maxMenuSize.x = WIDTH * 0.5f;
        currentlySelectedAssetParametersMenu->setPosition(WIDTH - currentlySelectedAssetParametersMenu->getBoundingRectangle().width, 0.f);
        levelCameraView.setViewport(sf::FloatRect(0.f,0.f, currentlySelectedAssetParametersMenu->getPosition().x / levelCameraView.getSize().x,1.f));
        assetsMenu->setAllComponentsToUnselected();
    }
    else
        levelCameraView.setViewport(sf::FloatRect(0.f,0.f, assetsMenu->getPosition().x / levelCameraView.getSize().x,1.f));

    if(selectedAssetMenu)
        delete selectedAssetMenu;

    selectedAssetMenu = currentlySelectedAssetParametersMenu;
}
