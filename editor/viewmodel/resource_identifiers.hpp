#ifndef RESOURCE_IDENTIFIERS_H
#define RESOURCE_IDENTIFIERS_H
#include "../../game/header/resource_holder.hpp"
#include "SFML/Graphics.hpp"
#include "SFML/Audio.hpp"
#include "../../game/header/json.hpp"

#define WIDTH 1200
#define HEIGHT 800
/* #define TEXTURE_DIR "../resources/textures/"
#define FONT_DIR "../resources/fonts/"
#define SOUND_DIR "../resources/sounds/"
#define LEVEL_EXPORT_DIR "../resources/game_levels/"
#define LEVEL_LOAD_DIR "../resources/editor_levels/" */
#define EDITOR_FPS 60.f


namespace EditorResources
{
	enum Textures
	{
		MAIN_CHARACTER, SQUARE_TEXTURE, CHECKPOINT_TEXTURE, PLATFORM_TEXTURE, TEXTURES_COUNT 
	};

	enum Fonts
	{
		TITLE_FONT, FONTS_COUNT
	};

	enum Sounds
	{
		SOUNDS_COUNT
	};

    enum AssetType
    {
        SPRITE_ASSET, WALL_ASSET, CHARACTER, ROCK_ASSET, DEATH_BLOCK, PLATFORM_ASSET, CHECKPOINT_ASSET, ASSET_TYPE_COUNT
    };

	constexpr unsigned int SQUARE_SIZE = 80;
	constexpr unsigned int CHECKPOINT_HEIGHT = SQUARE_SIZE / 5;
}

typedef ResourceHolder<sf::Texture, unsigned int> TextureHolder;
typedef ResourceHolder<sf::Font, unsigned int> FontHolder;
typedef ResourceHolder<sf::SoundBuffer, unsigned int> SoundBufferHolder;

struct EditorAppContext
{
	EditorAppContext()
		:window(nullptr), textures(nullptr), fonts(nullptr), sounds(nullptr), levelBeingEdited(nullptr){}
	EditorAppContext(sf::RenderWindow& _window, TextureHolder& _textures, FontHolder& _fonts, SoundBufferHolder& _sounds)
		:window(&_window), textures(&_textures), fonts(&_fonts), sounds(&_sounds), levelBeingEdited(nullptr){}
	sf::RenderWindow* window;
	TextureHolder* textures;
	FontHolder* fonts;
	SoundBufferHolder* sounds;
	class EditorLevel* levelBeingEdited;
};

#endif //RESOURCE_IDENTIFIERS_H