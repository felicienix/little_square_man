#ifndef EDITOR_LEVEL_HPP
#define EDITOR_LEVEL_HPP
#include "resource_identifiers.hpp"

class EditorLevel : public sf::Drawable
{
public:
    EditorLevel(EditorAppContext _context, class EditorLevelView& _levelView);
    ~EditorLevel();

    void addNewAssetToLevel(EditorResources::AssetType assetType);

    void exportLevelForGame(const std::string& jsonFileName);
    void saveLevel(const std::string& jsonFileName);
    void loadLevel(const std::string& jsonFileName);

    void handleEvent(const sf::Event& event);

    sf::Vector2f getMousePositionInLevel() const;

private:
    void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
    EditorAppContext context;
    class EditorLevelView& levelView;
    std::unique_ptr<class EditorAssetHandler> assetHandler;
};


#endif //EDITOR_LEVEL_HPP
