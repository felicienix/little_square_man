#ifndef LOAD_AND_SAVE_MANAGER_HPP
#define LOAD_AND_SAVE_MANAGER_HPP
#include "SFML/Graphics/Transformable.hpp"
#include "../../game/header/entities_resources_and_identifiers.hpp"
#include "../../game/header/json.hpp"
#include "resource_identifiers.hpp"

class LoadAndSaveManager
{
public:
    static void loadTransformable(sf::Transformable& transformableEntity, const nlohmann::json& jsonToLoadFrom)
    {
        transformableEntity.setPosition(jsonToLoadFrom[GameResources::JSON_ENTITY_POSITION_STRING][0], jsonToLoadFrom[GameResources::JSON_ENTITY_POSITION_STRING][1]);
        transformableEntity.setRotation(jsonToLoadFrom[GameResources::JSON_ENTITY_ROTATION_STRING]);
        transformableEntity.setScale(jsonToLoadFrom[GameResources::JSON_ENTITY_SCALE_STRING][0], jsonToLoadFrom[GameResources::JSON_ENTITY_SCALE_STRING][1]);
    }

    static void saveTransformable(sf::Transformable& transformableEntity, nlohmann::json& jsonToSaveTo)
    {
        jsonToSaveTo[GameResources::JSON_ENTITY_POSITION_STRING] = {transformableEntity.getPosition().x, transformableEntity.getPosition().y};
        jsonToSaveTo[GameResources::JSON_ENTITY_ROTATION_STRING] = transformableEntity.getRotation();
        jsonToSaveTo[GameResources::JSON_ENTITY_SCALE_STRING] = {transformableEntity.getScale().x, transformableEntity.getScale().y};
    }
};


#endif //LOAD_AND_SAVE_MANAGER_HPP