#include "editor_level.hpp"
#include "../model/editor_asset_handler.hpp"
#include "../view/editor_level_view.hpp"
#include "../model/wall_asset.hpp"
#include "../model/character_asset.hpp"
#include "../model/rock_asset.hpp"
#include "../model/deathblock_asset.hpp"
#include "../model/platform_asset.hpp"
#include "../model/checkpoint_asset.hpp"
#include "../../game/header/entities_resources_and_identifiers.hpp"
#include <fstream>

EditorLevel::EditorLevel(EditorAppContext _context, EditorLevelView &_levelView)
    :context(_context), levelView(_levelView), assetHandler()
{
    context.levelBeingEdited = this;
    assetHandler.reset(new EditorAssetHandler(context));
}

EditorLevel::~EditorLevel()
{
}

void EditorLevel::addNewAssetToLevel(EditorResources::AssetType assetType)
{
    EditorAsset* assetToAdd = nullptr;
    switch (assetType)
    {
    case EditorResources::AssetType::SPRITE_ASSET:
        assetToAdd = new SpriteAsset(context, EditorResources::Textures::SQUARE_TEXTURE);
        break;
    case EditorResources::AssetType::WALL_ASSET:
        assetToAdd = new WallAsset(context);
        break;
    case EditorResources::AssetType::CHARACTER:
        assetToAdd = new CharacterAsset(context);
        break;
    case EditorResources::AssetType::ROCK_ASSET:
        assetToAdd = new RockAsset(context);
        break;
    case EditorResources::AssetType::DEATH_BLOCK:
        assetToAdd = new DeathBlockAsset(context);
        break;
    case EditorResources::AssetType::PLATFORM_ASSET:
        assetToAdd = new PlatformAsset(context);
        break;
    case EditorResources::AssetType::CHECKPOINT_ASSET:
        assetToAdd = new CheckpointAsset(context);
        break;
    
    default:
        break;
    }

    assetToAdd->setPosition(context.window->mapPixelToCoords(sf::Vector2i(300,300), levelView.levelCameraView));
    assetHandler->addAsset(assetToAdd);
    levelView.setSelectedAssetMenu(assetToAdd->generateAssetMenu());
}

void EditorLevel::exportLevelForGame(const std::string &jsonFileName)
{
    std::ofstream outputJSONfile(jsonFileName, std::ios_base::trunc);
    if(!outputJSONfile.is_open())
        throw std::runtime_error("failed to open " + jsonFileName + " in exportLevelForGame");

    nlohmann::json outputJSON;
    sf::FloatRect levelBounds(INFINITY,0,0,HEIGHT);
    
    for(auto* asset : assetHandler->getAssets())
    {
        nlohmann::json entityJSON;
        sf::FloatRect assetBoundingRect = asset->getBoundingRectangle();
        asset->exportAssetToJSON(entityJSON);
        outputJSON[GameResources::JSON_ENTITIES_ARRAY_STRING].push_back(entityJSON);

        if(assetBoundingRect.left <= levelBounds.left)
            levelBounds.left = assetBoundingRect.left;
        if(assetBoundingRect.left + assetBoundingRect.width >= levelBounds.left + levelBounds.width)
            levelBounds.width = assetBoundingRect.left + assetBoundingRect.width - levelBounds.left;

        if(asset->getAssetType() == EditorResources::AssetType::CHARACTER)
            outputJSON[GameResources::JSON_GAME_LEVEL_INITIAL_CHARACTER_POSITION] = {asset->getPosition().x, asset->getPosition().y};
        if(asset->getAssetType() == EditorResources::AssetType::CHECKPOINT_ASSET && ((CheckpointAsset*)asset)->getCheckpointType() == CheckpointAsset::CheckpointType::START)
            outputJSON[GameResources::JSON_GAME_LEVEL_INITIAL_CHARACTER_POSITION] = {asset->getPosition().x + asset->getBoundingRectangle().width * 0.5f
                , asset->getPosition().y - EditorResources::SQUARE_SIZE};
    }

    outputJSON[GameResources::JSON_GAME_LEVELBOUNDS_STRING] = {levelBounds.left, levelBounds.top, levelBounds.width, levelBounds.height};


    outputJSONfile << outputJSON;
    outputJSONfile.close();
}

void EditorLevel::saveLevel(const std::string &jsonFileName)
{
    std::ofstream outputJSONfile(jsonFileName, std::ios_base::trunc);
    if(!outputJSONfile.is_open())
        throw std::runtime_error("failed to open " + jsonFileName + " in saveLevel");

    nlohmann::json outputJSON;
    for(auto* asset : assetHandler->getAssets())
    {
        nlohmann::json assetJSON;
        asset->saveAssetToJSON(assetJSON);
        outputJSON.push_back(assetJSON);
    }

    outputJSONfile << outputJSON;
    outputJSONfile.close();
}

void EditorLevel::loadLevel(const std::string &jsonFileName)
{
    std::ifstream inputJSONfile(jsonFileName);
    if(!inputJSONfile.is_open())
        throw std::runtime_error("failed to open " + jsonFileName + " in loadLevel");

    assetHandler->clearAllAssets();
    nlohmann::json inputJSON;
    inputJSONfile >> inputJSON;
    float xCharacterPosition = 0.f;
    for(auto& assetJSON : inputJSON)
    {
        addNewAssetToLevel(assetJSON[GameResources::JSON_ENTITY_TYPE_STRING]);
        assetHandler->getAssets().front()->loadAssetFromJSON(assetJSON);
        if(assetJSON[GameResources::JSON_ENTITY_TYPE_STRING] == EditorResources::AssetType::CHARACTER)
            xCharacterPosition = assetHandler->getAssets().front()->getPosition().x;
    }

    levelView.levelCameraView.setCenter(xCharacterPosition, levelView.levelCameraView.getCenter().y);
    inputJSONfile.close();
}

void EditorLevel::handleEvent(const sf::Event &event)
{
    EditorAsset* previouslySelectedAsset = assetHandler->getCurrentlySelectedAsset();
    switch (event.type)
    {
    case sf::Event::KeyPressed:
        switch (event.key.code)
        {
        case sf::Keyboard::Q:
            levelView.levelCameraView.move(-(int)(EditorResources::SQUARE_SIZE), 0.f);
            break;
        case sf::Keyboard::D:
            levelView.levelCameraView.move(EditorResources::SQUARE_SIZE, 0.f);
            break;
        
        default:
            break;
        }
        break;
    
    default:
        break;
    }

    assetHandler->handleEvent(event);

    EditorAsset* currentlySelectedAsset = assetHandler->getCurrentlySelectedAsset();
    if(currentlySelectedAsset != previouslySelectedAsset)
    {
        if(currentlySelectedAsset)
            levelView.setSelectedAssetMenu(currentlySelectedAsset->generateAssetMenu());
        else
            levelView.setSelectedAssetMenu(nullptr);
    }
}

sf::Vector2f EditorLevel::getMousePositionInLevel() const
{
    return context.window->mapPixelToCoords(sf::Mouse::getPosition(*context.window), levelView.levelCameraView);
}

void EditorLevel::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    for(auto iter_asset = assetHandler->getAssets().rbegin(); iter_asset != assetHandler->getAssets().rend();++iter_asset)
    {
        target.draw(*(*iter_asset), states);
    }
}
